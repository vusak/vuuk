package io.jmobile.vuuk.base;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class BaseClickableViewPager extends ViewPager {
    Context context;
    OnItemClickListener mOnItemClickListenr;

    public BaseClickableViewPager(Context context) {
        super(context);
        context = context;
        init();
    }

    public BaseClickableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        context = context;
        init();
    }

    private void init() {
        final GestureDetector detector = new GestureDetector(context, new TapGestureListener());
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (getAdapter() == null || getAdapter().getCount() == 0) {
                    return false;
                }
                detector.onTouchEvent(event);
                return false;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (getAdapter() == null || getAdapter().getCount() == 0) {
            return false;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public void setOnItemClickLIstener(OnItemClickListener listener) {
        mOnItemClickListenr = listener;
    }

    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
        return super.getChildDrawingOrder(childCount, i);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private class TapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (mOnItemClickListenr != null)
                mOnItemClickListenr.onItemClick(getCurrentItem());
            return true;
        }
    }
}

package io.jmobile.vuuk.base;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;

import io.jmobile.vuuk.BaseApplication;
import io.jmobile.vuuk.R;
import io.jmobile.vuuk.common.DBController;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.network.Api;
import io.jmobile.vuuk.network.BookDownloadManager;
import io.jmobile.vuuk.network.JNetworkMonitor;

public class BaseActivity extends FragmentActivity implements Api.ApiListener {
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    protected BaseApplication app;
    protected SPController sp;
    protected DBController db;
    protected BaseApplication.ResourceWrapper r;
    protected BookDownloadManager downloadManager;
    protected JNetworkMonitor networkMonitor;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_a100));
        }
        super.onCreate(savedInstanceState);

        app = (BaseApplication) getApplication();
        sp = app.getSPController();
        db = app.getDBController();
        r = app.getResourceWrapper();
        downloadManager = app.getDownloadManager();
        networkMonitor = app.getNetworkMonitor();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    protected <T extends BaseFragment> T addf(String tag, BaseFragment.BaseFragmentCreator<T> creator) {
        return Util.addf(getSupportFragmentManager(), tag, creator);
    }

    protected <T extends BaseFragment> T ff(String tag) {
        return Util.ff(getSupportFragmentManager(), tag);
    }

    protected <T extends BaseDialogFragment> T fdf(String tag) {
        return Util.fdf(getSupportFragmentManager(), tag);
    }

    protected void hdf(String tag) {
        Util.hdf(getSupportFragmentManager(), tag);
    }

    protected void sdf(BaseDialogFragment d) {
        Util.sdf(getSupportFragmentManager(), d);
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T fv(int id) {
        try {
            return (T) findViewById(id);
        } catch (ClassCastException e) {
            return null;
        }
    }

    protected void log(String msg) {
        LogUtil.log(getClass().getSimpleName(), msg);
    }

    protected void log(Throwable tr) {
        LogUtil.log(getClass().getSimpleName(), tr);
    }

    @Override
    public void handleApiMessage(Message m) {

    }

    @Override
    public BaseActivity getApiActivity() {
        return null;
    }

    @Override
    public FragmentManager getApiFragmentManager() {
        return null;
    }
}

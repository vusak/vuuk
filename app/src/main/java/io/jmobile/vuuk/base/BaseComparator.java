package io.jmobile.vuuk.base;

import android.text.TextUtils;

import java.nio.charset.Charset;

public class BaseComparator {
    protected int asc;

    public BaseComparator(boolean asc) {
        this.asc = asc ? 1 : -1;
    }

    protected final int compareString(String lhs, String rhs, boolean emptyToNull) {
        return compareString(lhs, rhs, emptyToNull, asc == 1);
    }

    protected final int compareString(String lhs, String rhs, boolean emptyToNull, boolean asc) {
        if (emptyToNull && TextUtils.isEmpty(lhs))
            lhs = null;
        if (emptyToNull && TextUtils.isEmpty(rhs))
            rhs = null;

        if (lhs == null && rhs != null)
            return emptyToNull ? 1 : (asc ? -1 : 1);
        else if (rhs == null && lhs != null)
            return emptyToNull ? -1 : (asc ? 1 : -1);
        else if (lhs == null && rhs == null)
            return 0;
        else {
            String lhsUTF8 = new String(Charset.forName("UTF-8").encode(lhs).array());
            String rhsUTF8 = new String(Charset.forName("UTF-8").encode(rhs).array());
            return (asc ? 1 : -1) * lhsUTF8.compareToIgnoreCase(rhsUTF8);
        }
    }

    protected final int compareInt(int lhs, int rhs, boolean zeroToEmpty) {
        if (zeroToEmpty && lhs == 0 && rhs > 0)
            return 1;
        else if (zeroToEmpty && lhs > 0 && rhs == 0)
            return -1;
        else
            return (lhs - rhs) * asc;
    }

    protected final int compareLong(long lhs, long rhs, boolean zeroToEmpty) {
        return compareLong(lhs, rhs, zeroToEmpty, asc == 1);
    }

    protected final int compareLong(long lhs, long rhs, boolean zeroToEmpty, boolean asc) {
        if (zeroToEmpty && lhs == 0 && rhs > 0)
            return 1;
        else if (zeroToEmpty && lhs > 0 && rhs == 0)
            return -1;
        else if (lhs == rhs)
            return 0;
        else if (lhs < rhs)
            return asc ? -1 : 1;
        else
            return asc ? 1 : -1;
    }
}

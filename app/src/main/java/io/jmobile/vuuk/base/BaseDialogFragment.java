package io.jmobile.vuuk.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import io.jmobile.vuuk.BaseApplication;
import io.jmobile.vuuk.common.DBController;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.network.Api;

public abstract class BaseDialogFragment extends DialogFragment implements Api.ApiListener {
    protected static final String DIALOG_TAG = "DIALOG_TAG";
    private static final String DISPATCH_TO_ACTIVITY_ON_BACK_PRESSED = "dispatch_to_activity_on_back_pressed";

    protected BaseApplication app;
    protected SPController sp;
    protected DBController db;
    protected BaseApplication.ResourceWrapper r;

    protected DialogDismissListener dismissListener;
    protected DialogPositiveListener positiveListener;
    protected DialogNegativeListener negativeListener;

    protected String tag;

    protected TextView dialogTitleText;
    protected View dialogCloseButton;
    protected boolean byCloseButton = false;
    private View v;

    private boolean dispatchToActivityOnBackPressed = false;
    private boolean softCancel;

    public abstract int getLayoutId();

    public abstract void onCreateView(Bundle savedInstanceState);

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Fragment f = getTargetFragment();
        if (f != null) {
            if (f instanceof DialogDismissListener && dismissListener == null)
                dismissListener = (DialogDismissListener) f;

            if (f instanceof DialogPositiveListener && positiveListener == null)
                positiveListener = (DialogPositiveListener) f;

            if (f instanceof DialogNegativeListener && negativeListener == null)
                negativeListener = (DialogNegativeListener) f;
        } else {
            if (activity instanceof DialogDismissListener && dismissListener == null)
                dismissListener = (DialogDismissListener) activity;

            if (activity instanceof DialogPositiveListener && positiveListener == null)
                positiveListener = (DialogPositiveListener) activity;

            if (activity instanceof DialogNegativeListener && negativeListener == null)
                negativeListener = (DialogNegativeListener) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (BaseApplication) getActivity().getApplication();
        sp = app.getSPController();
        db = app.getDBController();
        r = app.getResourceWrapper();

        Bundle arg = getArguments();

        if (savedInstanceState != null)
            dispatchToActivityOnBackPressed = savedInstanceState.getBoolean(DISPATCH_TO_ACTIVITY_ON_BACK_PRESSED, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(getLayoutId(), container, false);

        onCreateView(savedInstanceState);
        return v;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK)
                    if (dispatchToActivityOnBackPressed)
                        getActivity().onBackPressed();

                return false;
            }
        });

        return d;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(DISPATCH_TO_ACTIVITY_ON_BACK_PRESSED, dispatchToActivityOnBackPressed);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (dismissListener != null)
            dismissListener.onDialogDismiss(this, tag, byCloseButton);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        hideKeyboard();
        super.onCancel(dialog);
    }

    @Override
    public void dismiss() {
        hideKeyboard();
        try {
            super.dismiss();
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = 600;
        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public void dismissAllowingStateLoss() {
        hideKeyboard();
        try {
            super.dismissAllowingStateLoss();
        } catch (Exception e) {
        }
    }

    @Override
    public void setCancelable(boolean cancelable) {
        super.setCancelable(cancelable);
        dispatchToActivityOnBackPressed = !cancelable;
    }

    public void setCancelable(boolean cancelable, boolean dispatchToActivityOnBackPressed) {
        super.setCancelable(cancelable);
        this.dispatchToActivityOnBackPressed = dispatchToActivityOnBackPressed;
    }

    public void setPositiveListener(DialogPositiveListener listener) {
        positiveListener = listener;
    }

    public void setNegativeListener(DialogNegativeListener listener) {
        negativeListener = listener;
    }

    public void setDismissListener(DialogDismissListener listener) {
        dismissListener = listener;
    }

    public String getDialogTag() {
        return tag;
    }

    protected Bundle createArguments(String tag) {
        if (tag == null)
            tag = getDefaultTag();
        this.tag = tag;

        Bundle b = new Bundle();
        b.putString(DIALOG_TAG, tag);
        setArguments(b);

        return b;
    }

    protected <T extends BaseFragment> T ff(String tag) {
        return Util.ff(getFragmentManager(), tag);
    }

    protected <T extends BaseDialogFragment> T fdf(String tag) {
        return Util.fdf(getFragmentManager(), tag);
    }

    protected void hdf(String tag) {
        Util.hdf(getFragmentManager(), tag);
    }

    protected void sdf(BaseDialogFragment d) {
        Util.sdf(getFragmentManager(), d);
    }

    protected <T extends View> T fv(int id) {
        try {
            return (T) v.findViewById(id);
        } catch (ClassCastException e) {
            return null;
        }
    }

    protected void log(String msg) {
        LogUtil.log(getDefaultTag(), msg);
    }

    protected void log(Throwable tr) {
        LogUtil.log(getDefaultTag(), tr);
    }

    protected EditText[] getEditTexts() {
        return null;
    }

    private String getDefaultTag() {
        return getClass().getSimpleName();
    }

    private void hideKeyboard() {
        EditText[] list = getEditTexts();
        if (list != null)
            for (EditText edit : list)
                if (edit != null)
                    Util.hideKeyBoard(edit);
    }

    @Override
    public void handleApiMessage(Message m) {

    }

    @Override
    public BaseActivity getApiActivity() {
        return null;
    }

    @Override
    public FragmentManager getApiFragmentManager() {
        return null;
    }

    public static interface DialogDismissListener {
        public void onDialogDismiss(BaseDialogFragment dialog, String tag, boolean byCloseButton);
    }

    public static interface DialogPositiveListener {
        public void onDialogPositive(BaseDialogFragment dialog, String tag);
    }

    public static interface DialogNegativeListener {
        public void onDialogNegative(BaseDialogFragment dialog, String tag);
    }

    public static interface BaseFragmentCreator<T extends BaseFragment> {
        public T create();

        public int getFrameId();
    }
}

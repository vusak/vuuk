package io.jmobile.vuuk.base;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.jmobile.vuuk.BaseApplication;
import io.jmobile.vuuk.common.DBController;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.network.Api;
import io.jmobile.vuuk.network.BookDownloadManager;
import io.jmobile.vuuk.network.JNetworkMonitor;


public abstract class BaseFragment extends Fragment implements Api.ApiListener {
    protected BaseApplication app;
    protected SPController sp;
    protected DBController db;
    protected BaseApplication.ResourceWrapper r;

    protected BookDownloadManager downloadManager;
    protected JNetworkMonitor networkMonitor;
    private View v;

    public abstract int getLayoutId();

    public abstract void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (BaseApplication) getActivity().getApplication();
        sp = app.getSPController();
        db = app.getDBController();
        r = app.getResourceWrapper();
        downloadManager = app.getDownloadManager();
        networkMonitor = app.getNetworkMonitor();
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(getLayoutId(), container, false);

        onCreateView(savedInstanceState);

        return v;
    }

    protected <T extends BaseFragment> T ff(String tag) {
        return Util.ff(getFragmentManager(), tag);
    }

    protected <T extends BaseDialogFragment> T fdf(String tag) {
        return Util.fdf(getFragmentManager(), tag);
    }

    protected void hdf(String tag) {
        Util.hdf(getFragmentManager(), tag);
    }

    protected void sdf(BaseDialogFragment d) {
        Util.sdf(getFragmentManager(), d);
    }

    protected <T extends View> T fv(int id) {
        try {
            return (T) v.findViewById(id);
        } catch (ClassCastException e) {
            log(e.getMessage());
            return null;
        }
    }

    protected void log(String msg) {
        LogUtil.log(getClass().getSimpleName(), msg);
    }

    protected void log(Throwable tr) {
        LogUtil.log(getClass().getSimpleName(), tr);
    }

    @Override
    public void handleApiMessage(Message m) {

    }

    @Override
    public BaseActivity getApiActivity() {
        return null;
    }

    @Override
    public FragmentManager getApiFragmentManager() {
        return null;
    }

    public static interface BaseFragmentCreator<T extends BaseFragment> {
        public T create();

        public int getFrameId();
    }


}

package io.jmobile.vuuk.network;

import io.jmobile.vuuk.common.FileUtil;
import io.jmobile.vuuk.data.BookItem;

public class CoverDownloadTask extends FileUtil.DownloadTask<CoverDownloadManager> {
    private BookItem book;
    private String bookId;

    public CoverDownloadTask(CoverDownloadManager manager, String bookId, BookItem book) {
        super(manager);
        this.bookId = bookId;
        this.book = book;
    }

    @Override
    public void onProgress(long Progress, long total) {

    }

    @Override
    public void onSuccess() {
        if (ref.get() == null)
            return;

        ref.get().onCoverDownloadSuccess(bookId, book);
    }

    @Override
    public void onError(int resultCode) {

    }

    public String getBookId() {
        return this.bookId;
    }

    public BookItem getBook() {
        return book;
    }
}

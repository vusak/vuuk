package io.jmobile.vuuk.network;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.ui.MainActivity;
import io.jmobile.vuuk.ui.MessageActivity;

import static android.content.Context.KEYGUARD_SERVICE;
import static io.jmobile.vuuk.common.Common.ACTIVITY_MAIN;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE_POWER_CONNECT;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE_POWER_DISCONNECT;
import static io.jmobile.vuuk.common.Util.getCurrentActivityName;
import static io.jmobile.vuuk.common.Util.isAppIsInBackground;

public class PowerConnectionReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String name = getCurrentActivityName(context);

        boolean isMain = !TextUtils.isEmpty(name) && name.equalsIgnoreCase(ACTIVITY_MAIN);
        if (intent.getAction() == Intent.ACTION_POWER_CONNECTED) {
            KeyguardManager keyguardManager
                    = (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);
            KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
            lock.disableKeyguard();

            if (LogUtil.DEBUG_MODE)
                Toast.makeText(context, "ACTION_POWER_CONNECTED", Toast.LENGTH_SHORT).show();

            if (isAppIsInBackground(context)) {
                Intent i = new Intent(context, MessageActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(i);
            } else {
                Intent i = new Intent(context, MainActivity.class);
//                if (isMain)
//                    i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                else
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra(ARG_START_TYPE, ARG_START_TYPE_POWER_CONNECT);
                context.startActivity(i);
            }
        } else if (intent.getAction() == Intent.ACTION_POWER_DISCONNECTED) {
            if (!Util.isNetworkAbailable(context)
                    || !Util.getNetworkConnectionType(context).equalsIgnoreCase("WIFI"))
                return;
            else {
                if (isAppIsInBackground(context)) {

                } else {
                    Intent i = new Intent(context, MainActivity.class);
//                    if (isMain)
//                        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    else
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra(ARG_START_TYPE, ARG_START_TYPE_POWER_DISCONNECT);
                    context.startActivity(i);
                }
            }

            if (LogUtil.DEBUG_MODE)
                Toast.makeText(context, "ACTION_POWER_DISCONNECTED", Toast.LENGTH_SHORT).show();
        }
    }
}

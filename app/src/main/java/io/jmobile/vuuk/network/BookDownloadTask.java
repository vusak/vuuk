package io.jmobile.vuuk.network;

import io.jmobile.vuuk.common.FileUtil;
import io.jmobile.vuuk.data.BookItem;

public class BookDownloadTask extends FileUtil.DownloadTask<BookDownloadManager> {
    private static final float PROGRESS_THRESHOLD = 0.01f;
    private BookItem book;
    private float prev;

    public BookDownloadTask(BookDownloadManager manager, BookItem book) {
        super(manager);
        this.book = book;
    }

    @Override
    public void onProgress(long progress, long total) {
        if (ref.get() == null)
            return;

        float downloadProgress = progress / (float) total;
        if (downloadProgress >= prev + PROGRESS_THRESHOLD) {
            ref.get().onBookDownloadProgress(book, progress, total);
            prev = downloadProgress;
        }
    }

    @Override
    public void onSuccess() {
        if (ref.get() == null)
            return;

        ref.get().onBookDownloadSuccess(book);
    }

    @Override
    public void onError(int resultCode) {
        if (ref.get() == null)
            return;

        ref.get().onBookDownloadError(book, resultCode);
    }

    public BookItem getBook() {
        return this.book;
    }
}

package io.jmobile.vuuk.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class JNetworkMonitor extends BroadcastReceiver {
    public static int NETWORK_TYPE_WIFI = 7001;
    public static int NETWORK_TYPE_MOBILE = 7002;
    public static int NETWORK_TYPE_NOT_CONNECTED = 7003;
    private ConnectivityManager connManager = null;
    private OnChangeNetworkStatusListener listener = null;

    public JNetworkMonitor() {

    }

    public JNetworkMonitor(Context context) {
        connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public void setListener(OnChangeNetworkStatusListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (listener == null)
            return;

        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                listener.onNetworkChanged(NETWORK_TYPE_WIFI);
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                listener.onNetworkChanged(NETWORK_TYPE_MOBILE);
        } else
            listener.onNetworkChanged(NETWORK_TYPE_NOT_CONNECTED);
    }

    public interface OnChangeNetworkStatusListener {
        public void onNetworkChanged(int status);
    }
}

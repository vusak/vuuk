package io.jmobile.vuuk.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.widget.Toast;

import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.ui.MainActivity;

import static io.jmobile.vuuk.common.Common.ARG_CURRENT_ACTIVITY;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE_SCREEN_OFF;
import static io.jmobile.vuuk.common.Util.getCurrentActivityName;

public class ScreenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SPController sp = new SPController(context.getApplicationContext());
        if (!sp.isAutoPlaySetting()) {
            return;
        }

        String name = getCurrentActivityName(context);
        if (intent.getAction() == Intent.ACTION_SCREEN_OFF) {

            acquireWakeLock(context);
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(ARG_START_TYPE, ARG_START_TYPE_SCREEN_OFF);
            i.putExtra(ARG_CURRENT_ACTIVITY, name);
            context.startActivity(i);
            if (LogUtil.DEBUG_MODE)
                Toast.makeText(context, "ACTION_SCREEN_OFF", Toast.LENGTH_SHORT).show();
        } else if (intent.getAction() == Intent.ACTION_SCREEN_ON) {
            if (LogUtil.DEBUG_MODE)
                Toast.makeText(context, "ACTION_SCREEN_ON", Toast.LENGTH_SHORT).show();
        }
    }

    private void acquireWakeLock(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wake = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, context.getClass().getName());
        wake.acquire();

        wake.release();
        wake = null;
    }
}

package io.jmobile.vuuk.network;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.jmobile.vuuk.BaseApplication;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.data.BookItem;

public class BookDownloadManager {
    private static Map<String, BookDownloadTask> downloadTasks = Collections.synchronizedMap(new HashMap<String, BookDownloadTask>());
    private ArrayList<String> downloadRequestList = new ArrayList<String>();
    private BaseApplication app;
    private List<BookDownloadListener> listeners = new ArrayList<BookDownloadListener>();

    public BookDownloadManager(BaseApplication baseApplication) {
        app = baseApplication;
        app.getDBController().restateBookDownloadState();
    }

    public void addListener(BookDownloadListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    public void removeListener(BookDownloadListener listener) {
        listeners.remove(listener);
    }

    public boolean addToRequestList(String bookId) {
        boolean result = false;
        synchronized (downloadRequestList) {
            if (!downloadRequestList.contains(bookId))
                result = downloadRequestList.add(bookId);
        }

        if (downloadTasks.size() < 2) {
            BookItem item = app.getDBController().getBookItem(bookId);
            LogUtil.log(item.getBookPath());
            startBookDownload(item, item.getBookDownloadUrl(), false);
        }
        return result;
    }

    public void startBookDownload(BookItem book, String url, boolean continuing) {

        BookDownloadTask t = new BookDownloadTask(this, book);
        synchronized (downloadTasks) {
            downloadTasks.put(book.getBookId(), t);
        }
        if (book != null) {
            if (book.getBookDownloadAt() == 0)
                book.setBookDownloadStartedAt(System.currentTimeMillis());
            book.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_DOWNLOADING);
//
            if (TextUtils.isEmpty(book.getBookPath()))
                book.setBookPath(app.getSPController().getDownloadDirectory());

            app.getDBController().updateBookStates(book);
            t.download(url, book.getBookFile().getAbsolutePath(), continuing, null);

            for (BookDownloadListener listener : listeners)
                listener.onBookDownloadStart(book);
        }
    }

    public void cancelAll() {
        synchronized (downloadTasks) {
            Set<String> keySet = downloadTasks.keySet();
            for (String key : keySet) {
                BookDownloadTask task = downloadTasks.get(key);
                task.cancel(true);
                task.getBook().setBookDownloadStatus(BookItem.DOWNLOAD_STATE_PAUSE);
//                app.getDBController().updateBookStates(task.getBook());
            }

            downloadTasks.clear();
        }
        synchronized (downloadRequestList) {
            downloadRequestList.clear();
        }
    }

    public boolean contains(BookItem book) {
        synchronized (downloadRequestList) {
            return downloadRequestList.contains(book.getBookId());
        }
    }

    public boolean isEmpty() {
        synchronized (downloadRequestList) {
            return downloadRequestList.isEmpty();
        }
    }

    public void removeFromRequestList(String bookId) {
        synchronized (downloadRequestList) {
            downloadRequestList.remove(bookId);
        }
    }

    public void cancelDownload(BookItem book) {
        synchronized (downloadTasks) {
            BookDownloadTask t = downloadTasks.remove(book.getBookId());
            if (t != null)
                t.cancel(true);
        }
        book.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_PAUSE);
        app.getDBController().updateBookStates(book);

        synchronized (downloadRequestList) {
            downloadRequestList.remove(book.getBookId());
        }
    }

    public void onBookDownloadProgress(BookItem book, long progress, long total) {
        synchronized (downloadRequestList) {
            if (!downloadRequestList.contains(book.getBookId()))
                return;
        }

        for (BookDownloadListener listener : listeners)
            listener.onBookDownloadProgress(book, progress, total);

        float downloadProgress = progress / (float) total;
        book.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_DOWNLOADING);
        book.setBookDownloadProgress(downloadProgress);
        app.getDBController().updateBookStates(book);


    }

    public void onBookDownloadSuccess(BookItem book) {
        synchronized (downloadTasks) {
            downloadTasks.remove(book.getBookId());
        }


        removeFromRequestList(book.getBookId());
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (book != null) {
            book.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_DONE);
            book.setBookDownloadAt(System.currentTimeMillis());
            app.getDBController().updateBookStates(book);
        }
        for (BookDownloadListener listener : listeners)
            listener.onBookDownloadSuccess(book);

        int index = 0;
        while (downloadRequestList.size() > index && downloadTasks.size() < 2) {
            BookItem temp = app.getDBController().getBookItem(downloadRequestList.get(index));
            if (temp != null) {
                startBookDownload(temp, temp.getBookDownloadUrl(), false);
                index++;
            }
        }
    }

    public void onBookDownloadError(BookItem book, int resultCode) {
        synchronized (downloadTasks) {
            downloadTasks.remove(book.getBookId());
        }

        book.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_PAUSE);
        app.getDBController().updateBookStates(book);
        for (BookDownloadListener listener : listeners)
            listener.onBookDownloadError(book, resultCode);

    }

    public static interface BookDownloadListener {
        public void onBookDownloadStart(BookItem book);

        public void onBookDownloadProgress(BookItem book, long progress, long total);

        public void onBookDownloadSuccess(BookItem book);

        public void onBookDownloadError(BookItem book, int errorCode);
    }
}

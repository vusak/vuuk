package io.jmobile.vuuk.network;

import android.os.AsyncTask;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.jmobile.vuuk.BaseApplication;
import io.jmobile.vuuk.data.BookItem;

public class CoverDownloadManager {

    private static Map<String, CoverDownloadTask> downloadTasks = Collections.synchronizedMap(new HashMap<String, CoverDownloadTask>());
    private Set<String> downloadRequestList = new HashSet<>();
    private BaseApplication app;
    private List<CoverDownloadListener> listeners = new ArrayList<CoverDownloadListener>();

    public CoverDownloadManager(BaseApplication baseApplication) {
        app = baseApplication;
    }

    public void startCoverDownload(BookItem book, String url) {
        new AsyncTask<Object, Void, Void>() {
            @Override
            protected Void doInBackground(Object... params) {
                BookItem c = (BookItem) params[0];
                CoverDownloadTask t = new CoverDownloadTask(CoverDownloadManager.this, c.getBookId(), c);
                synchronized (downloadTasks) {
                    downloadTasks.put(c.getBookId(), t);
                }
                t.download(c.getBookThumbnailUrl(), c.getBookThumbnailPath(), true, null);
                return null;
            }

        }.execute(book, url);
    }

    public void cancelAll() {
        synchronized (downloadTasks) {
            Set<String> keySet = downloadTasks.keySet();
            for (String key : keySet) {
                CoverDownloadTask task = downloadTasks.get(key);
                task.cancel(true);
            }

            downloadTasks.clear();
        }
        synchronized (downloadRequestList) {
            downloadRequestList.clear();
        }
    }

    public boolean addToRequestList(BookItem book) {
        synchronized (downloadRequestList) {
            if (downloadRequestList.add(book.getBookId())) {
                return true;
            }

            return false;
        }
    }

    public boolean contains(BookItem book) {
        synchronized (downloadRequestList) {
            return downloadRequestList.contains(book.getBookId());
        }
    }

    public boolean isEmpty() {
        synchronized (downloadRequestList) {
            return downloadRequestList.isEmpty();
        }
    }

    public void cancelDownload(BookItem book) {
        synchronized (downloadTasks) {
            CoverDownloadTask t = downloadTasks.remove(book.getBookId());
            if (t != null)
                t.cancel(true);

            File f = new File(book.getBookThumbnailPath());
            if (f != null && f.exists())
                f.delete();
        }
        synchronized (downloadRequestList) {
            downloadRequestList.remove(book.getBookId());
        }
    }

    public void addListener(CoverDownloadListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    public void removeListener(CoverDownloadListener listener) {
        listeners.remove(listener);
    }

    public void onCoverDownloadSuccess(String bookId, final BookItem book) {
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                String cId = params[0];
                synchronized (downloadTasks) {
                    downloadTasks.remove(cId);
                }

                synchronized (downloadRequestList) {
                    downloadRequestList.remove(cId);
                }
                return cId;
            }

            protected void onPostExecute(String result) {
                for (CoverDownloadListener listener : listeners)
                    listener.onCoverDownloadSuccess(result, book);
            }

            ;
        }.execute(bookId);
    }

    public static interface CoverDownloadListener {
        public void onCoverDownloadSuccess(String bookId, BookItem book);
    }
}

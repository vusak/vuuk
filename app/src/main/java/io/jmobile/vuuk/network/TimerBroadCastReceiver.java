package io.jmobile.vuuk.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.jmobile.vuuk.common.Common;

public class TimerBroadCastReceiver extends BroadcastReceiver {
    public static boolean isLaunched = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        isLaunched = true;
//        Toast.makeText(context, "Times up!!!!!" , Toast.LENGTH_SHORT).show();

//        BaseApplication app = (BaseApplication) context.getApplicationContext();
//        SPController sp = app.getSPController();
//        sp.setSettingTimerRelease();

        Intent i = new Intent(Common.INTENTFILTER_BROADCAST_TIMER);
        context.sendBroadcast(i);

    }
}

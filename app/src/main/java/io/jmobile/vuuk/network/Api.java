package io.jmobile.vuuk.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import io.jmobile.vuuk.BaseApplication;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.DBController;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.CategoryItem;
import io.jmobile.vuuk.data.ChannelItem;
import io.jmobile.vuuk.data.CommentItem;

public class Api extends Handler {

    private static RequestQueue requestQueue;
    private static Queue<Message> messageQueue = new LinkedList<Message>();

    private WeakReference<ApiListener> ref;
    private BaseApplication app;
    private SPController sp;
    private DBController db;
    private BaseApplication.ResourceWrapper r;

    public Api(BaseApplication application, ApiListener target) {
        app = application;
        ref = new WeakReference<ApiListener>(target);
        sp = application.getSPController();
        db = application.getDBController();
        r = application.getResourceWrapper();

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(app.getApplicationContext());
    }

    public static void cancelApprequest() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

    public void setTarget(ApiListener target) {
        if (ref != null)
            ref.clear();

        ref = new WeakReference<ApiListener>(target);
    }

    public void versionCheck() {
        Message m = newMessage(Common.API_CODE_POST_VERSION, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_VERSION, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    String version = j_result.optString(Common.TAG_VERSION);
                    m.getData().putString(Common.ARG_VERSION, version);
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }

                m.getData().putBoolean(Common.ARG_RESULT, res);

            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void login(String id, String pw) {
        Message m = newMessage(Common.API_CODE_POST_LOGIN, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_USER_ID, id);
            params.put(Common.PARAM_PASSWORD, pw);
//            params.put(Common.PARAM_USER_ID, "pyunma@jmobile.io");
//            params.put(Common.PARAM_PASSWORD, "12341234");
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_LOGIN, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    sp.clearUserInfo();
                    try {
                        JSONObject obj = j_result.getJSONObject(Common.TAG_USER_INFO);
                        if (obj != null) {
                            String tag = Util.optString(obj, Common.TAG_USER_KEY);
                            sp.setUserInfoKey(tag);
                            sp.setUserInfoID(Util.optString(obj, Common.TAG_USER_ID));
                            sp.setUserInfoName(Util.optString(obj, Common.TAG_USER_NAME));
                            sp.setUserInfoNickname(Util.optString(obj, Common.TAG_NICK_NAME));
                            sp.setUserInfoOrgan(Util.optString(obj, Common.TAG_ORGAN));
                            sp.setUserInfoTimestamp(Util.optString(obj, Common.TAG_TIME_STAMP));
                            sp.setUserInfoProfileImg(Util.optString(obj, Common.TAG_PROFILE_IMG));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }

                m.getData().putBoolean(Common.ARG_LOGIN_RESULT, res);

            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getBookList() {
        Message m = newMessage(Common.API_CODE_POST_BOOK_LIST, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_CHANNEL_ID, sp.getSelectedChannelId());
            if (!TextUtils.isEmpty(sp.getUserInfoKey()))
                params.put(Common.PARAM_USER_KEY, sp.getUserInfoKey());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_BOOK_LIST, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<BookItem> items = new ArrayList<>();
                ArrayList<BookItem> dbItems = new ArrayList<>();
                dbItems.addAll(db.getBookItemByChannel(sp.getSelectedChannelId()));

                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_BOOK_LIST);
                        int max = arr.length();
//                        if (sp.getSelectedChannelId().equalsIgnoreCase("illustrator"))
//                            max = 60;
                        for (int i = 0; i < max; i++) {
                            JSONObject json_c = arr.getJSONObject(i);
                            BookItem item = new BookItem(json_c);
                            if (item != null) {
                                item.setChannelId(sp.getSelectedChannelId());
                                items.add(item);
                            }
                        }

                        for (BookItem bItem : items) {
                            boolean exist = false;
                            for (BookItem temp : dbItems) {
                                if (bItem.getBookId().equalsIgnoreCase(temp.getBookId())) {
//                                    bItem.setBookCommentCount(bItem.getBookCommentCount());
//                                    bItem.setBookLikeCount(bItem.getBookLikeCount());
//                                    bItem.setBookViewCount(bItem.getBookViewCount());
//                                    bItem.setBookLike(bItem.isBookLike());
                                    if (temp.getBookThumbnailUrl().equalsIgnoreCase(bItem.getBookThumbnailUrl()))
                                        bItem.setBookThumbnail(temp.getBookThumbnail());
                                    bItem.setBookFileVersion(TextUtils.isEmpty(temp.getBookFileVersion()) ? "0" : temp.getBookFileVersion());
                                    db.insertOrUpdateBookItem(bItem);
                                    exist = true;
                                    break;
                                }
                            }
                            if (!exist)
                                db.insertOrUpdateBookItem(bItem);

                        }

                        for (BookItem item : dbItems) {
                            boolean exist = false;
                            for (int i = 0; i < items.size(); i++) {
                                if (item.getBookId().equalsIgnoreCase(items.get(i).getBookId())) {
                                    exist = true;
                                    break;
                                }
                            }

                            if (!exist) {
                                db.deleteBookItem(item);
                                db.deletePlaylistItemByBookId(item.getBookId());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message)) {
                        if (message.equalsIgnoreCase(Common.RESULT_CODE_ERROR_NO_DATA)) {
                            for (BookItem item : dbItems) {
                                db.deleteBookItem(item);
                                db.deletePlaylistItemByBookId(item.getBookId());
                            }
                        }
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                    }
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void checkBookFileVersion(String channelId, String bookId) {
        Message m = newMessage(Common.API_CODE_POST_BOOK_FILE_VERSION_CHECK, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_CHANNEL_ID, channelId);
            if (!TextUtils.isEmpty(sp.getUserInfoKey()))
                params.put(Common.PARAM_USER_KEY, sp.getUserInfoKey());
            params.put(Common.PARAM_BOOK_ID, bookId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_BOOK_LIST, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<BookItem> items = new ArrayList<>();

                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_BOOK_LIST);
                        JSONObject json_c = arr.getJSONObject(0);
                        BookItem item = new BookItem(json_c);
                        BookItem dbItem = db.getBookItem(item.getBookId());
                        String oldVersion = dbItem.getBookFileVersion();
                        if (TextUtils.isEmpty(oldVersion))
                            oldVersion = "0";
                        String version = Util.optString(json_c, Common.TAG_BOOK_FILE_VERSION);
                        if (TextUtils.isEmpty(version))
                            version = "0";

                        boolean update = false;
                        try {
                            update = Integer.parseInt(oldVersion) < Integer.parseInt(version);
                        } catch (Exception e) {
                            LogUtil.log(e.getMessage());
                            update = false;
                        }

                        m.getData().putBoolean(Common.ARG_FILE_UPDATE, update);
                        m.getData().putString(Common.ARG_FILE_VERSION, version);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void setBookFileVersion(String channelId, String bookId) {
        Message m = newMessage(Common.API_CODE_POST_BOOK_FILE_VERSION_SAVE, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_CHANNEL_ID, channelId);
            if (!TextUtils.isEmpty(sp.getUserInfoKey()))
                params.put(Common.PARAM_USER_KEY, sp.getUserInfoKey());
            params.put(Common.PARAM_BOOK_ID, bookId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_BOOK_LIST, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<BookItem> items = new ArrayList<>();

                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_BOOK_LIST);
                        JSONObject json_c = arr.getJSONObject(0);
                        BookItem item = new BookItem(json_c);
                        BookItem dbItem = db.getBookItem(item.getBookId());
                        String version = Util.optString(json_c, Common.TAG_BOOK_FILE_VERSION);
                        if (TextUtils.isEmpty(version))
                            version = "0";
                        dbItem.setBookFileVersion(version);
                        db.insertOrUpdateBookItem(dbItem);

                        m.getData().putParcelable(Common.ARG_BOOK_ITEM, item);
                        m.getData().putString(Common.ARG_FILE_VERSION, version);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getChannelList() {
        Message m = newMessage(Common.API_CODE_POST_CHANNEL_LIST, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_CHANNEL_LIST, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<ChannelItem> items = new ArrayList<>();
                ArrayList<ChannelItem> dbItems = new ArrayList<>();
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    dbItems.addAll(db.getChannelItems());
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_SITE_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject json_c = arr.getJSONObject(i);
                            ChannelItem item = new ChannelItem(json_c);
                            if (item != null) {
                                if (sp.isFirstOpen() && item.getChannelId().equalsIgnoreCase(SPController.DEFAULT_SELECTED_CHANNEL_ID)) {
                                    item.setChannelJoined(true);
                                    item.setChannelAt(System.currentTimeMillis());
                                }
                                items.add(item);
                            }
                        }

                        for (ChannelItem bItem : items) {
                            for (ChannelItem temp : dbItems) {
                                if (bItem.getChannelId().equalsIgnoreCase(temp.getChannelId())) {
                                    bItem.setChannelJoined(temp.isChannelJoined());
                                    bItem.setChannelAt(temp.getChannelAt());
                                    break;
                                }
                            }
                            db.insertOrUpdateChannelItem(bItem);
                        }

                        for (ChannelItem item : dbItems) {
                            boolean exist = false;
                            for (int i = 0; i < items.size(); i++) {
                                if (item.getChannelId().equalsIgnoreCase(items.get(i).getChannelId())) {
                                    exist = true;
                                    break;
                                }
                            }
                            if (!exist) {
                                db.deleteChannelItem(item);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }

                m.getData().putBoolean(Common.ARG_RESULT, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getCategoryList(final String channelId) {
        Message m = newMessage(Common.API_CODE_POST_CATEGORY_LIST, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_CHANNEL_ID, channelId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_CATEGORY_LIST, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<CategoryItem> items = new ArrayList<>();
                ArrayList<CategoryItem> dbItems = new ArrayList<>();
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    dbItems.addAll(db.getCategoryItemsByChannel(channelId));
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_CATEGORY_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject json_c = arr.getJSONObject(i);
                            CategoryItem item = new CategoryItem(json_c);
                            item.setCategoryChannelId(channelId);
                            if (item != null) {
                                items.add(item);
                            }
                        }

                        for (CategoryItem item : dbItems) {
                            boolean exist = false;
                            for (int i = 0; i < items.size(); i++) {
                                if (item.getCategoryId().equalsIgnoreCase(items.get(i).getCategoryId())) {
                                    exist = true;
                                    break;
                                }
                            }
                            if (!exist)
                                db.deleteCategoryItem(item);
                        }

                        for (CategoryItem bItem : items) {
                            db.insertOrUpdateCategoryItem(bItem);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getBookCommentList(String bookId) {
        Message m = newMessage(Common.API_CODE_POST_COMMENT_LIST, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_BOOK_ID, bookId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_COMMENT_LIST, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<CommentItem> items = new ArrayList<>();
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_COMMENT_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject json_c = arr.getJSONObject(i);
                            CommentItem item = new CommentItem(json_c);
                            if (item != null) {
                                items.add(item);
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
                m.getData().putParcelableArrayList(Common.ARG_COMMENT_LIST, items);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void addBookComment(String bookId, String comment) {
        Message m = newMessage(Common.API_CODE_POST_ADD_COMMENT, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_BOOK_ID, bookId);
            params.put(Common.PARAM_USER_KEY, sp.getUserInfoKey());
            params.put(Common.PARAM_BOOK_COMMENT, comment);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_ADD_COMMENT, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {

                ArrayList<CommentItem> items = new ArrayList<>();
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_COMMENT_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject json_c = arr.getJSONObject(i);
                            CommentItem item = new CommentItem(json_c);
                            if (item != null) {
                                items.add(item);
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
                m.getData().putParcelableArrayList(Common.ARG_COMMENT_LIST, items);

//                m.getData().putBoolean(Common.ARG_CATEGORY_UPDATE, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void deleteBookComment(String commentId) {
        Message m = newMessage(Common.API_CODE_POST_DEL_COMMENT, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_BOOK_COMMENT_ID, commentId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_DEL_COMMENT, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {

                ArrayList<CommentItem> items = new ArrayList<>();
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_COMMENT_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject json_c = arr.getJSONObject(i);
                            CommentItem item = new CommentItem(json_c);
                            if (item != null) {
                                items.add(item);
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
                m.getData().putParcelableArrayList(Common.ARG_COMMENT_LIST, items);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void checkBookLike(final String bookId) {
        Message m = newMessage(Common.API_CODE_POST_SET_LIKES, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_BOOK_ID, bookId);
            params.put(Common.PARAM_USER_KEY, sp.getUserInfoKey());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_SET_LIKES, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                BookItem item = db.getBookItem(bookId);
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_BOOK_LIST);
                        JSONObject obj = arr.getJSONObject(0);
                        if (obj != null) {
                            item.setBookViewCount(Util.optString(obj, Common.TAG_BOOK_VIEW_COUNT));
                            item.setBookLikeCount(Util.optString(obj, Common.TAG_BOOK_LIKE_COUNT));
                            item.setBookCommentCount(Util.optString(obj, Common.TAG_BOOK_COMMENT_COUNT));
                            item.setBookLike(Util.optString(obj, Common.TAG_BOOK_IS_LIKE).equals("1"));

                            db.updateBookCountState(item);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getBookCountStatus(final String bookId) {
        Message m = newMessage(Common.API_CODE_POST_BOOK_RECOUNT, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_TYPE, Common.TYPE);
            params.put(Common.PARAM_VERSION, String.valueOf(Common.VERSION));
            params.put(Common.PARAM_BOOK_ID, bookId);
            if (!TextUtils.isEmpty(sp.getUserInfoKey()))
                params.put(Common.PARAM_USER_KEY, sp.getUserInfoKey());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_POST_BOOK_RECOUNT, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                BookItem item = db.getBookItem(bookId);
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_BOOK_LIST);
                        JSONObject obj = arr.getJSONObject(0);
                        if (obj != null) {
                            item.setBookViewCount(Util.optString(obj, Common.TAG_BOOK_VIEW_COUNT));
                            item.setBookLikeCount(Util.optString(obj, Common.TAG_BOOK_LIKE_COUNT));
                            item.setBookCommentCount(Util.optString(obj, Common.TAG_BOOK_COMMENT_COUNT));
                            String like = Util.optString(obj, Common.TAG_BOOK_IS_LIKE);
                            if (!TextUtils.isEmpty(like))
                                item.setBookLike(like.equals("1"));

                            db.updateBookCountState(item);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = j_result.optString(Common.TAG_MESSAGE);
                    if (!TextUtils.isEmpty(message))
                        m.getData().putString(Common.ARG_ERROR_MESSAGE, message);
                }
                m.getData().putBoolean(Common.ARG_RESULT, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    private Message newMessage(int apiCode, String progressMessage, boolean showErrorMessage) {
        Message m = Message.obtain(this, apiCode);
        Bundle b = new Bundle();
        b.putString(Common.PARAM_PROGRESS_MESSAGE, progressMessage);
        b.putBoolean(Common.PARAM_SHOW_ERROR_MESSAGE, showErrorMessage);
        m.setData(b);

        return m;
    }

    @Override
    public void handleMessage(Message msg) {
        if (ref == null || ref.get() == null)
            return;

        ref.get().handleApiMessage(msg);
    }

    public static interface ApiListener {
        public void handleApiMessage(Message m);

        public BaseActivity getApiActivity();

        public FragmentManager getApiFragmentManager();
    }

    private abstract static class ApiRequest implements Response.Listener<String>, Response.ErrorListener {
        protected Api api;
        protected Message m;
        protected boolean addAuthorization;
        protected StringRequest request;
        protected String paramString;

        public ApiRequest(Api _api, Message _m, int method, String _url, final Map<String, String> _params, final boolean addAuthorizationToken) {
            this.api = _api;
            this.m = _m;
            this.addAuthorization = addAuthorizationToken;

//            JSONObject j_params = null;
            if (method == Request.Method.GET) {
                if (_params != null && _params.size() > 0) {
                    Object[] temp = new Object[_params.size()];
                    Set<String> keySet = _params.keySet();
                    int i = 0;
                    for (String key : keySet) {
                        try {
                            temp[i] = key + "=" + URLEncoder.encode(String.valueOf(_params.get(key)), "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            temp[i] = key + "=" + String.valueOf(_params.get(key));
                        }
                        i++;
                    }

                    _url += "?" + TextUtils.join("&", temp);
                }
            }
//            else if (params != null) {
////                j_params = new JSONObject(params);
////                paramString = j_params.toString();
//            }

            request = new StringRequest(method, _url, this, this) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = super.getHeaders();
                    if (headers == null || headers.equals(Collections.emptyMap()))
                        headers = new Hashtable<String, String>();

                    headers.put(Common.HEADER_CACHE_CONTROL, Common.HEADER_NO_CACHE);
                    headers.put(Common.HEADER_APPLICATION_VERSION, api.app.getVersion());
                    headers.put(Common.HEADER_OS, api.app.getOS());
//                    headers.put(Common.HEADER_DEVICE_IDENTIFIER, api.app.getDeviceIdentifier());
//                    if (addAuthorizationToken)
//                        headers.put(Common.HEADER_ACCESS_TOKEN, api.sp.getAccessToken());

                    if (LogUtil.DEBUG_MODE) {
                        StringBuilder sb = new StringBuilder();
                        for (String key : headers.keySet()) {
                            sb.append(key)
                                    .append(": ")
                                    .append(headers.get(key))
                                    .append("\n");
                        }
                        LogUtil.log("API_REQUEST", "HEADER===\n" + sb.toString());
                    }
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = super.getParams();
                    if (params == null || params.equals(Collections.emptyMap()))
                        params = new Hashtable<String, String>();

                    params.putAll(_params);

                    return params;
                }
            };

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(Common.TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            if (LogUtil.DEBUG_MODE) {
                LogUtil.log("API_REQUEST", "URL : " + request.getUrl());
                try {
                    if (request.getBody() != null)
                        LogUtil.log("API_REQUEST", "BODY : " + new String(request.getBody()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        abstract public void onSuccess(String result);

        abstract public void onSuccess(JSONObject j_result);

        abstract public void onError();

        @Override
        public void onResponse(String response) {
            if (response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }

//            try {
//                response = new String(response.getBytes("8859_1"), "utf-8");
//                LogUtil.log("UTF-8 " + response);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            if (LogUtil.DEBUG_MODE && response != null)
                LogUtil.log("API_REQUEST", "RESPONSE : " + response.toString());

//            if (m.what == Common.API_CODE_GET_TREND_LIST_WORLDWIDE
//                    || m.what == Common.API_CODE_GET_TREND_LEARNMORE
//                    || m.what == Common.API_CODE_GET_REPORT_LEARNMORE) {
//                onReceiveResponse(null, response, null);
//            } else {
            JSONObject j_response = null;
            try {
                j_response = new JSONObject(response);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (j_response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }
            onReceiveResponse(j_response, null, null);
//            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;

            if (error.networkResponse == null)
                m.arg2 = Common.RESULT_CODE_ERROR_UNKNOWN;
            else
                m.arg2 = error.networkResponse.statusCode;

            onReceiveResponse(null, null, String.valueOf(m.arg2));
        }

        public void ajax() {
            Api.requestQueue.add(request);
            synchronized (messageQueue) {
                messageQueue.add(m);
            }
        }

        private void onReceiveResponse(JSONObject j_result, String result, String errorCode) {
            synchronized (messageQueue) {
                messageQueue.remove(m);
            }

            new ResponseTask(this, j_result, result, errorCode).run();
        }
    }

    private static class ResponseTask extends AsyncTask<Void, Void, Void> {
        protected ApiRequest request;
        protected JSONObject j_result;
        protected String result;
        protected String errorCode;

        ResponseTask(ApiRequest request, JSONObject j_result, String result, String errorCode) {
            this.request = request;
            this.j_result = j_result;
            this.result = result;
            this.errorCode = errorCode;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (j_result != null)
                request.onSuccess(j_result);
            else if (result != null)
                request.onSuccess(result);
            else
                request.onError();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            request.api.sendMessage(request.m);
        }

        public void run() {
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        }
    }
}

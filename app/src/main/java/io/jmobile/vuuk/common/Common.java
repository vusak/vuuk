package io.jmobile.vuuk.common;

public class Common {
    public static final String TYPE = "a";
    public static final int VERSION = 1;
    // HEADER_**
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_NO_CACHE = "no-cache";
    public static final String HEADER_APPLICATION_VERSION = "APPLICATION_VERSION";
    public static final String HEADER_OS = "OS";
    public static final int TIMEOUT = 20000;
    public static final String BASE_URL = "https://api.vuuk.kr/api/";
    public static final String API_URL_POST_LOGIN = "loginProc";
    public static final String API_URL_POST_BOOK_INFO = "books/";
    public static final String API_URL_POST_BOOK_LIST = "books";
    public static final String API_URL_POST_CHANNEL_LIST = "sites";
    public static final String API_URL_POST_CATEGORY_LIST = "category";
    public static final String API_URL_POST_COMMENT_LIST = "books/comments";
    public static final String API_URL_POST_ADD_COMMENT = "books/comments/a";
    public static final String API_URL_POST_DEL_COMMENT = "books/comments/d";
    public static final String API_URL_POST_SET_LIKES = "books/likes";
    public static final String API_URL_POST_BOOK_RECOUNT = "books/recount";
    public static final String API_URL_POST_VERSION = "v";

    public static final int API_CODE_POST_LOGIN = 20001;
    public static final int API_CODE_POST_BOOK_INFO = 20002;
    public static final int API_CODE_POST_BOOK_LIST = 20003;
    public static final int API_CODE_POST_CHANNEL_LIST = 20004;
    public static final int API_CODE_POST_CATEGORY_LIST = 20005;
    public static final int API_CODE_POST_COMMENT_LIST = 20006;
    public static final int API_CODE_POST_ADD_COMMENT = 20007;
    public static final int API_CODE_POST_DEL_COMMENT = 20008;
    public static final int API_CODE_POST_SET_LIKES = 20009;
    public static final int API_CODE_POST_BOOK_RECOUNT = 20010;
    public static final int API_CODE_POST_VERSION = 20011;
    public static final int API_CODE_POST_BOOK_FILE_VERSION_CHECK = 20012;
    public static final int API_CODE_POST_BOOK_FILE_VERSION_SAVE = 20013;

    // RESULT_CODE_**
    public static final int RESULT_CODE_ERROR_OK = 0;
    public static final int RESULT_CODE_ERROR_NO_RESPONSE = -1;
    public static final int RESULT_CODE_ERROR_UNKNOWN = -2;
    public static final int RESULT_CODE_ERROR_CANCELED = -3;
    public static final String PARAM_PROGRESS_MESSAGE = "progressMessage";
    public static final String PARAM_SHOW_ERROR_MESSAGE = "showErrorMessage";
    public static final String RESULT_CODE_ERROR_NO_DATA = "E01";
    public static final String RESULT_CODE_ERROR_VERSION = "E98";
    public static final String RESULT_CODE_ERROR_LOGIN_FAIL = "E99";

    public static final String PARAM_USER_ID = "userid";
    public static final String PARAM_PASSWORD = "passwd";
    public static final String PARAM_USER_KEY = "person_objid";
    public static final String PARAM_CHANNEL_ID = "site_id";
    public static final String PARAM_BOOK_ID = "book_objid";
    public static final String PARAM_BOOK_COMMENT = "comment";
    public static final String PARAM_BOOK_COMMENT_ID = "comment_objid";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_VERSION = "version";

    public static final String TAG_RESULT = "result";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_RESULT_SUCCESS = "success";
    public static final String TAG_USER_INFO = "userInfo";
    public static final String TAG_USER_KEY = "person_objid";
    public static final String TAG_USER_ID = "userid";
    public static final String TAG_USER_NAME = "user_name";
    public static final String TAG_NICK_NAME = "nickname";
    public static final String TAG_PROFILE_IMG = "profile_img";
    public static final String TAG_ORGAN = "organ";
    public static final String TAG_TIME_STAMP = "timestamp";

    public static final String TAG_BOOK_LIST = "bookList";
    public static final String TAG_BOOK_OBJ_ID = "OBJID";
    public static final String TAG_BOOK_TAGS = "TAGS";
    public static final String TAG_BOOK_SUMMARY = "SUMMARY";
    public static final String TAG_BOOK_VIEW_COUNT = "VIEW_COUNT";
    public static final String TAG_BOOK_LIKE_COUNT = "LIKE_COUNT";
    public static final String TAG_BOOK_COMMENT_COUNT = "COMMENT_COUNT";
    public static final String TAG_BOOK_TITLE = "TITLE";
    public static final String TAG_BOOK_AUTHOR = "AUTHOR";
    public static final String TAG_BOOK_PUBLISHER = "PUBLISHER";
    public static final String TAG_BOOK_PUBLISHED_DATE = "PUBLISHED_DATE";
    public static final String TAG_BOOK_LANGUAGE = "LANGUAGE";
    public static final String TAG_BOOK_DOCUMENT_FORMAT = "DOCUMENT_FORMAT";
    public static final String TAG_BOOK_SHORT_URL = "SHORT_URL";
    public static final String TAG_BOOK_CREATE_USER = "CREATE_USER";
    public static final String TAG_BOOK_CREATE_DATE = "CREATE_DATE";
    public static final String TAG_BOOK_MODIFY_DATE = "MODIFY_DATE";
    public static final String TAG_BOOK_SHARE = "SHARE";
    public static final String TAG_BOOK_CATEGORY_CODE = "CATEGORY_CODE";
    //    public static final String TAG_BOOK_CATEGORY_VALUE_JA = "CATEGORY_VALUE_JA";
    public static final String TAG_BOOK_CATEGORY_VALUE_KO = "CATEGORY_VALUE_KO";
    public static final String TAG_BOOK_PERSON_OBJID = "PERSON_OBJID";
    public static final String TAG_BOOK_USER_NAME = "USER_NAME";
    public static final String TAG_BOOK_NICKNAME = "NICKNAME";
    public static final String TAG_BOOK_ABOUT_CONTENT = "ABOUT_CONTENT";
    public static final String TAG_BOOK_FILE_SIZE = "FILE_SIZE";
    public static final String TAG_BOOK_THUMBNAIL_URL = "THUMBNAIL_URL";
    public static final String TAG_BOOK_DOWNLOAD_URL = "DOWNLOAD_URL";
    public static final String TAG_BOOK_VIEWER_URL = "VIEWER_URL";
    public static final String TAG_BOOK_PROFILE_URL = "PROFILE_URL";
    public static final String TAG_BOOK_IS_LIKE = "IS_LIKE";
    public static final String TAG_BOOK_FILE_VERSION = "DOC_VERSION";

    public static final String TAG_SITE_LIST = "siteList";
    public static final String TAG_CHANNEL_ID = "SITE_ID";
    public static final String TAG_CHANNEL_NAME = "SITE_NAME";
    public static final String TAG_DOMAIN = "DOMAIN";
    public static final String TAG_LOGO_TYPE = "LOGO_TYPE";
    public static final String TAG_LOGO_URL = "LOGO_URL";
    public static final String TAG_USE_CATEGORY = "YN_CATEGORY";

    public static final String TAG_CATEGORY_LIST = "categoryList";
    public static final String TAG_CATEGORY_NAME = "CATEGORY_NAME";
    public static final String TAG_CATEGORY_CODE = "CATEGORY_CODE";

    public static final String TAG_COMMENT_LIST = "commentList";
    public static final String TAG_COMMENT_ID = "OBJID";
    public static final String TAG_COMMENT_USER_NAME = "PERSON_NAME";
    public static final String TAG_COMMENT_USER_ID = "CREATE_USER";
    public static final String TAG_COMMENT_USER_PROFILE_URL = "PROFILE_URL";
    public static final String TAG_COMMENT_VALUE = "COMMENT";
    public static final String TAG_COMMENT_LIKE_COUNT = "LIKE_COUNT";
    public static final String TAG_COMMENT_DISLIKE_COUNT = "DISLIKE_COUNT";
    public static final String TAG_COMMENT_DATE = "CREATE_DATE";

    public static final String TAG_VERSION = "version";

    public static final String ARG_RESULT = "result";
    public static final String ARG_LOGIN_RESULT = "login_result";
    public static final String ARG_COMMENT_LIST = "comment_list";
    public static final String ARG_CONNECT_POWER = "connect_power";
    public static final String ARG_DISCONNECT_POWER = "disconnect_power";

    public static final String ARG_START_TYPE = "start_type";
    public static final String ARG_START_TYPE_POWER_CONNECT = "type_power_connect";
    public static final String ARG_START_TYPE_POWER_DISCONNECT = "type_power_disconnect";
    public static final String ARG_START_TYPE_SCREEN_OFF = "type_screen_off";
    public static final String ARG_EPUB_URL = "epub_url";
    public static final String ARG_PDF_URL = "pdf_url";
    public static final String ARG_VIDEO_URL = "video_url";
    public static final String ARG_SLEEP_MODE = "sleep_mode";
    public static final String ARG_MULTI_MODE = "multi_mode";
    public static final String ARG_CURRENT_ACTIVITY = "current_activity";
    public static final String ARG_ERROR_MESSAGE = "error_message";
    public static final String ARG_FILE_VERSION = "file_version";
    public static final String ARG_BOOK_ITEM = "book_item";
    public static final String ARG_FILE_UPDATE = "file_update";

    public static final String ARG_VERSION = "version_check";

    public static final String INTENTFILTER_BROADCAST_TIMER = "broadcast_timer";

    public static final String ACTIVITY_VIDEO = ".ui.VideoViewActivity";
    public static final String ACTIVITY_EPUB = ".ui.EpubViewActivity";
    public static final String ACTIVITY_PDF = ".ui.PDFViewActivity";
    public static final String ACTIVITY_MAIN = ".ui.MainActivity";
}

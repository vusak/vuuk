package io.jmobile.vuuk.common;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.CategoryItem;
import io.jmobile.vuuk.data.ChannelItem;
import io.jmobile.vuuk.data.PlayListItem;

public class DBController {
    public static final String DB_ID = "_id";

    private static final String DB_NAME = "vuuk.db";
    private static final int DB_VERSION = 2;
    private static DBHelper helper;
    private static SQLiteDatabase db;
    private Context context;

    public DBController(Context context) {
        this.context = context;

        if (helper == null) {
            helper = new DBHelper(context.getApplicationContext());
            db = helper.getWritableDatabase();
        }
    }

    public synchronized long getNextDBId(String tableName) {
        long id = -1;

        Cursor cursor = db.rawQuery("select seq from SQLITE_SEQUENCE where name = ?", new String[]{tableName});
        if (cursor.moveToFirst())
            id = cursor.getLong(cursor.getColumnIndex("seq"));
        cursor.close();

        return id + 1;
    }

    public synchronized ChannelItem getChannelItem(String channelId) {
        return ChannelItem.getChannelItem(db, channelId);
    }

    public synchronized List<ChannelItem> getChannelItems() {
        return ChannelItem.getChannelItems(db, null, null, null, null, null, null, null);
    }

    public synchronized List<ChannelItem> getJoinedChannelItems() {
        return ChannelItem.getJoinedChannelItems(db);
    }

    public synchronized List<ChannelItem> getChannelItems(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        return ChannelItem.getChannelItems(db, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    public synchronized boolean insertOrUpdateChannelItem(ChannelItem item) {
        return ChannelItem.insertOrUpdateChannelItem(db, item);
    }

    public synchronized boolean updateChannelJoined(ChannelItem item) {
        return ChannelItem.updateChannelJoined(db, item);
    }

    public synchronized boolean deleteChannelItem(ChannelItem item) {
        return ChannelItem.deleteChannelItem(db, item);
    }

    public synchronized boolean deleteAllChannelItems() {
        return ChannelItem.deleteAllChannelItems(db);
    }

    public synchronized BookItem getBookItem(String bookId) {
        return BookItem.getBookItem(db, bookId);
    }

    public synchronized List<BookItem> getBookItemsByCategory(String categoryId) {
        return BookItem.getBookItemsByCategory(db, categoryId);
    }

    public synchronized List<BookItem> getBookItemByChannel(String channelId) {
        return BookItem.getBookItemsByChannel(db, channelId);
    }

    public synchronized List<BookItem> getBookItems() {
        return BookItem.getBookItems(db, null, null, null, null, null, null, null);
    }

    public synchronized List<BookItem> getBookItems(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        return BookItem.getBookItems(db, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    public synchronized boolean insertOrUpdateBookItem(BookItem item) {
        return BookItem.insertOrUpdateBookItem(db, item);
    }

    public synchronized boolean deleteBookItem(BookItem item) {
        return BookItem.deleteBook(context, db, item);
    }

    public synchronized boolean deleteAllBookItems() {
        return BookItem.deleteAllBookItems(db);
    }

    public synchronized boolean restateBookDownloadState() {
        return BookItem.restateBookDownloadState(db);
    }

    public synchronized boolean updateBookStates(BookItem item) {
        return BookItem.updateBookStatus(db, item);
    }

    public synchronized boolean updateBookCountState(BookItem item) {
        return BookItem.updateBookCountState(db, item);
    }

    public synchronized CategoryItem getCategoryItem(String id) {
        return CategoryItem.getCategoryItem(db, id);
    }

    public synchronized List<CategoryItem> getCategoryItems() {
        return CategoryItem.getCategoryItems(db, null, null, null, null, null, null, null);
    }

    public synchronized List<CategoryItem> getCategoryItemsByChannel(String channelId) {
        return CategoryItem.getCategoryItemsByChannel(db, channelId);
    }

    public synchronized boolean insertOrUpdateCategoryItem(CategoryItem item) {
        return CategoryItem.insertOrUpdateCategoryItem(db, item);
    }

    public synchronized boolean deleteCategoryItem(CategoryItem item) {
        return CategoryItem.deleteCategoryItem(db, item);
    }

    public synchronized List<PlayListItem> getPlaylistItems() {
        return PlayListItem.getPlaylistItems(db, null, null, null, null, null, PlayListItem.PLAYLIST_IDX + " ASC", null);
    }

    public synchronized PlayListItem getPlaylistItem(String id) {
        return PlayListItem.getPlaylistItem(db, id);
    }

    public synchronized boolean insertOrupdatePlaylistItem(PlayListItem item) {
        return PlayListItem.insertOrUpdatePlaylistItem(db, item);
    }

    public synchronized boolean deletePlaylistItem(PlayListItem item) {
        return PlayListItem.deletePlaylistItem(db, item);
    }

    public synchronized boolean updatePlaylistIndex(PlayListItem item) {
        return PlayListItem.updatePlaylistIndex(db, item);
    }

    public synchronized boolean deleteAllPlaylistItems() {
        return PlayListItem.deleteAllPlaylistItems(db);
    }

    public synchronized boolean deletePlaylistItemByBookId(String bookId) {
        return PlayListItem.deletePlaylistItemByBookId(db, bookId);
    }

    private static class DBHelper extends SQLiteOpenHelper {
        DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // create tables
            createAllTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            switch (oldVersion) {
                case 1:
                    PlayListItem.createTablePlaylist(db);
                    break;
            }
        }


        private void dropAllTables(SQLiteDatabase db) {

        }

        private void createAllTables(SQLiteDatabase db) {
            BookItem.createTableBook(db);
            ChannelItem.createTableChannel(db);
            CategoryItem.createTableCategory(db);
            PlayListItem.createTablePlaylist(db);
        }
    }
}

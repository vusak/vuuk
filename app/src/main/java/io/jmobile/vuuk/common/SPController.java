package io.jmobile.vuuk.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

import io.jmobile.vuuk.data.BookItem;

public class SPController {

    public static final String FIRST_OPEN = "first_open";
    public static final boolean DEFAULT_FIRST_OPEN = true;

    public static final String SELECTED_CHANNEL_ID = "selected_channel_id";
    public static final String DEFAULT_SELECTED_CHANNEL_ID = "demo";

    public static final String DOWNLOAD_DIRECTORY = "download_directory";
    public static final String DEFAULT_DOWNLOAD_DIRECTORY = "";

    public static final String BOOK_ORDER = "book_order";
    public static final String DEFAULT_BOOK_ORDER = BookItem.BOOK_ORDER_NEW;
    public static final String BOOK_TYPE = "book_type";
    public static final String DEFAULT_BOOK_TYPE = BookItem.TYPE_ALL;
    public static final String BOOK_CATEGORY = "book_category";
    public static final String DEFAULT_BOOK_CATEGORY = "";


    public static final String USER_INFO_KEY = "user_info_key";
    public static final String DEFAULT_USER_INFO_KEY = "";
    public static final String USER_INFO_ID = "user_info_id";
    public static final String DEFAULT_USER_INFO_ID = "";
    public static final String USER_INFO_NAME = "user_info_name";
    public static final String DEFAULT_USER_INFO_NAME = "";
    public static final String USER_INFO_NICKNAME = "user_info_nickname";
    public static final String DEFAULT_USER_INFO_NICKNAME = "";
    public static final String USER_INFO_TIMESTAMP = "user_info_timestamp";
    public static final String DEFAULT_USER_INFO_TIMESTAMP = "";
    public static final String USER_INFO_PROFILE_IMG = "user_info_profile_img";
    public static final String DEFAULT_USER_INFO_PROFILE_IMG = "";
    public static final String USER_INFO_ORGAN = "user_info_organ";
    public static final String DEFAULT_USER_INFO_ORGAN = "";

    public static final String AUTO_DOWNLOAD_START = "auto_download_start";
    public static final boolean DEFAULT_AUTO_DOWNLOAD_START = false;
    public static final String AUTO_PLAY_SETTING = "auto_play_setting";
    public static final boolean DEFAULT_AUTO_PLAY_SETTING = false;
    public static final String AUTO_PLAY_CHANNEL = "auto_play_channel";
    public static final String DEFAULT_AUTO_PLAY_CHANNEL = "demo";
    public static final String AUTO_PLAY_CONTENT = "auto_play_content";
    public static final String DEFAULT_AUTO_PLAY_CONTENT = "";
    public static final String AUTO_PLAY_TIMER = "auto_play_timer";
    public static final long DEFAULT_AUTO_PLAY_TIMER = 0;
    public static final String AUTO_PLAY_START_TIME = "auto_play_start_time";
    public static final long DEFAULT_AUTO_PLAY_START_TIME = 0;
    public static final String AUTO_PLAY_CURRENT_URL = "auto_play_current_url";
    public static final String DEFAULT_AUTO_PLAY_CURRENT_URL = "";
    public static final String PLAY_TURNING_INTERVAL = "play_turning_interval";
    public static final int DEFAULT_PLAY_TURNING_INTERVAL = 10;
    public static final String AUTO_START = "auto_start";
    public static final boolean DEFAULT_AUTO_START = false;

    protected SharedPreferences sp;

    public SPController(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isFirstOpen() {
        return sp.getBoolean(FIRST_OPEN, DEFAULT_FIRST_OPEN);
    }

    public void setFirstOpen(boolean val) {
        put(FIRST_OPEN, val);
    }

    public String getSelectedChannelId() {
        return sp.getString(SELECTED_CHANNEL_ID, DEFAULT_SELECTED_CHANNEL_ID);
    }

    public void setSelectedChannelId(String val) {
        put(SELECTED_CHANNEL_ID, val);
    }

    public String getDownloadDirectory() {
        return sp.getString(DOWNLOAD_DIRECTORY, DEFAULT_DOWNLOAD_DIRECTORY);
    }

    public void setDownloadDirectory(String val) {
        put(DOWNLOAD_DIRECTORY, val);
    }

    public String getBookOrder() {
        return sp.getString(BOOK_ORDER, DEFAULT_BOOK_ORDER);
    }

    public void setBookOrder(String val) {
        put(BOOK_ORDER, val);
    }

    public String getBookType() {
        return sp.getString(BOOK_TYPE, DEFAULT_BOOK_TYPE);
    }

    public void setBookType(String val) {
        put(BOOK_TYPE, val);
    }

    public String getUserInfoKey() {
        return sp.getString(USER_INFO_KEY, DEFAULT_USER_INFO_KEY);
    }

    public void setUserInfoKey(String val) {
        put(USER_INFO_KEY, val);
    }

    public String getUserInfoID() {
        return sp.getString(USER_INFO_ID, DEFAULT_USER_INFO_ID);
    }

    public void setUserInfoID(String val) {
        put(USER_INFO_ID, val);
    }

    public String getUserInfoName() {
        return sp.getString(USER_INFO_NAME, DEFAULT_USER_INFO_NAME);
    }

    public void setUserInfoName(String val) {
        put(USER_INFO_NAME, val);
    }

    public String getUserInfoNickname() {
        return sp.getString(USER_INFO_NICKNAME, DEFAULT_USER_INFO_NICKNAME);
    }

    public void setUserInfoNickname(String val) {
        put(USER_INFO_NICKNAME, val);
    }

    public String getUserInfoTimestamp() {
        return sp.getString(USER_INFO_TIMESTAMP, DEFAULT_USER_INFO_TIMESTAMP);
    }

    public void setUserInfoTimestamp(String val) {
        put(USER_INFO_TIMESTAMP, val);
    }

    public String getUserInfoProfileImg() {
        return sp.getString(USER_INFO_PROFILE_IMG, DEFAULT_USER_INFO_PROFILE_IMG);
    }

    public void setUserInfoProfileImg(String val) {
        put(USER_INFO_PROFILE_IMG, val);
    }

    public String getUserInfoOrgan() {
        return sp.getString(USER_INFO_ORGAN, DEFAULT_USER_INFO_ORGAN);
    }

    public void setUserInfoOrgan(String val) {
        put(USER_INFO_ORGAN, val);
    }

    public String getBookCategory() {
        return sp.getString(BOOK_CATEGORY, DEFAULT_BOOK_CATEGORY);
    }

    public void setBookCategory(String val) {
        put(BOOK_CATEGORY, val);
    }

    public boolean isAutoDownloadStart() {
        return sp.getBoolean(AUTO_DOWNLOAD_START, DEFAULT_AUTO_DOWNLOAD_START);
    }

    public void setAutoDownloadStart(boolean val) {
        put(AUTO_DOWNLOAD_START, val);
    }

    public boolean isAutoPlaySetting() {
        return sp.getBoolean(AUTO_PLAY_SETTING, DEFAULT_AUTO_PLAY_SETTING);
    }

    public void setAutoPlaySetting(boolean val) {
        put(AUTO_PLAY_SETTING, val);
    }

    public String getAutoPlayChannel() {
        return sp.getString(AUTO_PLAY_CHANNEL, getSelectedChannelId());
    }

    public void setAutoPlayChannel(String val) {
        put(AUTO_PLAY_CHANNEL, val);
    }

    public String getAutoPlayContent() {
        return sp.getString(AUTO_PLAY_CONTENT, DEFAULT_AUTO_PLAY_CONTENT);
    }

    public void setAutoPlayContent(String val) {
        put(AUTO_PLAY_CONTENT, val);
    }

    public long getAutoPlayTimer() {
        return sp.getLong(AUTO_PLAY_TIMER, DEFAULT_AUTO_PLAY_TIMER);
    }

    public void setAutoPlayTimer(long val) {
        put(AUTO_PLAY_TIMER, val);
    }

    public String getAutoPlayCurrentUrl() {
        return sp.getString(AUTO_PLAY_CURRENT_URL, DEFAULT_AUTO_PLAY_CURRENT_URL);
    }

    public void setAutoPlayCurrentUrl(String val) {
        put(AUTO_PLAY_CURRENT_URL, val);
    }

    public long getAutoPlayStartTime() {
        return sp.getLong(AUTO_PLAY_START_TIME, DEFAULT_AUTO_PLAY_START_TIME);
    }

    public void setAutoPlayStartTime(long val) {
        put(AUTO_PLAY_START_TIME, val);
    }

    public int getPlayTurningInterval() {
        return sp.getInt(PLAY_TURNING_INTERVAL, DEFAULT_PLAY_TURNING_INTERVAL);
    }

    public void setPlayTurningInterval(int val) {
        put(PLAY_TURNING_INTERVAL, val);
    }

    public boolean isAutoStart() {
        return sp.getBoolean(AUTO_START, DEFAULT_AUTO_START);
    }

    public void setAutoStart(boolean val) {
        put(AUTO_START, val);
    }

    public void clearUserInfo() {
        setUserInfoKey("");
        setUserInfoID("");
        setUserInfoName("");
        setUserInfoNickname("");
        setUserInfoProfileImg("");
        setUserInfoTimestamp("");
        setUserInfoOrgan("");
    }

    public void clearSP() {
        sp.edit().clear().commit();
    }

    protected void put(String key, boolean value) {
        sp.edit().putBoolean(key, value).commit();
    }

    protected void put(String key, int value) {
        sp.edit().putInt(key, value).commit();
    }

    protected void put(String key, float value) {
        sp.edit().putFloat(key, value).commit();
    }

    protected void put(String key, long value) {
        sp.edit().putLong(key, value).commit();
    }

    protected void put(String key, String value) {
        sp.edit().putString(key, value).commit();
    }

    protected void put(String key, Set<String> value) {
        sp.edit().putStringSet(key, value).commit();
    }
}

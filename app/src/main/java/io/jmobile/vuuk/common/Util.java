package io.jmobile.vuuk.common;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.base.BaseFragment;
import io.jmobile.vuuk.network.TimerBroadCastReceiver;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public final class Util {
    private static final String HEXES = "0123456789ABCDEF";
    private static SimpleDateFormat dayFormat = new SimpleDateFormat("d MMM", Locale.US);
    private static SimpleDateFormat hourFormat = new SimpleDateFormat("kk", Locale.getDefault());
    private static SimpleDateFormat monthFormat = new SimpleDateFormat("MMM, yyyy", Locale.getDefault());
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.getDefault());
    //    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM, dd, yyyy", Locale.US);
    private static SimpleDateFormat vbnm,
            timeFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
    private static SimpleDateFormat dayFormat2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public static Activity getActivity(Object object) {

        return object instanceof Activity ? (Activity) object : ((Fragment) object).getActivity();
    }

    public static boolean isTablet(Context context) {
        Configuration config = context.getResources().getConfiguration();

        if (Build.VERSION.SDK_INT >= 13)
            return config.smallestScreenWidthDp >= 600;
        else
            return false;
    }

    public static String getMACAddress() {
        String macaddress = "";
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (!intf.getName().equalsIgnoreCase("wlan0"))
                    continue;
                byte[] mac = intf.getHardwareAddress();
                if (mac != null) {
                    StringBuilder buf = new StringBuilder();
                    for (int idx = 0; idx < mac.length; idx++)
                        buf.append(String.format("%02X:", mac[idx]));
                    if (buf.length() > 0)
                        buf.deleteCharAt(buf.length() - 1);
                    macaddress = buf.toString();
                }
            }
        } catch (Exception ex) {
        }

        return TextUtils.isEmpty(macaddress) ? "00:00:00:00:00:00" : macaddress;
    }

    public static String getVersion(Context context) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "0.0.0";
        }
    }

    public static boolean isNetworkAbailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            return true;

        } else
            return false;
    }

    public static String getNetworkConnectionType(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo == null ? null : activeNetworkInfo.getTypeName();
    }

    public static void lockRotatation(Activity activity) {
        switch (activity.getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                break;

            case Configuration.ORIENTATION_LANDSCAPE:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                break;
        }
    }

    public static void unlockRotation(Activity activity) {
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public static void hideKeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null)
            imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    public static void hideKeyBoard(View v) {
        v.clearFocus();
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void showKeyBoard(View v) {
        v.requestFocus();
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
    }

    public static <T extends BaseFragment> T addf(FragmentManager fm, String tag, BaseFragment.BaseFragmentCreator<T> creator) {
        T t = null;
        BaseFragment f = ff(fm, tag);
        if (f != null)
            t = (T) f;
        else {
            t = creator.create();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(creator.getFrameId(), t, tag);
            ft.commit();
        }

        return t;
    }

    public static <T extends BaseFragment> T ff(FragmentManager fm, String tag) {
        if (fm == null || tag == null)
            return null;

        Fragment f = fm.findFragmentByTag(tag);

        try {
            return (T) f;
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            return null;
        }
    }

    // find dialog fragment
    @SuppressWarnings("unchecked")
    public static <T extends BaseDialogFragment> T fdf(FragmentManager fm, String tag) {
        if (fm == null || tag == null)
            return null;

        Fragment f = fm.findFragmentByTag(tag);

        try {
            return (T) f;
        } catch (Exception e) {
            return null;
        }
    }

    // hide dialog fragment
    public static void hdf(FragmentManager fm, String tag) {
        DialogFragment prev = fdf(fm, tag);
        if (prev != null)
            prev.dismissAllowingStateLoss();
    }

    // show dialog fragment
    public static void sdf(FragmentManager fm, BaseDialogFragment d) {
        if (fm == null || d == null || d.getDialogTag() == null)
            return;

        hdf(fm, d.getDialogTag());
        try {
            d.show(fm, d.getDialogTag());
        } catch (Exception e) {
        }
    }

    public static String convertToStringTime(final long time) {
        long duration = time / 1000;
        long hours = duration / 3600;
        long minutes = (duration - hours * 3600) / 60;
        long seconds = duration - (hours * 3600 + minutes * 60);

        if (hours > 0)
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        else
            return String.format("%02d:%02d", minutes, seconds);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static String toHexString(byte[] bytes) {
        final StringBuilder builder = new StringBuilder(2 * bytes.length);
        for (final byte b : bytes) {
            builder.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
        }
        return builder.toString();
    }

    public static String sha256(String s) {
        byte[] bytes = s.getBytes();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
            algorithm.reset();
            algorithm.update(bytes);
            byte result[] = algorithm.digest();

            return toHexString(result);
        } catch (NoSuchAlgorithmException e) {
            return "00000000000000000000000000000000";
        }
    }

    public static String optString(JSONObject o, String name) {
        return o.isNull(name) ? null : o.optString(name);
    }

    public static String optString(JSONArray a, int index) {
        return a.isNull(index) ? null : a.optString(index);
    }

    public static String getTimeString(long date) {
        return timeFormat.format(new Date(date));
    }

    public static String getDateString(long date) {
        return dateFormat.format(new Date(date));
    }

    public static String getFullDateString(long date) {
        return format.format(new Date(date));
    }

    public static String getDay(long date) {
        return dayFormat.format(new Date(date));
    }

    public static String getHour(long date) {
        return hourFormat.format(new Date(date));
    }

    public static String getMonth(long date) {
        return monthFormat.format(new Date(date));
    }

    public static String getDay2(long date) {
        return dayFormat2.format(new Date(date));
    }

    public static boolean isToday(String date) {
        String today = getDateString(System.currentTimeMillis());
        return today.equalsIgnoreCase(date);
    }

    public static String convertSecondsToHMmSs(long seconds) {
//        long s = seconds % 60;
//        long m = (seconds / 60) % 60;
//        long h = (seconds / (60 * 60)) % 24;
        long h = MILLISECONDS.toHours(seconds) % 24;
        long m = MILLISECONDS.toMinutes(seconds) % 60;
        long s = MILLISECONDS.toSeconds(seconds) % 60;

        return String.format("%d hr %02d min %02d sec", h, m, s);
    }

    public static boolean isYesterday(String date) {
        long temp = 1000 * 60 * 60 * 24;
        String yesterday = getDateString(System.currentTimeMillis() - temp);
        return yesterday.equalsIgnoreCase(date);
    }

    public static String getTimerString(int hour, int min) {
        StringBuilder sb = new StringBuilder()
                .append(hour / 12 > 0 ? "PM " : "AM ")
                .append(hour % 12 + ":")
                .append(String.format("%02d", min));

        return sb.toString();
    }

    public static void startTimer(Context context, long delay) {
        delay = System.currentTimeMillis() + delay;
        Intent alarmIntent = new Intent(context, TimerBroadCastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, delay, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            manager.setExact(AlarmManager.RTC_WAKEUP, delay, pendingIntent);
        } else {
            manager.set(AlarmManager.RTC_WAKEUP, delay, pendingIntent);
        }
    }

    public static void offTimer(Context context) {
        Intent alarmIntent = new Intent(context, TimerBroadCastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
    }

    public static void showToast(Activity context, String message) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_toast,
                (ViewGroup) context.findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text_message);
        text.setText(message);

        Toast toast = new Toast(context.getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static String getCurrentActivityName(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
        ComponentName topActivity = info.get(0).topActivity;
        String topActivityName = topActivity.getShortClassName();

        return topActivityName;
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    public static String getFileSize(long size) {
        if (size <= 0)
            return "0";

        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

}

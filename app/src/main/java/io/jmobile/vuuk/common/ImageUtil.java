package io.jmobile.vuuk.common;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Cecil on 2017-05-30.
 */

public class ImageUtil {
    public static void recycleImageView(ImageView iv) {
        if (iv == null)
            return;

//        if (iv instanceof ScaledImageView) {
//            ((ScaledImageView) iv).recycle();
//
//            return ;
//        }

        BitmapDrawable bitmapDrawable = null;

        if (iv.getDrawable() != null)
            bitmapDrawable = ((BitmapDrawable) iv.getDrawable());

        iv.setImageDrawable(null);

        if (bitmapDrawable != null && bitmapDrawable.getBitmap() != null)
            bitmapDrawable.getBitmap().recycle();

        bitmapDrawable = null;


    }

    public static Bitmap resizeBitmap(String path) {
        return resizeBitmap(path, -1, -1);
    }

    public static Bitmap resizeBitmap(String path, int maxWidth, int maxHeight) {
        if (maxWidth <= 0 || maxHeight <= 0)
            return BitmapFactory.decodeFile(path);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        BitmapFactory.Options options1 = new BitmapFactory.Options();
        options1.inSampleSize = getInSampleSize((int) ((float) options.outWidth / (float) maxWidth));

        return resizeBitmap(BitmapFactory.decodeFile(path, options1), maxWidth, maxHeight);
    }

    public static Bitmap resizeBitmap(Resources r, int resId) {
        return resizeBitmap(r, resId, -1, -1);
    }

    public static Bitmap resizeBitmap(Resources r, int resId, int maxWidth, int maxHeight) {
        if (maxWidth <= 0 || maxHeight <= 0)
            return BitmapFactory.decodeResource(r, resId);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(r, resId, options);

        BitmapFactory.Options options1 = new BitmapFactory.Options();
        options1.inSampleSize = getInSampleSize((int) ((float) options.outWidth / (float) maxWidth));

        return resizeBitmap(BitmapFactory.decodeResource(r, resId, options1), maxWidth, maxHeight);
    }

    private static Bitmap resizeBitmap(Bitmap orgBitmap, int maxWidth, int maxHeight) {
        if (orgBitmap == null)
            return null;

        int orgWidth = orgBitmap.getWidth();
        int orgHeight = orgBitmap.getHeight();

        float orgRatio = (float) orgWidth / (float) orgHeight;
        float viewRatio = (float) maxWidth / (float) maxHeight;

        int scaledWidth;
        int scaledHeight;
        float scale;

        if (orgRatio >= viewRatio) {
            scale = (float) maxWidth / (float) orgWidth;
            scaledWidth = maxWidth;
            scaledHeight = (int) ((float) orgHeight * scale);
        } else {
            scale = (float) maxHeight / (float) orgHeight;
            scaledHeight = maxHeight;
            scaledWidth = (int) ((float) orgWidth * scale);
        }

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(orgBitmap, scaledWidth, scaledHeight, true);
        if (scaledBitmap != orgBitmap)
            orgBitmap.recycle();

        return scaledBitmap;
    }

    private static int getInSampleSize(int ratio) {
        int i = 1;

        while (i <= ratio)
            i *= 2;

        return Math.max(i / 2, i);
    }

    public static Bitmap getThumbnailFromFilepath(String path) {
        Bitmap bitmap = null;

        File imageFile = new File(path);
        if (imageFile.exists()) {
            bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        }

//        MediaMetadataRetriever mediaMetadataRetriever = null;
//
//        try {
//            mediaMetadataRetriever = new MediaMetadataRetriever();
//            mediaMetadataRetriever.setDataSource(path);
//            bitmap = mediaMetadataRetriever.getFrameAtTime();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (mediaMetadataRetriever != null)
//                mediaMetadataRetriever.release();
//        }

        return bitmap;
    }

    public static Bitmap saveThumbnailFromURL(String url, String path) {
        Bitmap bitmap = null;
        try {
            bitmap = new ImageDownloadTask().execute(url).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (bitmap != null) {

            File file = new File(path);
            OutputStream os;
            try {
                os = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return bitmap;
    }

    public static Bitmap getThumbanilFromURL(String url) {
        try {
            return new ThumbnailImageLoader().execute(url).get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getImageFromURL(String url) {
        try {
            return new ImageDownloadTask().execute(url).get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getImageFromURLNonTask(String strUrl) {
        Bitmap bitmap = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(strUrl);
            connection = (HttpURLConnection) url.openConnection();
            InputStream is = connection.getInputStream();

            bitmap = BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.disconnect();
        }
        return bitmap;
    }

    public static byte[] getImageBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        if (stream == null || stream.toByteArray() == null)
            return null;
        else
            return stream.toByteArray();
    }

    public static Bitmap getImage(byte[] image) {
        try {
            return BitmapFactory.decodeByteArray(image, 0, image.length);
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static Bitmap getScreenShot(Context context, View view) {
        Bitmap bitmap = null;

        try {
            View screenView = view.getRootView();
            screenView.setDrawingCacheEnabled(true);
            bitmap = Bitmap.createBitmap(screenView.getDrawingCache(), 0, getStatusBarSize(context), screenView.getWidth(), screenView.getHeight() / 7 * 6);
            screenView.setDrawingCacheEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public static byte[] getScreenShotByteArray(Context context, View view) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap bitmap = getScreenShot(context, view);
        if (bitmap == null)
            return null;
        else {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            return stream.toByteArray();
        }
    }

    public static int getStatusBarSize(Context context) {
        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }

        return statusBarHeight;
    }

    private static class ThumbnailImageLoader extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            String path = params[0];
            MediaMetadataRetriever mediaMetadataRetriever = null;

            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(path, new HashMap<String, String>());
                else
                    mediaMetadataRetriever.setDataSource(path);
                bitmap = mediaMetadataRetriever.getFrameAtTime();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }
    }

    private static class ImageDownloadTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                InputStream is = connection.getInputStream();

                bitmap = BitmapFactory.decodeStream(is);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null)
                    connection.disconnect();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }
    }

}

package io.jmobile.vuuk.ui.dialog;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.data.BookItem;

public class BookFilterDialog extends BaseDialogFragment {

    public static final String TAG = BookFilterDialog.class.getSimpleName();

    Button closeButton;

    LinearLayout orderNewLayout, orderHitLayout, orderBestLayout, typeAllLayout, typeEpubLayout, typePDFLayout, typeVideoLayout;
    LinearLayout orderNewButton, orderHitButton, orderBestButton, typeAllButton, typeEpubButton, typePDFButton, typeVideoButton;
    ImageView orderNewCKImage, orderHitCKImage, orderBestCKImage, typeAllCKImage, typeEpubCKImage, typePDFCKImage, typeVideoCKImage;

    String curType, curOrder;

    public static BookFilterDialog newInstance(String tag) {
        BookFilterDialog d = new BookFilterDialog();

        d.createArguments(tag);
        return d;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        curType = sp.getBookType();
        curOrder = sp.getBookOrder();

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_filter;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        closeButton = fv(R.id.button_close);
        closeButton.setOnClickListener(v -> {
            dismiss();
        });

        orderNewLayout = fv(R.id.layout_order_date);
        orderNewCKImage = fv(R.id.image_check_order_date);
        orderNewButton = fv(R.id.button_order_date);
        orderNewButton.setOnClickListener(v -> {
            sp.setBookOrder(BookItem.BOOK_ORDER_NEW);
            if (positiveListener != null && !curOrder.equalsIgnoreCase(sp.getBookOrder())) {
                positiveListener.onDialogPositive(BookFilterDialog.this, TAG);
                curOrder = sp.getBookOrder();
                changeBookOrder();
            }

        });

        orderHitLayout = fv(R.id.layout_order_hit);
        orderHitCKImage = fv(R.id.image_check_order_hit);
        orderHitButton = fv(R.id.button_order_hit);
        orderHitButton.setOnClickListener(v -> {
            sp.setBookOrder(BookItem.BOOK_ORDER_HIT);
            if (positiveListener != null && !curOrder.equalsIgnoreCase(sp.getBookOrder())) {
                positiveListener.onDialogPositive(BookFilterDialog.this, TAG);
                curOrder = sp.getBookOrder();
                changeBookOrder();
            }

        });

        orderBestLayout = fv(R.id.layout_order_best);
        orderBestCKImage = fv(R.id.image_check_order_best);
        orderBestButton = fv(R.id.button_order_best);
        orderBestButton.setOnClickListener(v -> {
            sp.setBookOrder(BookItem.BOOK_ORDER_BEST);
            if (positiveListener != null && !curOrder.equalsIgnoreCase(sp.getBookOrder())) {
                positiveListener.onDialogPositive(BookFilterDialog.this, TAG);
                curOrder = sp.getBookOrder();
                changeBookOrder();
            }

        });

        typeAllLayout = fv(R.id.layout_type_all);
        typeAllCKImage = fv(R.id.image_check_type_all);
        typeAllButton = fv(R.id.button_type_all);
        typeAllButton.setOnClickListener(v -> {
            sp.setBookType(BookItem.TYPE_ALL);
            if (positiveListener != null && !curType.equalsIgnoreCase(sp.getBookType())) {
                positiveListener.onDialogPositive(BookFilterDialog.this, TAG);
                curType = sp.getBookType();
                changeBookType();
            }

        });

        typeEpubLayout = fv(R.id.layout_type_epub);
        typeEpubCKImage = fv(R.id.image_check_type_epub);
        typeEpubButton = fv(R.id.button_type_epub);
        typeEpubButton.setOnClickListener(v -> {
            sp.setBookType(BookItem.TYPE_EPUB);
            if (positiveListener != null && !curType.equalsIgnoreCase(sp.getBookType())) {
                positiveListener.onDialogPositive(BookFilterDialog.this, TAG);
                curType = sp.getBookType();
                changeBookType();
            }

        });

        typePDFLayout = fv(R.id.layout_type_pdf);
        typePDFCKImage = fv(R.id.image_check_type_pdf);
        typePDFButton = fv(R.id.button_type_pdf);
        typePDFButton.setOnClickListener(v -> {
            sp.setBookType(BookItem.TYPE_PDF);
            if (positiveListener != null && !curType.equalsIgnoreCase(sp.getBookType())) {
                positiveListener.onDialogPositive(BookFilterDialog.this, TAG);
                curType = sp.getBookType();
                changeBookType();
            }

        });

        typeVideoLayout = fv(R.id.layout_type_video);
        typeVideoCKImage = fv(R.id.image_check_type_video);
        typeVideoButton = fv(R.id.button_type_video);
        typeVideoButton.setOnClickListener(v -> {
            sp.setBookType(BookItem.TYPE_VIDEO);
            if (positiveListener != null && !curType.equalsIgnoreCase(sp.getBookType())) {
                positiveListener.onDialogPositive(BookFilterDialog.this, TAG);
                curType = sp.getBookType();
                changeBookType();
            }

        });

        changeBookOrder();
        changeBookType();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();
        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.9f;
        params.width = (int) (dm.widthPixels * rate);
        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void changeBookOrder() {
        orderNewLayout.setBackgroundResource(BookItem.BOOK_ORDER_NEW.equalsIgnoreCase(sp.getBookOrder()) ? R.color.gray2_a100 : R.color.white);
        orderNewCKImage.setVisibility(BookItem.BOOK_ORDER_NEW.equalsIgnoreCase(sp.getBookOrder()) ? View.VISIBLE : View.INVISIBLE);
        orderHitLayout.setBackgroundResource(BookItem.BOOK_ORDER_HIT.equalsIgnoreCase(sp.getBookOrder()) ? R.color.gray2_a100 : R.color.white);
        orderHitCKImage.setVisibility(BookItem.BOOK_ORDER_HIT.equalsIgnoreCase(sp.getBookOrder()) ? View.VISIBLE : View.INVISIBLE);
        orderBestLayout.setBackgroundResource(BookItem.BOOK_ORDER_BEST.equalsIgnoreCase(sp.getBookOrder()) ? R.color.gray2_a100 : R.color.white);
        orderBestCKImage.setVisibility(BookItem.BOOK_ORDER_BEST.equalsIgnoreCase(sp.getBookOrder()) ? View.VISIBLE : View.INVISIBLE);
    }

    private void changeBookType() {
        typeAllLayout.setBackgroundResource(BookItem.TYPE_ALL.equalsIgnoreCase(sp.getBookType()) ? R.color.gray2_a100 : R.color.white);
        typeAllCKImage.setVisibility(BookItem.TYPE_ALL.equalsIgnoreCase(sp.getBookType()) ? View.VISIBLE : View.INVISIBLE);
        typeEpubLayout.setBackgroundResource(BookItem.TYPE_EPUB.equalsIgnoreCase(sp.getBookType()) ? R.color.gray2_a100 : R.color.white);
        typeEpubCKImage.setVisibility(BookItem.TYPE_EPUB.equalsIgnoreCase(sp.getBookType()) ? View.VISIBLE : View.INVISIBLE);
        typePDFLayout.setBackgroundResource(BookItem.TYPE_PDF.equalsIgnoreCase(sp.getBookType()) ? R.color.gray2_a100 : R.color.white);
        typePDFCKImage.setVisibility(BookItem.TYPE_PDF.equalsIgnoreCase(sp.getBookType()) ? View.VISIBLE : View.INVISIBLE);
        typeVideoLayout.setBackgroundResource(BookItem.TYPE_VIDEO.equalsIgnoreCase(sp.getBookType()) ? R.color.gray2_a100 : R.color.white);
        typeVideoCKImage.setVisibility(BookItem.TYPE_VIDEO.equalsIgnoreCase(sp.getBookType()) ? View.VISIBLE : View.INVISIBLE);
    }

}

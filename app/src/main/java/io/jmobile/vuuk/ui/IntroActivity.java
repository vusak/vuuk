package io.jmobile.vuuk.ui;

import android.animation.Animator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.Window;

import com.airbnb.lottie.LottieAnimationView;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.network.Api;
import io.jmobile.vuuk.ui.dialog.MessageDialog;

public class IntroActivity extends BaseActivity {

    LottieAnimationView animationView;

    Api api;
    boolean start = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intro);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        animationView = fv(R.id.animation_view);
        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (start)
                    start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        if (Util.isNetworkAbailable(this)) {
            api.versionCheck();
        }
    }

    @Override
    public void handleApiMessage(Message m) {
        boolean success = m.getData().getBoolean(Common.ARG_RESULT);
        if (m.what == Common.API_CODE_POST_VERSION) {

            if (success) {
                String v = m.getData().getString(Common.ARG_VERSION);
                if (!TextUtils.isEmpty(v)) {
                    int version = Integer.parseInt(v);
                    if (version > Common.VERSION)
                        update();
                }
            }
        }
        super.handleApiMessage(m);
    }

    private void start() {
        Intent intent = new Intent(IntroActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void update() {
        start = false;

        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
        dlg.setMessage(r.s(R.string.message_required_update));
        dlg.setPositiveLabel(r.s(R.string.str_continue));
        dlg.setPositiveListener((dialog, tag) -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + getPackageName()));
            startActivity(intent);
            finish();
        });
        dlg.setNegativeLabel(r.s(R.string.cancel));
        dlg.setNegativeListener((dialog, tag) -> finish());
        sdf(dlg);
    }
}

package io.jmobile.vuuk.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseActivity;

import static io.jmobile.vuuk.common.Common.ARG_START_TYPE;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE_POWER_CONNECT;

public class MessageActivity extends BaseActivity {

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };
    Button closeButton, cancelButton, openButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.activity_message);

        closeButton = fv(R.id.button_close);
        closeButton.setOnClickListener(v -> onBackPressed());

        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(v -> onBackPressed());

        openButton = fv(R.id.button_positive);
        openButton.setOnClickListener(v -> startMainActivity());

        handler.postDelayed(() -> startMainActivity(), 5000);

        if (!sp.isAutoStart())
            onBackPressed();
    }

    private void startMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(ARG_START_TYPE, ARG_START_TYPE_POWER_CONNECT);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacksAndMessages(null);
        super.onBackPressed();
    }
}

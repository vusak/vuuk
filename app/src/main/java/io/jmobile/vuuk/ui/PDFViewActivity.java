package io.jmobile.vuuk.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.File;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.LogUtil;

import static io.jmobile.vuuk.common.Common.ARG_MULTI_MODE;
import static io.jmobile.vuuk.common.Common.ARG_PDF_URL;
import static io.jmobile.vuuk.common.Common.ARG_SLEEP_MODE;
import static io.jmobile.vuuk.common.Util.offTimer;
import static io.jmobile.vuuk.common.Util.showToast;
import static io.jmobile.vuuk.common.Util.startTimer;

public class PDFViewActivity extends BaseActivity {

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };
    private final int AUTO_PLAY_DELAY = 7000;

    Button backButton, closeButton;
    String url = "";
    PDFView pdfView;
    boolean sleepMode = false;
    boolean multiMode = false;

    int currentPage = 0;
    int delay = AUTO_PLAY_DELAY;

    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogUtil.DEBUG_MODE)
                showToast(PDFViewActivity.this, "Times up");
            finishViewer();
        }
    };

    private void finishViewer() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_view_pdf);

        delay = sp.getPlayTurningInterval();
        url = getIntent().getStringExtra(ARG_PDF_URL);
        if (TextUtils.isEmpty(url)) {
            url = sp.getAutoPlayCurrentUrl();
            if (TextUtils.isEmpty(url)) {
                showToast(this, "ERROR!!!! URL is Empty!!!");
                finish();
            }
        } else
            sp.setAutoPlayCurrentUrl(url);

        sleepMode = getIntent().getBooleanExtra(ARG_SLEEP_MODE, false);
        multiMode = getIntent().getBooleanExtra(ARG_MULTI_MODE, false);
        if (sleepMode) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            if (sp.getAutoPlayTimer() > 0) {
                long delay = sp.getAutoPlayTimer() - (System.currentTimeMillis() - sp.getAutoPlayStartTime());

                if (LogUtil.DEBUG_MODE)
                    showToast(this, "Timer :: " + delay);
                if (delay > 0)
                    startTimer(this, delay);
            }
        }

        delay = sp.getPlayTurningInterval() * 1000;
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(timeReceiver, new IntentFilter(Common.INTENTFILTER_BROADCAST_TIMER));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(timeReceiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (!url.isEmpty()) {
            LogUtil.log("" + newConfig.orientation);
            pdfView.postDelayed(() -> pdfView.fromFile(new File(url))
                    .spacing(2)
                    .defaultPage(currentPage)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
                    .enableDoubletap(true)
                    .onPageScroll((page, positionOffset) -> {

                    })
                    .scrollHandle(new DefaultScrollHandle(PDFViewActivity.this))
                    .onPageChange((page, pageCount) -> {
                        currentPage = page;
                        if (sleepMode) {
                            final int toPage;
                            if (pageCount > page + 1)
                                toPage = page + 1;
                            else {
                                if (multiMode) {
                                    pdfView.postDelayed(() -> {
                                        setResult(RESULT_OK);
                                        finish();

                                    }, delay);
                                    return;
                                } else
                                    toPage = 0;
                            }
                            handler.postDelayed(() -> pdfView.jumpTo(toPage), delay);
                        }
                    })
                    .onLoad(nbPages -> {
                        if (sleepMode)
                            handler.postDelayed(() -> {
                                pdfView.jumpTo(pdfView.getCurrentPage() + 1);
                            }, delay);
                    })
                    .pageFitPolicy(FitPolicy.BOTH)
                    .load(), 100);

        }
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());
        backButton.setVisibility(sleepMode ? View.GONE : View.VISIBLE);

        closeButton = fv(R.id.button_close);
        closeButton.setOnClickListener(v -> {
            offTimer(this);
            if (handler != null)
                handler.removeCallbacksAndMessages(null);
            onBackPressed();
        });
        closeButton.setVisibility(sleepMode ? View.VISIBLE : View.GONE);

        pdfView = fv(R.id.pdfView);

        if (!url.isEmpty()) {
            pdfView.fromFile(new File(url))
                    .spacing(2)
                    .defaultPage(currentPage)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                    .enableDoubletap(true)
                    .onPageScroll((page, positionOffset) -> {

                    })
                    .scrollHandle(new DefaultScrollHandle(this))
                    .onPageChange((page, pageCount) -> {
                        currentPage = page;
                        if (sleepMode) {
                            final int toPage;
                            if (pageCount > page + 1)
                                toPage = page + 1;
                            else {
                                if (multiMode) {
                                    pdfView.postDelayed(() -> {
                                        setResult(RESULT_OK);
                                        finish();

                                    }, delay);
                                    return;
                                } else
                                    toPage = 0;
                            }
                            handler.postDelayed(() -> pdfView.jumpTo(toPage), delay);
                        }
                    })
                    .onLoad(nbPages -> {
                        if (sleepMode)
                            handler.postDelayed(() -> {
                                pdfView.jumpTo(pdfView.getCurrentPage() + 1);
                            }, delay);
                    })
                    .pageFitPolicy(FitPolicy.BOTH)
                    .load();
        }
    }

}

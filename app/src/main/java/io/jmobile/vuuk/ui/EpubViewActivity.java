package io.jmobile.vuuk.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.LogUtil;

import static io.jmobile.vuuk.common.Common.ARG_EPUB_URL;
import static io.jmobile.vuuk.common.Common.ARG_MULTI_MODE;
import static io.jmobile.vuuk.common.Common.ARG_SLEEP_MODE;
import static io.jmobile.vuuk.common.Util.offTimer;
import static io.jmobile.vuuk.common.Util.showToast;
import static io.jmobile.vuuk.common.Util.startTimer;

public class EpubViewActivity extends BaseActivity {

    Context context;
    WebView webView;
    String url;
    Button backButton, closeButton;
    boolean sleepMode = false;
    boolean multiMode = false;

    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogUtil.DEBUG_MODE)
                showToast(EpubViewActivity.this, "Times up");
            finishViewer();
        }
    };

    private void finishViewer() {
//        Intent returnIntent = new Intent();
//        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onDestroy() {

        if (webView != null) {
            webView.removeCallbacks(null);
            webView.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_view_epub);

        context = this;
        url = getIntent().getStringExtra(ARG_EPUB_URL);
        if (TextUtils.isEmpty(url)) {
            url = sp.getAutoPlayCurrentUrl();
            if (TextUtils.isEmpty(url)) {
                showToast(this, "ERROR!!!! URL is Empty!!!");
                finish();
            }
        } else
            sp.setAutoPlayCurrentUrl(url);
        url = url + "?lang=" + app.getLocale() + "&notitle=notitle";

        sleepMode = getIntent().getBooleanExtra(ARG_SLEEP_MODE, false);
        multiMode = getIntent().getBooleanExtra(ARG_MULTI_MODE, false);
        if (sleepMode) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            if (multiMode)
                url = url + "&autoplay=y&delay=" + String.valueOf(sp.getPlayTurningInterval()) + "&multi=y#cover";
            else
                url = url + "&autoplay=y&delay=" + String.valueOf(sp.getPlayTurningInterval()) + "#cover";
            if (sp.getAutoPlayTimer() > 0) {
                long delay = sp.getAutoPlayTimer() - (System.currentTimeMillis() - sp.getAutoPlayStartTime());

                if (LogUtil.DEBUG_MODE)
                    showToast(this, "Timer :: " + delay);
                if (delay > 0)
                    startTimer(this, delay);
            }
        }

        initView();

        webView.loadUrl(url);
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(timeReceiver, new IntentFilter(Common.INTENTFILTER_BROADCAST_TIMER));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(timeReceiver);
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());
        backButton.setVisibility(sleepMode ? View.GONE : View.VISIBLE);

        closeButton = fv(R.id.button_close);
        closeButton.setOnClickListener(v -> {
            offTimer(this);
            onBackPressed();
        });
        closeButton.setVisibility(sleepMode ? View.VISIBLE : View.GONE);

        webView = fv(R.id.webview);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSavePassword(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.addJavascriptInterface(new JavaScriptInterface(), "_android_");

        webView.setWebViewClient(new MyWebViewClient());
        webView.requestFocus();

        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                view.removeAllViews();

                WebView child = new WebView(view.getContext());
                child.getSettings().setJavaScriptEnabled(true);
                child.setWebChromeClient(new WebChromeClient() {
                    @Override
                    public void onCloseWindow(WebView window) {
                        window.setVisibility(View.GONE);
                        webView.removeView(window);
                    }
                });
                child.setWebViewClient(new WebViewClient());
                child.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                view.addView(child);

                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(child);
                resultMsg.sendToTarget();

                return true;
            }
        });
    }

    public class JavaScriptInterface {
        public JavaScriptInterface() {
        }

        @JavascriptInterface
        public void callLastPage(String msg) {
            ((Activity) context).runOnUiThread(() -> {
                if (LogUtil.DEBUG_MODE)
                    LogUtil.log("END!!!");
                if (multiMode) {
                    setResult(RESULT_OK);
                    finish();
                }
            });
        }
    }

    private class MyWebViewClient extends WebViewClient {

        String compareText = "";

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            resend.sendToTarget();
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }

        @Override
        public void onLoadResource(WebView view, final String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, final String url) {

            view.loadUrl(url);
            return true;
        }


    }
}

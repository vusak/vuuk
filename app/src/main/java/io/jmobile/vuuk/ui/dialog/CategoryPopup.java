package io.jmobile.vuuk.ui.dialog;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.CategoryAdapter;
import io.jmobile.vuuk.adapter.ReAdapter;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.data.CategoryItem;

public class CategoryPopup extends BaseDialogFragment {

    public static final String TAG = CategoryPopup.class.getSimpleName();

    RecyclerView lv;
    ArrayList<CategoryItem> list;
    LinearLayoutManager manager;
    CategoryAdapter adapter;
    LinearLayout allButton, allLayout;
    ImageView allCKImage;
    Button closeButton;

    String curCategory;

    public static CategoryPopup newInstance(String tag) {
        CategoryPopup d = new CategoryPopup();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        curCategory = sp.getBookCategory();
    }

    @Override
    public int getLayoutId() {
        return R.layout.popup_book_category;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        closeButton = fv(R.id.button_close);
        closeButton.setOnClickListener(v -> dismiss());

        allLayout = fv(R.id.layout_all);
        allCKImage = fv(R.id.image_check_all);
        allButton = fv(R.id.button_all);
        allButton.setOnClickListener(v -> {
            sp.setBookCategory("");
            if (positiveListener != null && !curCategory.equalsIgnoreCase(sp.getBookCategory())) {
                positiveListener.onDialogPositive(CategoryPopup.this, TAG);
                curCategory = sp.getBookCategory();

                changeCategory();
            }
        });


        lv = fv(R.id.lv);
        list = new ArrayList<>();
        list.addAll(db.getCategoryItemsByChannel(sp.getSelectedChannelId()));
        Collections.sort(list, CategoryItem.CATEGORY_DEPTH_ASC);
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new CategoryAdapter(getActivity(), R.layout.item_category, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                sp.setBookCategory(((CategoryItem) item).getCategoryId());
                if (positiveListener != null && !curCategory.equalsIgnoreCase(sp.getBookCategory())) {
                    positiveListener.onDialogPositive(CategoryPopup.this, TAG);
                    curCategory = sp.getBookCategory();
//                    adapter.setSelectedItem(position);
                    changeCategory();
                }
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);

        changeCategory();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.9f;
        params.width = (int) (dm.widthPixels * rate);
        getDialog().getWindow().setAttributes(params);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void changeCategory() {
        allLayout.setBackgroundResource(TextUtils.isEmpty(sp.getBookCategory()) ? R.color.gray2_a100 : R.color.white);
        allCKImage.setVisibility(TextUtils.isEmpty((sp.getBookCategory())) ? View.VISIBLE : View.INVISIBLE);
        adapter.setSelectedItem(sp.getBookCategory());
    }
}

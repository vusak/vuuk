package io.jmobile.vuuk.ui.dialog;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.ChannelListAdapter;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.ChannelItem;

public class ChannelPopup extends BaseDialogFragment {

    public static final String TAG = ChannelPopup.class.getSimpleName();

    RecyclerView lv;
    ArrayList<ChannelItem> list;
    LinearLayoutManager manager;
    ChannelListAdapter adapter;

    String curChannel;
    ChannelPopupListener listener;

    public static ChannelPopup newInstance(String tag, String id) {
        ChannelPopup d = new ChannelPopup();

        d.curChannel = id;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.popup_channel;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {


        lv = fv(R.id.lv);
        list = new ArrayList<>();
        for (ChannelItem item : db.getChannelItems())
            if (item.isChannelJoined())
                list.add(item);
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new ChannelListAdapter(getActivity(), R.layout.item_channel_list, list, new ChannelListAdapter.ChannelListAdapterListener() {
            @Override
            public void OnItemClick(int position, ChannelItem item) {
                if (listener != null)
                    listener.onSelectedChannel(item);
                dismiss();
            }

            @Override
            public void OnItemLongClick(int position, ChannelItem item) {

            }
        });
        lv.setAdapter(adapter);
        adapter.setSelectedItem(curChannel);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void setListener(ChannelPopupListener listener) {
        this.listener = listener;
    }


    public static interface ChannelPopupListener {
        public void onSelectedChannel(ChannelItem item);
    }
}

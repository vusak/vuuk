package io.jmobile.vuuk.ui.dialog;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.ChannelListAdapter;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.base.ExpandLayout;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.ChannelItem;
import io.jmobile.vuuk.network.Api;

public class AddChannelDialog extends BaseDialogFragment {

    public static final String TAG = AddChannelDialog.class.getSimpleName();

    Button cancelButton, addButton;
    RecyclerView lv;
    ArrayList<ChannelItem> list, filterList;
    LinearLayoutManager manager;
    ChannelListAdapter adapter;

    ExpandLayout channelLayout;
    TextView recentText;
    EditText channelEdit;
    Button moreButton;
    Api api;

    private AddBookmarkDialogListener listener;

    private Handler searchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                if (filterList == null)
                    filterList = new ArrayList<>();
                filterList.clear();

                String keyword = channelEdit.getText().toString();
                if (!TextUtils.isEmpty(keyword)) {
                    for (ChannelItem item : list) {
                        if (item.getChannelId().contains(keyword))
                            filterList.add(item);
                    }
                    recentText.setVisibility(View.GONE);
                } else {
                    for (int i = 0; i < 3; i++) {
                        if (list.size() > i) {
                            filterList.add(list.get(list.size() - 1 - i));
                        } else
                            break;
                    }
                    recentText.setVisibility(View.VISIBLE);
                }
                adapter.setKeyword(keyword);
            }
        }
    };

    public static AddChannelDialog newInstance(String tag) {
        AddChannelDialog d = new AddChannelDialog();

        d.createArguments(tag);
        return d;
    }

    public void setListener(AddBookmarkDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);
        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_channel;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        cancelButton = fv(R.id.button_cancel);
        cancelButton.setOnClickListener(v -> {
            if (listener != null)
                listener.onCancelButtonClick();

            dismiss();
        });

        addButton = fv(R.id.button_add);
        addButton.setOnClickListener(v -> {
            ChannelItem item = adapter.getSelectedChannelItem();
            if (item == null) {
                Toast.makeText(getActivity(), "no Selected Channel", Toast.LENGTH_SHORT).show();

                return;
            }
            if (listener != null) {
                listener.onAddButtonClick(item);
            }
            dismiss();
        });

        channelLayout = fv(R.id.layout_expand_channel);
        channelLayout.collapse(false);
        channelEdit = fv(R.id.edit_channel);
        channelEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchHandler.removeCallbacksAndMessages(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchHandler.sendEmptyMessage(0);
            }
        });

        channelEdit.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchHandler.sendEmptyMessage(0);
                Util.hideKeyBoard(channelEdit);
                handled = true;
            }
            return handled;
        });

        recentText = fv(R.id.text_recent);
        moreButton = fv(R.id.button_more);
        moreButton.setOnClickListener(v -> {
            if (channelLayout != null)
                channelLayout.expand(false);
            moreButton.setVisibility(View.GONE);
        });
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        filterList = new ArrayList<>();
//        for (ChannelItem item : db.getChannelItems())
//            if (!item.isChannelJoined())
//                list.add(item);
//        list.addAll(db.getChannelItems());
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new ChannelListAdapter(getActivity(), R.layout.item_channel_list, filterList, new ChannelListAdapter.ChannelListAdapterListener() {
            @Override
            public void OnItemClick(int position, ChannelItem item) {
                if (adapter != null)
                    adapter.setSelectedPosition(position);
                if (channelEdit != null)
                    Util.hideKeyBoard(channelEdit);
            }

            @Override
            public void OnItemLongClick(int position, ChannelItem item) {

            }
        });
        lv.setAdapter(adapter);

        if (Util.isNetworkAbailable(getActivity())) {
            refreshList();
        } else
            api.getChannelList();
    }

    @Override
    public void handleApiMessage(Message m) {
        super.handleApiMessage(m);
        if (m.what == Common.API_CODE_POST_CHANNEL_LIST) {
            if (m.getData().getBoolean(Common.ARG_RESULT)) {
                refreshList();
            } else {
                String message = m.getData().getString(Common.ARG_ERROR_MESSAGE);
                if (!TextUtils.isEmpty(message) && message.equalsIgnoreCase(Common.RESULT_CODE_ERROR_VERSION)) {
                    if (listener != null)
                        listener.onVersionUpdate();
                }
            }
        }
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);

        if (!Util.isTablet(getActivity())) {

            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }

        if (lv != null) {
            int height = (int) (dm.heightPixels * 0.3f);

            ViewGroup.LayoutParams p = lv.getLayoutParams();
            p.height = height;
            lv.setLayoutParams(p);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void refreshList() {
        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();
        for (ChannelItem item : db.getChannelItems())
            if (!item.isChannelJoined())
                list.add(item);

        recentText.setVisibility(View.VISIBLE);
        moreButton.setVisibility(list.size() > 3 ? View.VISIBLE : View.GONE);
        for (int i = 0; i < 3; i++) {
            if (list.size() > i) {
                filterList.add(list.get(list.size() - 1 - i));
            } else
                break;
        }

        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    public static interface AddBookmarkDialogListener {
        public void onAddButtonClick(ChannelItem item);

        public void onCancelButtonClick();

        public void onVersionUpdate();
    }
}

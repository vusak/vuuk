package io.jmobile.vuuk.ui.dialog;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.ContentAdapter;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.BookItem;

public class ContentPopup extends BaseDialogFragment {

    public static final String TAG = ContentPopup.class.getSimpleName();

    RecyclerView lv;
    ArrayList<BookItem> list;
    LinearLayoutManager manager;
    ContentAdapter adapter;

    String curChannel;
    String curContent;

    ContentPopupListener listener;

    public static ContentPopup newInstance(String tag, String channel, String content) {
        ContentPopup d = new ContentPopup();

        d.curChannel = channel;
        d.curContent = content;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.popup_content;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {


//        lv = fv(R.id.lv);
//        list = new ArrayList<>();
//        list.addAll(db.getBookItemByChannel(curChannel));
//
//        manager = new GridLayoutManager(getActivity(), 1);
//        lv.setLayoutManager(manager);
////        adapter = new ContentAdapter(getActivity(), R.layout.item_content, list, new ReAdapter.ReOnItemClickListener() {
////            @Override
////            public void OnItemClick(int position, Object item) {
////                if (listener != null)
////                    listener.onSelectedContent((BookItem) item);
////                dismiss();
////            }
////
////            @Override
////            public void OnItemLongClick(int position, Object item) {
////
////            }
////        });
//        lv.setAdapter(adapter);
//        adapter.setSelectedItem(curContent);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void setListener(ContentPopupListener listener) {
        this.listener = listener;
    }

    public static interface ContentPopupListener {
        public void onSelectedContent(BookItem item);
    }
}

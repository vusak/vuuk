package io.jmobile.vuuk.ui.dialog;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.ReAdapter;
import io.jmobile.vuuk.adapter.TimerAdapter;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.TimerItem;

public class TimerPopup extends BaseDialogFragment {
    public static final String TAG = TimerPopup.class.getSimpleName();

    RecyclerView lv;
    TextView title;
    ArrayList<TimerItem> list;
    LinearLayoutManager manager;
    TimerAdapter adapter;

    long curTimer;
    long[] timers = {
            TimerItem.TIMER_NONE,
            TimerItem.TIMER_30_MINUTES,
            TimerItem.TIMER_1_HOUR,
            TimerItem.TIMER_3_HOURS,
            TimerItem.TIMER_6_HOURS,
            TimerItem.TIMER_9_HOURS
    };

    TimerPopupListener listener;

    public static TimerPopup newInstance(String tag) {
        TimerPopup d = new TimerPopup();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        curTimer = sp.getAutoPlayTimer();
    }

    @Override
    public int getLayoutId() {
        return R.layout.popup_timer;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        title = fv(R.id.text_title);
        title.setText(r.s(R.string.set_finish_timer));

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        for (int i = 0; i < timers.length; i++)
            list.add(new TimerItem(getActivity(), timers[i]));

        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new TimerAdapter(getActivity(), R.layout.item_timer, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                if (listener != null)
                    listener.onSelecteCTimer((TimerItem) item);
                dismiss();
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);
        adapter.setSelectedItem(curTimer);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void setListener(TimerPopupListener listener) {
        this.listener = listener;
    }

    public static interface TimerPopupListener {
        public void onSelecteCTimer(TimerItem item);
    }
}

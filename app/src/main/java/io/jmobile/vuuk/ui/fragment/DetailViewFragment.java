package io.jmobile.vuuk.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseFragment;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.FileUtil;
import io.jmobile.vuuk.common.ImageUtil;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.ChannelItem;
import io.jmobile.vuuk.network.Api;
import io.jmobile.vuuk.network.JNetworkMonitor;
import io.jmobile.vuuk.ui.EpubViewActivity;
import io.jmobile.vuuk.ui.PDFViewActivity;
import io.jmobile.vuuk.ui.VideoViewActivity;
import io.jmobile.vuuk.ui.dialog.CommentDialog;
import io.jmobile.vuuk.ui.dialog.LoginDialog;
import io.jmobile.vuuk.ui.dialog.MessageDialog;

import static io.jmobile.vuuk.common.Common.API_CODE_POST_BOOK_FILE_VERSION_CHECK;
import static io.jmobile.vuuk.common.Common.ARG_EPUB_URL;
import static io.jmobile.vuuk.common.Common.ARG_PDF_URL;
import static io.jmobile.vuuk.common.Common.ARG_VIDEO_URL;

public class DetailViewFragment extends BaseFragment implements JNetworkMonitor.OnChangeNetworkStatusListener {

    String bookId;
    BookItem item;

    ImageButton refreshButton;
    ImageView thumbnail, readImage, openImage;
    TextView categoryText, titleText, detailText, infoText, dateText, byText, readText, openText;
    TextView viewCount, likeCount, commentCount;

    LinearLayout readButton, openButton;
    LinearLayout categoryLayout;

    ImageButton likeButton;
    LinearLayout sendButton;
    Api api;
    private DetailViewListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_detail_view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        networkMonitor.setListener(this);
        initView();
    }

    @Override
    public void handleApiMessage(Message m) {
        boolean res = m.getData().getBoolean(Common.ARG_RESULT);

        if (m.what == Common.API_CODE_POST_SET_LIKES || m.what == Common.API_CODE_POST_BOOK_RECOUNT || m.what == Common.API_CODE_POST_BOOK_FILE_VERSION_CHECK) {
            if (res) {
                if (m.what == API_CODE_POST_BOOK_FILE_VERSION_CHECK) {
                    boolean update = m.getData().getBoolean(Common.ARG_FILE_UPDATE);
                    if (LogUtil.DEBUG_MODE)
                        Util.showToast(getActivity(), item.getBookTitle() + " :: " + (update ? "true" : "false"));

                    File file = new File(sp.getDownloadDirectory(), item.getChannelId());
                    File p = new File(file.getAbsoluteFile(), item.getBookId());
                    if (update) {
                        FileUtil.delete(p);
                        setButtonState();
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_required_update_download));
                        dlg.setNegativeLabel(r.s(R.string.cancel));
                        dlg.setPositiveLabel(r.s(R.string.button_download));
                        dlg.setPositiveListener((dialog, tag) -> {
                            if (listener != null) {
                                listener.onDownloadButtonClicked(item);
                            }
                        });
                        sdf(dlg);
                    } else {

                        boolean pdf = item.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF);
                        Intent intent = new Intent(getActivity(), pdf ? PDFViewActivity.class : VideoViewActivity.class);
                        intent.putExtra(pdf ? ARG_PDF_URL : ARG_VIDEO_URL, p.getAbsolutePath());
                        startActivity(intent);
                    }
                } else
                    setBookId(bookId);
            } else {
                String message = m.getData().getString(Common.ARG_ERROR_MESSAGE);
                if (!TextUtils.isEmpty(message) && message.equalsIgnoreCase(Common.RESULT_CODE_ERROR_VERSION)) {
                    if (listener != null)
                        listener.onVersionUpdate();
                }
            }
        }
    }

    public BookItem getItem() {
        return item;
    }

    public void setBookId(String id) {
        bookId = id;
        item = db.getBookItem(bookId);

        if (item.getBookThumbnail() == null) {
            if (Util.isNetworkAbailable(getActivity()))
                Glide.with(getActivity()).load(item.getBookThumbnailUrl()).asBitmap().centerCrop().placeholder(R.drawable.img_logo_vuuk_100).into(thumbnail);
            else
                thumbnail.setImageResource(R.drawable.img_logo_vuuk_100);
        } else
            thumbnail.setImageBitmap(ImageUtil.getImage(item.getBookThumbnail()));
//        if (item.getBookThumbnail() == null)
//            thumbnail.setImageBitmap(ImageUtil.getImageFromURL(item.getBookThumbnailUrl()));
//        else
//            thumbnail.setImageBitmap(ImageUtil.getImage(item.getBookThumbnail()));
        categoryText.setText(item.getCategoryName());
        titleText.setText(item.getBookTitle());
        detailText.setText(item.getBookDetail());
        long size = item.getBookFileSize();
        infoText.setText(item.getBookType() + " / " + Util.getFileSize(size));
        dateText.setText(item.getBookPublicationDate());
        byText.setText(item.getBookPublishedBy());

        viewCount.setText(item.getBookViewCount());
        likeCount.setText(item.getBookLikeCount());
        commentCount.setText(item.getBookCommentCount());

        refreshButton = fv(R.id.button_refresh);
        refreshButton.setVisibility(View.GONE);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Util.isNetworkAbailable(getActivity())) {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_check_network));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                } else
                    api.getBookCountStatus(bookId);
            }
        });

        readButton.setVisibility(item.getBookType().equals(BookItem.TYPE_VIDEO) ? View.GONE : View.VISIBLE);
        openButton.setVisibility(item.getBookType().equals(BookItem.TYPE_EPUB) ? View.GONE : View.VISIBLE);
        ChannelItem cItem = db.getChannelItem(sp.getSelectedChannelId());
        categoryLayout.setVisibility(cItem.useChannelCategory() ? View.VISIBLE : View.GONE);

        likeButton.setImageResource(item.isBookLike() ? R.drawable.button_like_on : R.drawable.button_like_off);
        setButtonState();
    }

    private void initView() {
        thumbnail = fv(R.id.image_thumbnail);

        categoryText = fv(R.id.text_category);
        titleText = fv(R.id.text_title);
        detailText = fv(R.id.text_detail);
        infoText = fv(R.id.text_info);
        dateText = fv(R.id.text_p_date);
        byText = fv(R.id.text_p_by);

        categoryLayout = fv(R.id.layout_category);

        viewCount = fv(R.id.text_count_view);
        likeCount = fv(R.id.text_count_favorite);
        commentCount = fv(R.id.text_count_comment);

        readText = fv(R.id.text_read);
        readImage = fv(R.id.image_read);
        readButton = fv(R.id.button_read);
        readButton.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), EpubViewActivity.class);
            intent.putExtra(ARG_EPUB_URL, item.getBookViewUrl());
            startActivity(intent);
        });

        openText = fv(R.id.text_open);
        openImage = fv(R.id.image_open);
        openButton = fv(R.id.button_open);
        openButton.setOnClickListener(v -> {
            File file = new File(sp.getDownloadDirectory(), item.getChannelId());
            File p = new File(file.getAbsoluteFile(), item.getBookId());
            if (p.exists()) {
                if (!Util.isNetworkAbailable(getActivity())) {
                    boolean pdf = item.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF);
                    Intent intent = new Intent(getActivity(), pdf ? PDFViewActivity.class : VideoViewActivity.class);
                    intent.putExtra(pdf ? ARG_PDF_URL : ARG_VIDEO_URL, p.getAbsolutePath());
                    startActivity(intent);
                } else
                    api.checkBookFileVersion(item.getChannelId(), item.getBookId());

            } else {
                if (!Util.isNetworkAbailable(getActivity())) {
//                        || !Util.getNetworkConnectionType(getActivity()).equalsIgnoreCase("WIFI")) {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_check_network));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                } else {
                    if (listener != null)
                        listener.onDownloadButtonClicked(item);
                }
            }
        });

        likeButton = fv(R.id.button_send_favorite);
        likeButton.setOnClickListener(v -> {
            if (TextUtils.isEmpty(sp.getUserInfoKey())) {
                if (fdf(LoginDialog.TAG) == null) {

                    LoginDialog dlg = LoginDialog.newInstance(LoginDialog.TAG);
                    dlg.setListener(new LoginDialog.LoginDialogListener() {
                        @Override
                        public void onLoginButtonClick() {
                            if (listener != null)
                                listener.onLoginSucceed();
                        }

                        @Override
                        public void onCancelButtonClick() {


                        }

                        @Override
                        public void onVersionUpdate() {
                            if (listener != null)
                                listener.onVersionUpdate();
                        }
                    });

                    sdf(dlg);
                }
            } else {
                if (!Util.isNetworkAbailable(getActivity())) {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_check_network));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                } else
                    api.checkBookLike(item.getBookId());
            }
        });

        sendButton = fv(R.id.button_send_comment);
        sendButton.setOnClickListener(v -> {
            if (TextUtils.isEmpty(sp.getUserInfoKey())) {
                if (fdf(LoginDialog.TAG) == null) {

                    LoginDialog dlg = LoginDialog.newInstance(LoginDialog.TAG);
                    dlg.setListener(new LoginDialog.LoginDialogListener() {
                        @Override
                        public void onLoginButtonClick() {
                            if (listener != null)
                                listener.onLoginSucceed();
                        }

                        @Override
                        public void onCancelButtonClick() {
                        }

                        @Override
                        public void onVersionUpdate() {
                            if (listener != null)
                                listener.onVersionUpdate();
                        }
                    });

                    sdf(dlg);
                }
            } else {
                if (!Util.isNetworkAbailable(getActivity())) {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_check_network));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                } else {
                    if (fdf(CommentDialog.TAG) == null) {
                        CommentDialog dlg = CommentDialog.newInstance(CommentDialog.TAG, bookId);
                        dlg.setListener(() -> {
                            if (listener != null)
                                listener.onVersionUpdate();
                        });
                        dlg.setDismissListener((dialog, tag, byCloseButton) -> {
                            if (!Util.isNetworkAbailable(getActivity())) {
                                if (fdf(MessageDialog.TAG) == null) {
                                    MessageDialog dlg1 = MessageDialog.newInstance(MessageDialog.TAG);
                                    dlg1.setMessage(r.s(R.string.message_check_network));
                                    dlg1.setPositiveLabel(r.s(R.string.ok));
                                    sdf(dlg1);
                                }
                            } else
                                api.getBookCountStatus(bookId);
                        });
                        sdf(dlg);
                    }
                }
            }
        });
    }

    public void setButtonState() {
        boolean network = Util.isNetworkAbailable(getActivity());
        if (!item.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO)) {
            readButton.setEnabled(network);
            readButton.setBackgroundResource(network ? R.drawable.button_orange : R.drawable.button_gray);
            readText.setTextColor(ContextCompat.getColor(getActivity(), network ? R.color.white : R.color.gray7_a100));
            readImage.setImageResource(network ? R.drawable.ic_open_book_white_24 : R.drawable.ic_open_book_disable_gray_24);
        }
        if (!item.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB)) {
            File file = new File(sp.getDownloadDirectory(), item.getChannelId());
            File p = new File(file.getAbsoluteFile(), item.getBookId());

            int icon = R.drawable.ic_file_download_white_24;
            if (p.exists()) {
                if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF))
                    icon = R.drawable.ic_open_pdf_white_24;
                else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO))
                    icon = R.drawable.ic_open_video_white_24;
                else
                    icon = R.drawable.ic_open_book_white_24;
            }
//            openImage.setImageResource(p.exists() ? R.drawable.ic_open_book_white_24 : R.drawable.ic_file_download_white_24);
            openImage.setImageResource(icon);
            openText.setText(p.exists() ? R.string.button_open : R.string.button_download);
        }
    }

    @Override
    public void onNetworkChanged(int status) {
        if (item == null)
            return;

        boolean network = Util.isNetworkAbailable(getActivity());
        if (!item.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO)) {
            readButton.setEnabled(network);
            readButton.setBackgroundResource(network ? R.drawable.button_orange : R.drawable.button_gray);
            readText.setTextColor(ContextCompat.getColor(getActivity(), network ? R.color.white : R.color.gray7_a100));
            readImage.setImageResource(network ? R.drawable.ic_open_book_white_24 : R.drawable.ic_open_book_disable_gray_24);
        }
    }

    public void setListener(DetailViewListener listener) {
        this.listener = listener;
    }

    public static interface DetailViewListener {
        public void onDownloadButtonClicked(BookItem item);

        public void onLoginSucceed();

        public void onVersionUpdate();
    }


}

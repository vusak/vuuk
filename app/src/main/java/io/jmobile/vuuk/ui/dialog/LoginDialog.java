package io.jmobile.vuuk.ui.dialog;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.network.Api;

public class LoginDialog extends BaseDialogFragment {

    public static final String TAG = LoginDialog.class.getSimpleName();

    Button cancelButton, loginButton;

    EditText idEdit, pwEdit;
    Api api;
    private LoginDialogListener listener;

    public static LoginDialog newInstance(String tag) {
        LoginDialog d = new LoginDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_POST_LOGIN) {
            boolean result = m.getData().getBoolean(Common.ARG_LOGIN_RESULT);

            if (result) {
                if (listener != null)
                    listener.onLoginButtonClick();
                dismiss();
            } else {
                String message = m.getData().getString(Common.ARG_ERROR_MESSAGE);
                if (!TextUtils.isEmpty(message) && message.equalsIgnoreCase(Common.RESULT_CODE_ERROR_VERSION)) {
                    if (listener != null)
                        listener.onVersionUpdate();
                } else
                    Toast.makeText(getActivity(), "Login Fail", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setListener(LoginDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        setCancelable(false, false);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_login;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        cancelButton = fv(R.id.button_cancel);
        cancelButton.setOnClickListener(v -> {
            if (listener != null)
                listener.onCancelButtonClick();

            dismiss();
        });

        loginButton = fv(R.id.button_login);
        loginButton.setOnClickListener(v -> {
            if (!checkEmpty())
                return;

            Util.hideKeyBoard(idEdit);
            Util.hideKeyBoard(pwEdit);
            if (!Util.isNetworkAbailable(getActivity())) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_check_network));
                    dlg.setPositiveLabel(r.s(R.string.ok));
                    sdf(dlg);
                }
            } else
                api.login(idEdit.getText().toString().trim(), pwEdit.getText().toString().trim());
        });

        idEdit = fv(R.id.edit_id);
        pwEdit = fv(R.id.edit_pw);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private boolean checkEmpty() {
        if (idEdit != null && TextUtils.isEmpty(idEdit.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Empty ID", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (pwEdit != null && TextUtils.isEmpty(pwEdit.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Empty PW", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static interface LoginDialogListener {
        public void onLoginButtonClick();

        public void onCancelButtonClick();

        public void onVersionUpdate();
    }
}

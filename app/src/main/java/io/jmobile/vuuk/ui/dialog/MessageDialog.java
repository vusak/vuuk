package io.jmobile.vuuk.ui.dialog;


import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseDialogFragment;

public class MessageDialog extends BaseDialogFragment {

    public static final String TAG = MessageDialog.class.getSimpleName();

    private static final String TAG_TITLE = "title";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_POSITIVE_LABEL = "positive_label";
    private static final String TAG_NEGATIVE_LABEL = "negative_label";
    private static final String TAG_SHOW_PROGRESS = "show_progress";
    private static final String TAG_NOTI = "noti";
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };
    private TextView titleText, messageText; //, notiText;
    //    private ProgressBar progressBar;
    private View buttonLayout;
    private Button positiveButton, negativeButton;
    //    private LinearLayout titleLayout;
    private LinearLayout bodyLayout;
    private boolean showProgress;
    private String title, message, positiveLabel, negativeLabel, noti;

    public static MessageDialog newInstance(String tag) {
        MessageDialog d = new MessageDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCancelable(false, false);
        if (savedInstanceState != null) {
            title = savedInstanceState.getString(TAG_TITLE);
            message = savedInstanceState.getString(TAG_MESSAGE);
            positiveLabel = savedInstanceState.getString(TAG_POSITIVE_LABEL);
            negativeLabel = savedInstanceState.getString(TAG_NEGATIVE_LABEL);
            showProgress = savedInstanceState.getBoolean(TAG_SHOW_PROGRESS);
            noti = savedInstanceState.getString(TAG_NOTI);
        } else {
            if (title == null)
                title = "";
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_message;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        titleText = fv(R.id.text_dlg_title);
//        titleLayout = fv(R.id.layout_dlg_title);
        messageText = fv(R.id.text_message);
//        progressBar = fv(R.id.progress);
        buttonLayout = fv(R.id.layout_button);
        positiveButton = fv(R.id.button_positive);
        negativeButton = fv(R.id.button_negative);
        bodyLayout = fv(R.id.layout_body);
//        notiText = fv(R.id.text_noti);

        initializeViews();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.8f;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            rate = 0.5f;

        params.width = (int) (dm.widthPixels * rate);
        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(TAG_TITLE, title);
        outState.putString(TAG_MESSAGE, message);
        outState.putString(TAG_POSITIVE_LABEL, positiveLabel);
        outState.putString(TAG_NEGATIVE_LABEL, negativeLabel);
        outState.putBoolean(TAG_SHOW_PROGRESS, showProgress);
        outState.putString(TAG_NOTI, noti);
    }

    public MessageDialog setTitle(String title) {
        this.title = title;

        if (!isViewCreated())
            return this;

        if (title != null)
            titleText.setText(title);

        titleText.setVisibility(!TextUtils.isEmpty(title) ? View.VISIBLE : View.GONE);
        bodyLayout.setBackgroundResource(!TextUtils.isEmpty(title) ? R.drawable.dialog_body : R.drawable.dialog_background);
//        titleLayout.setVisibility(!TextUtils.isEmpty(title) ? View.VISIBLE : View.GONE);
        return this;
    }

    public MessageDialog setMessage(String message) {
        this.message = message;

        if (!isViewCreated())
            return this;

        messageText.setText(message != null ? message : "");
        return this;
    }

//    public MessageDialog setNoti(String noti) {
//        this.noti = noti;
//
//        if (!isViewCreated())
//            return this;
//
//        if (noti != null)
//            notiText.setText(noti);
//        notiText.setVisibility(noti != null ? View.VISIBLE : View.GONE);
//        return this;
//    }

    public MessageDialog setPositiveLabel(String positiveLabel) {
        this.positiveLabel = positiveLabel;

        if (!isViewCreated())
            return this;

        if (positiveLabel != null)
            positiveButton.setText(positiveLabel);

        positiveButton.setVisibility(positiveLabel != null ? View.VISIBLE : View.GONE);

        setButtonLayout();
        return this;
    }

    public MessageDialog setNegativeLabel(String negativeLabel) {
        this.negativeLabel = negativeLabel;

        if (!isViewCreated())
            return this;

        if (negativeLabel != null)
            negativeButton.setText(negativeLabel);
        negativeButton.setVisibility(negativeLabel != null ? View.VISIBLE : View.GONE);

        setButtonLayout();
        return this;
    }

//    public MessageDialog showProgress(boolean showProgress) {
//        this.showProgress = showProgress;
//
//        if (!isViewCreated())
//            return this;
//
//        progressBar.setVisibility(showProgress ? View.VISIBLE : View.GONE);
//        return this;
//    }

    private void initializeViews() {
        positiveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (positiveListener != null)
                    positiveListener.onDialogPositive(MessageDialog.this, tag);

                dismiss();
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (negativeListener != null)
                    negativeListener.onDialogNegative(MessageDialog.this, tag);

                dismiss();
            }
        });

        setTitle(title);
        setMessage(message);
        setPositiveLabel(positiveLabel);
        setNegativeLabel(negativeLabel);
//        showProgress(showProgress);
//        setNoti(noti);
    }

    private boolean isViewCreated() {
        return negativeButton != null;
    }

    private void setButtonLayout() {
        buttonLayout.setVisibility(positiveLabel == null && negativeLabel == null ? View.GONE : View.VISIBLE);
    }

    public void setDisapearDelay(long delay) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, delay);
    }
}

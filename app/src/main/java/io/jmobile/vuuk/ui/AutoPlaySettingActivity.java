package io.jmobile.vuuk.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.ContentAdapter;
import io.jmobile.vuuk.adapter.PlaylistAdapter;
import io.jmobile.vuuk.adapter.ReAbstractViewHolder;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.ChannelItem;
import io.jmobile.vuuk.data.PlayListItem;

public class AutoPlaySettingActivity extends BaseActivity {

    private static final int TYPE_NONE = 0;
    private static final int TYPE_ADD = 1;
    private static final int TYPE_EDIT = 2;
    ImageButton cancelButton, okButton, editButton;
    LinearLayout autoplayLayout, emptyLayout, addContentLayout;
    TextView titleText;
    Switch autoPlaySwitch;
    RecyclerView lv;
    ItemTouchHelper helper;
    LinearLayoutManager manager;
    ArrayList<PlayListItem> playlist;
    ArrayList<ChannelItem> channelList;
    PlaylistAdapter playlistAdapter;
    ContentAdapter adapter;
    ExpandableListView expandableLv;
    Map<String, List<BookItem>> map;
    Map<String, String> channelMap;
    NestedScrollView scrollView;
    private int type = TYPE_NONE;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_auto_play);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.orange_a100));
        }

        initView();
        changedType();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initView() {
        titleText = fv(R.id.text_title);

        cancelButton = fv(R.id.button_cancel);
        cancelButton.setOnClickListener(v -> {
            if (type != TYPE_NONE) {
                type = TYPE_NONE;
                changedType();
            } else {
                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, resultIntent);
                finish();
            }
        });

        editButton = fv(R.id.button_edit);
        editButton.setOnClickListener(v -> {
            type = TYPE_EDIT;
            changedType();
        });

        okButton = fv(R.id.button_save);
        okButton.setVisibility(View.GONE);
        okButton.setOnClickListener(v -> {
            if (type == TYPE_ADD && adapter.getCheckedList().size() > 0) {
                for (BookItem item : adapter.getCheckedList()) {
                    PlayListItem playItem = new PlayListItem(item);
                    playItem.setPlaylistId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                    playItem.setIndex((int) db.getNextDBId(PlayListItem.TABLE_NAME));
                    db.insertOrupdatePlaylistItem(playItem);
                }
            } else if (type == TYPE_EDIT) {
                db.deleteAllPlaylistItems();
                for (int i = 0; i < playlist.size(); i++) {
                    PlayListItem item = playlist.get(i);
                    item.setIndex(i + 1);
                    db.insertOrupdatePlaylistItem(item);
                }
            }

            type = TYPE_NONE;
            changedType();
        });

        scrollView = fv(R.id.scroll);
        scrollView.post(() -> scrollView.scrollTo(0, 0));
        autoplayLayout = fv(R.id.layout_auto_play);

        autoPlaySwitch = fv(R.id.switch_auto_play);
        autoPlaySwitch.setChecked(sp.isAutoPlaySetting());
        autoPlaySwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            sp.setAutoPlaySetting(isChecked);
        });

        addContentLayout = fv(R.id.layout_add);
        addContentLayout.setOnClickListener(v -> {
            type = TYPE_ADD;
            changedType();
        });

        channelList = new ArrayList<>();
        channelList.addAll(db.getJoinedChannelItems());
        playlist = new ArrayList<>();
        playlist.addAll(db.getPlaylistItems());

        emptyLayout = fv(R.id.layout_empty);
        emptyLayout.setVisibility(playlist.size() > 0 ? View.GONE : View.VISIBLE);
        lv = fv(R.id.lv);
        manager = new GridLayoutManager(this, 1);
        lv.setLayoutManager(manager);
        playlistAdapter = new PlaylistAdapter(this, R.layout.item_playlist, playlist, new PlaylistAdapter.PlaylistAdapterListener() {
            @Override
            public void onDeleteButtonClick(int position, PlayListItem item) {
                playlist.remove(position);
                playlistAdapter.notifyItemRemoved(position);
            }

            @Override
            public void onStartDrag(ReAbstractViewHolder holder) {
                helper.startDrag(holder);
            }

            @Override
            public void OnItemClick(int position, PlayListItem item) {
            }

            @Override
            public void OnItemLongClick(int position, PlayListItem item) {

            }
        });
        lv.setAdapter(playlistAdapter);
        helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        expandableLv = fv(R.id.lv_expandable);
        expandableLv.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            String channel = channelList.get(groupPosition).getChannelId();
            BookItem item = map.get(channel).get(childPosition);
            adapter.changedCheckList(item);

            int count = adapter.getCheckedList().size();
            okButton.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
            titleText.setText(count > 0 ? String.format(r.s(R.string.set_format_selected), count) : r.s(R.string.add_content));
            return false;
        });
        map = new HashMap<>();
        channelMap = new HashMap<>();
        channelList = new ArrayList<>();
        adapter = new ContentAdapter(this, map, channelList);
        expandableLv.setAdapter(adapter);
    }

    private void changedType() {
        editButton.setVisibility(type == TYPE_NONE ? View.VISIBLE : View.GONE);
        okButton.setVisibility(type == TYPE_EDIT ? View.VISIBLE : View.GONE);

        autoplayLayout.setVisibility(type == TYPE_NONE ? View.VISIBLE : View.GONE);
        addContentLayout.setVisibility(type == TYPE_NONE ? View.VISIBLE : View.GONE);

        scrollView.setVisibility(type != TYPE_ADD ? View.VISIBLE : View.GONE);
        scrollView.post(() -> scrollView.scrollTo(0, 0));
        expandableLv.setVisibility(type == TYPE_ADD ? View.VISIBLE : View.GONE);

        playlistAdapter.setEditMode(type == TYPE_EDIT);

        String title = r.s(R.string.set_auto_playlist);
        switch (type) {
            case TYPE_EDIT:
                title = r.s(R.string.edit_playlist);
                break;

            case TYPE_ADD:
                title = r.s(R.string.add_content);
                break;
        }
        titleText.setText(title);

        refreshBookItems();
    }

    private void refreshBookItems() {
        playlist.clear();
        playlist.addAll(db.getPlaylistItems());
        emptyLayout.setVisibility(playlist.size() > 0 ? View.GONE : View.VISIBLE);
        editButton.setVisibility(type == TYPE_NONE && playlist.size() > 0 ? View.VISIBLE : View.GONE);

        channelList.clear();
        channelList.addAll(db.getJoinedChannelItems());

        map.clear();
        for (ChannelItem item : channelList) {
            ArrayList<BookItem> temp = new ArrayList<>();
            temp.addAll(db.getBookItemByChannel(item.getChannelId()));
            map.put(item.getChannelId(), new ArrayList<BookItem>());
            channelMap.put(item.getChannelId(), item.getChannelName());
            for (BookItem book : temp) {
                map.get(item.getChannelId()).add(book);
            }
        }
        adapter.notifyDataSetChanged();
        playlistAdapter.setChannelMap(channelMap);

        for (int i = 0; i < channelList.size(); i++)
            expandableLv.expandGroup(i);
    }

    private void movePlaylistItem(int oldPos, int newPos) {
        PlayListItem item = playlist.get(oldPos);
        playlist.remove(oldPos);
        playlist.add(newPos, item);
        playlistAdapter.notifyItemMoved(oldPos, newPos);
    }

    private void deletePlaylistItem(int position) {
        playlist.remove(position);
        playlistAdapter.notifyItemRemoved(position);
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                movePlaylistItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                deletePlaylistItem(viewHolder.getAdapterPosition());
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;

                    itemView.setAlpha(Math.abs(1f - (Math.abs(dX) / itemView.getWidth())));
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return playlistAdapter != null && playlistAdapter.isEditMode();
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }
}

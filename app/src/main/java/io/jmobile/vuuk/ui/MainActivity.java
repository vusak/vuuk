package io.jmobile.vuuk.ui;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.BookAdapter;
import io.jmobile.vuuk.adapter.ChannelAdapter;
import io.jmobile.vuuk.adapter.ReAdapter;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.base.GridDividerItemDecoration;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.ImageUtil;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.CategoryItem;
import io.jmobile.vuuk.data.ChannelItem;
import io.jmobile.vuuk.data.PlayListItem;
import io.jmobile.vuuk.network.Api;
import io.jmobile.vuuk.network.BookDownloadManager;
import io.jmobile.vuuk.network.JNetworkMonitor;
import io.jmobile.vuuk.network.ScreenReceiver;
import io.jmobile.vuuk.ui.dialog.AddChannelDialog;
import io.jmobile.vuuk.ui.dialog.BookFilterDialog;
import io.jmobile.vuuk.ui.dialog.CategoryPopup;
import io.jmobile.vuuk.ui.dialog.LoginDialog;
import io.jmobile.vuuk.ui.dialog.MessageDialog;
import io.jmobile.vuuk.ui.fragment.DetailViewFragment;

import static io.jmobile.vuuk.common.Common.ACTIVITY_EPUB;
import static io.jmobile.vuuk.common.Common.ACTIVITY_PDF;
import static io.jmobile.vuuk.common.Common.ACTIVITY_VIDEO;
import static io.jmobile.vuuk.common.Common.ARG_EPUB_URL;
import static io.jmobile.vuuk.common.Common.ARG_MULTI_MODE;
import static io.jmobile.vuuk.common.Common.ARG_PDF_URL;
import static io.jmobile.vuuk.common.Common.ARG_SLEEP_MODE;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE_POWER_CONNECT;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE_POWER_DISCONNECT;
import static io.jmobile.vuuk.common.Common.ARG_START_TYPE_SCREEN_OFF;
import static io.jmobile.vuuk.common.Common.ARG_VIDEO_URL;

public class MainActivity extends BaseActivity implements DetailViewFragment.DetailViewListener, BookDownloadManager.BookDownloadListener, JNetworkMonitor.OnChangeNetworkStatusListener,
        SwipeRefreshLayout.OnRefreshListener {

    private final int REQUEST_EPUB = 1001;
    private final int REQUEST_PDF = 1002;
    private final int REQUEST_VIDEO = 1003;

    private final String KEY_BOOK_ID = "book_id";

    ImageButton homeButton, autoplayButton, repeatButton;
    ImageButton menuButton, backButton, settingButton, categoryButton;
    LinearLayout downloadButton, loginButton, infoLayout, emptyLayout;
    TextView categoryText, tagText;//, categoryText, countText, tagText;
    Button logoutButton;
    ImageView profileImage;
    TextView channelText, nicknameText, userIdText;
    SwipeRefreshLayout refreshLayout;
    RecyclerView lv, lv_channel;
    LinearLayoutManager manager, channelManager;
    GridDividerItemDecoration dividerItemDecoration;
    BookAdapter adapter;
    ChannelAdapter channelAdapter;
    ArrayList<BookItem> list, listAdapter;
    ArrayList<ChannelItem> channelList;
    DrawerLayout drawerLayout;
    LinearLayout addButton;
    ImageButton closeButton;
    LinearLayout mainLayout;
    FrameLayout detailLayout;
    DetailViewFragment detailFragment;
    ImageButton filterButton;
    ImageView typeImage;
    ProgressBar progress;
    Intent startIntent;
    Api api;
    String bookId = null;
    private BroadcastReceiver receiver = null;
    private ArrayList<PlayListItem> playlist;
    private int playIndex = 0;

    @Override
    public void onBackPressed() {
        if (detailLayout != null && detailLayout.getVisibility() == View.VISIBLE)
            visibleDetailView(false);
        else if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.LEFT))
            drawerLayout.closeDrawers();
        else {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_exit));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveListener((dialog, tag) -> finish());
                dlg.setPositiveLabel(r.s(R.string.exit));
                sdf(dlg);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();

        if (isFinishing()) {
            downloadManager.cancelAll();
            api.removeCallbacksAndMessages(null);
        }

        downloadManager.removeListener(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        receiver = new ScreenReceiver();
        registerReceiver(receiver, filter);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();

        downloadManager.addListener(this);
        networkMonitor.setListener(this);

        if (!Util.isNetworkAbailable(this)) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_check_network));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }
        } else
            api.getChannelList();

        startIntent = getIntent();
        String type = startIntent.getStringExtra(ARG_START_TYPE);
        if (!TextUtils.isEmpty(type)) {
            if (type.equalsIgnoreCase(ARG_START_TYPE_POWER_CONNECT) && sp.isAutoDownloadStart()) {
                if (!Util.isNetworkAbailable(MainActivity.this)
                        || !Util.getNetworkConnectionType(MainActivity.this).equalsIgnoreCase("WIFI"))
                    return;
                else
                    startDownload();
            } else if (type.equalsIgnoreCase(ARG_START_TYPE_POWER_DISCONNECT)) {
                if (downloadManager != null && !downloadManager.isEmpty()) {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_download_continue));
                        dlg.setPositiveLabel(r.s(R.string.str_continue));
                        dlg.setNegativeLabel(r.s(R.string.stop));
                        dlg.setDisapearDelay(5000);
                        dlg.setNegativeListener((dialog, tag) -> {
                            if (downloadManager != null)
                                downloadManager.cancelAll();
                            db.restateBookDownloadState();
                        });
                        sdf(dlg);
                    }
                }
            } else if (type.equalsIgnoreCase(ARG_START_TYPE_SCREEN_OFF)) {
                startPlayList();
//                String name = startIntent.getStringExtra(ARG_CURRENT_ACTIVITY);
//                startAutoPlay(name);
            }
        }

        if (savedInstanceState != null) {
            bookId = savedInstanceState.getString(KEY_BOOK_ID);
            if (!TextUtils.isEmpty(bookId)) {
                detailLayout.post(() -> {
                    detailFragment.setBookId(bookId);
                    visibleDetailView(true);
                });

            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(KEY_BOOK_ID, detailLayout.getVisibility() == View.VISIBLE ? bookId : null);
    }

    @Override
    public void handleApiMessage(Message m) {
        boolean success = m.getData().getBoolean(Common.ARG_RESULT);
        if (success) {
            if (m.what == Common.API_CODE_POST_BOOK_LIST) {
                initBookList();
            } else if (m.what == Common.API_CODE_POST_CHANNEL_LIST) {
                sp.setFirstOpen(false);
                if (sp.getSelectedChannelId().equalsIgnoreCase(sp.DEFAULT_SELECTED_CHANNEL_ID)) {
                    if (Util.isNetworkAbailable(MainActivity.this))
                        api.getCategoryList(sp.getSelectedChannelId());
                }

                channelList.clear();
                for (ChannelItem item : db.getChannelItems()) {
                    if (item.isChannelJoined())
                        channelList.add(item);
                    if (!TextUtils.isEmpty(item.getChannelThumbnailUrl()))
                        new AddChannelLogo().execute(item);
                }
                if (channelAdapter != null)
                    channelAdapter.notifyDataSetChanged();

                setChannel();
            } else if (m.what == Common.API_CODE_POST_CATEGORY_LIST) {
                if (TextUtils.isEmpty(sp.getBookCategory())) {
                    categoryText.setText("ALL");
                } else {
                    CategoryItem categoryItem = db.getCategoryItem(sp.getBookCategory());
                    categoryText.setText(categoryItem.getCategoryName());
                }
            } else if (m.what == Common.API_CODE_POST_BOOK_FILE_VERSION_CHECK) {

            } else if (m.what == Common.API_CODE_POST_BOOK_FILE_VERSION_SAVE) {
                BookItem bookItem = m.getData().getParcelable(Common.ARG_BOOK_ITEM);
                BookItem dbItem = db.getBookItem(bookItem.getBookId());
                String version = m.getData().getString(Common.ARG_FILE_VERSION);
                if (LogUtil.DEBUG_MODE) {
                    String message = dbItem.getBookTitle() + " version " + dbItem.getBookFileVersion() + "/" + version;
                    Util.showToast(MainActivity.this, message);
                    LogUtil.log(message);
                }
            }
        } else {
            if (refreshLayout != null)
                refreshLayout.setRefreshing(false);
            String message = m.getData().getString(Common.ARG_ERROR_MESSAGE);
            if (!TextUtils.isEmpty(message) && message.equalsIgnoreCase(Common.RESULT_CODE_ERROR_VERSION)) {
                update();
            } else if (!TextUtils.isEmpty(message) && message.equalsIgnoreCase(Common.RESULT_CODE_ERROR_NO_DATA)) {
                initBookList();
            }
        }
        super.handleApiMessage(m);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_EPUB:
            case REQUEST_PDF:
            case REQUEST_VIDEO:
                if (resultCode == RESULT_OK) {
                    playIndex++;
                    startPlay();
                }
                break;
        }
    }

    private void initView() {
        tagText = fv(R.id.text_tag);
        tagText.setText("");
//        countText = fv(R.id.text_count);
//        countText.setText("");
        homeButton = fv(R.id.button_home);
        homeButton.setOnClickListener(v -> {
            if (detailLayout != null && detailLayout.getVisibility() == View.VISIBLE)
                visibleDetailView(false);
//                detailLayout.setVisibility(View.GONE);
        });

        autoplayButton = fv(R.id.button_auto_play);
        autoplayButton.setOnClickListener(v -> {
            startPlayList();
//            startAutoPlay("");
        });

        repeatButton = fv(R.id.button_repeat);
        repeatButton.setOnClickListener(v -> {
            startRepeatPlay();
        });

        drawerLayout = fv(R.id.drawer);

        menuButton = fv(R.id.button_menu);
        menuButton.setOnClickListener(v -> drawerLayout.openDrawer(Gravity.LEFT, true));

        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> {
            if (detailLayout != null && detailLayout.getVisibility() == View.VISIBLE)
                visibleDetailView(false);
        });

        closeButton = fv(R.id.button_close);
        closeButton.setOnClickListener(v -> drawerLayout.closeDrawers());

        channelText = fv(R.id.text_channel);
        channelText.setVisibility(View.GONE);

        settingButton = fv(R.id.button_setting);
        settingButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);
        });

        loginButton = fv(R.id.button_login);
        loginButton.setOnClickListener(v -> {
            if (fdf(LoginDialog.TAG) == null) {

                LoginDialog dlg = LoginDialog.newInstance(LoginDialog.TAG);
                dlg.setListener(new LoginDialog.LoginDialogListener() {
                    @Override
                    public void onLoginButtonClick() {
                        String url = sp.getUserInfoProfileImg();
                        if (!TextUtils.isEmpty(url)) {
                            File file = new File(app.DEFAULT_PROFILE_FOLDER, sp.getSelectedChannelId());
                            ImageUtil.saveThumbnailFromURL(url, file.getAbsolutePath());

                            profileImage.setImageBitmap(ImageUtil.getThumbnailFromFilepath(file.getAbsolutePath()));
                        }
                        setLoginStatus();
                    }

                    @Override
                    public void onCancelButtonClick() {
                    }

                    @Override
                    public void onVersionUpdate() {
                        update();
                    }
                });

                sdf(dlg);
            }
        });

        infoLayout = fv(R.id.layout_user_info);
        nicknameText = fv(R.id.text_user_nickname);
        userIdText = fv(R.id.text_user_id);
        logoutButton = fv(R.id.button_logout);
        logoutButton.setOnClickListener(v -> {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_logout));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.logout));
                dlg.setPositiveListener((dialog, tag) -> {
                    sp.clearUserInfo();
                    setLoginStatus();
                });
                sdf(dlg);
            }
        });

        categoryText = fv(R.id.text_category);
//        categoryLabel = fv(R.id.text_category_label);
        categoryButton = fv(R.id.button_category);
        categoryButton.setOnClickListener(v -> {
            if (fdf(CategoryPopup.TAG) == null) {
                CategoryPopup popup = CategoryPopup.newInstance(CategoryPopup.TAG);
                popup.setPositiveListener((dialog, tag) -> {
                    if (TextUtils.isEmpty(sp.getBookCategory())) {
                        categoryText.setText(r.s(R.string.type_all));
                    } else {
                        CategoryItem categoryItem = db.getCategoryItem(sp.getBookCategory());
                        categoryText.setText(categoryItem.getCategoryName());
                    }
                    refreshList();
                });
                sdf(popup);
            }
        });


        profileImage = fv(R.id.image_profile);

        progress = fv(R.id.progress);
        progress.setVisibility(View.GONE);
        downloadButton = fv(R.id.button_download_all);
        downloadButton.setVisibility(View.GONE);
        downloadButton.setOnClickListener(v -> {
            if (!Util.isNetworkAbailable(MainActivity.this)
                    || !Util.getNetworkConnectionType(MainActivity.this).equalsIgnoreCase("WIFI")) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_check_network));
                    dlg.setPositiveLabel(r.s(R.string.ok));
                    sdf(dlg);
                }
            } else {
                int index = 0;
                for (BookItem item : db.getBookItemByChannel(sp.getSelectedChannelId())) {
                    if (!item.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB)
                            && item.getBookDownloadStatus() != BookItem.DOWNLOAD_STATE_DONE) {

                        index++;
                        item.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_READY);
                        db.updateBookStates(item);
                        downloadManager.addToRequestList(item.getBookId());
                    }
                }
                tagText.setText("Download Start [ " + index + " ]");
                tagText.setVisibility(index > 0 ? View.VISIBLE : View.GONE);
            }
        });

        mainLayout = fv(R.id.layout_main);
        emptyLayout = fv(R.id.layout_empty);

        refreshLayout = fv(R.id.layout_refresh);
        refreshLayout.setColorSchemeResources(R.color.orange_a100);
        refreshLayout.setOnRefreshListener(this);

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        listAdapter = new ArrayList<>();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            manager = new GridLayoutManager(this, getResources().getInteger(R.integer.land_book_column));
        else
            manager = new GridLayoutManager(this, getResources().getInteger(R.integer.port_book_column));
        lv.setLayoutManager(manager);
        dividerItemDecoration = new GridDividerItemDecoration(this);
        lv.addItemDecoration(dividerItemDecoration);
        adapter = new BookAdapter(this, R.layout.item_book_grid, listAdapter, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                BookItem bItem = (BookItem) item;
                if (detailFragment != null)
                    detailFragment.setBookId(bItem.getBookId());

                bookId = bItem.getBookId();
                visibleDetailView(true);
//                detailLayout.setVisibility(View.VISIBLE);
//                Intent intent = new Intent(MainActivity.this, DetailViewActivity.class);
//                intent.putExtra("book_id", bItem.getBookId());
//                startActivity(intent);
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);

        addButton = fv(R.id.button_add);
        addButton.setOnClickListener(v -> {
            if (fdf(AddChannelDialog.TAG) == null) {
                AddChannelDialog dlg = AddChannelDialog.newInstance(AddChannelDialog.TAG);
                dlg.setListener(new AddChannelDialog.AddBookmarkDialogListener() {
                    @Override
                    public void onAddButtonClick(ChannelItem item) {
                        item.setChannelJoined(true);
                        item.setChannelAt(System.currentTimeMillis());
                        db.updateChannelJoined(item);

                        channelList.clear();
                        channelList.addAll(db.getJoinedChannelItems());
                        if (channelAdapter != null)
                            channelAdapter.notifyDataSetChanged();

                        sp.setSelectedChannelId(item.getChannelId());
                        if (!Util.isNetworkAbailable(MainActivity.this)) {
                            if (fdf(MessageDialog.TAG) == null) {
                                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                dlg.setMessage(r.s(R.string.message_check_network));
                                dlg.setPositiveLabel(r.s(R.string.ok));
                                sdf(dlg);
                            }
                        } else
                            api.getCategoryList(item.getChannelId());

                        if (channelAdapter != null)
                            channelAdapter.notifyDataSetChanged();
                        drawerLayout.closeDrawers();
                        if (detailLayout != null && detailLayout.getVisibility() == View.VISIBLE)
                            visibleDetailView(false);
//                            detailLayout.setVisibility(View.GONE);

                        sp.setBookType(BookItem.TYPE_ALL);
                        typeImage.setImageResource(R.drawable.ic_all_navy_40);
                        setChannel();
                    }

                    @Override
                    public void onCancelButtonClick() {

                    }

                    @Override
                    public void onVersionUpdate() {
                        update();
                    }
                });
                sdf(dlg);
            }
        });

        lv_channel = fv(R.id.lv_channel);
        channelManager = new GridLayoutManager(this, 1);
        lv_channel.setLayoutManager(channelManager);
        channelList = new ArrayList<>();
        channelList.addAll(db.getJoinedChannelItems());
        channelAdapter = new ChannelAdapter(this, sp, R.layout.item_channel, channelList, new ChannelAdapter.ChannelAdapterListener() {
            @Override
            public void onDeleteButtonClick(int position, final ChannelItem item) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setTitle(r.s(R.string.delete));
                    dlg.setMessage(String.format(r.s(R.string.message_delete_channel), item.getChannelName()));
                    dlg.setNegativeLabel(r.s(R.string.cancel));
                    dlg.setPositiveLabel(r.s(R.string.delete));
                    dlg.setPositiveListener((dialog, tag) -> {
                        ChannelItem cItem = item;
                        cItem.setChannelJoined(false);
                        cItem.setChannelAt(0);
                        db.updateChannelJoined(cItem);

                        if (sp.getSelectedChannelId().equals(item.getChannelId())) {
                            sp.setSelectedChannelId(SPController.DEFAULT_SELECTED_CHANNEL_ID);
                            if (channelAdapter != null)
                                channelAdapter.setSelectedPosition(-1);
                            channelText.setText("");
                            channelText.setVisibility(View.GONE);
                        }

                        channelList.clear();
                        channelList.addAll(db.getJoinedChannelItems());
                        if (channelAdapter != null)
                            channelAdapter.notifyDataSetChanged();
                    });
                    sdf(dlg);
                }

            }

            @Override
            public void OnItemLongClick(int position, ChannelItem item) {

            }

            @Override
            public void OnItemClick(int position, ChannelItem item) {
                drawerLayout.closeDrawers();
                if (channelAdapter != null)
                    channelAdapter.setSelectedPosition(position);
                String channel = sp.getSelectedChannelId();
                sp.setSelectedChannelId(item.getChannelId());
                if (!TextUtils.isEmpty(channel) && !channel.equalsIgnoreCase(sp.getSelectedChannelId()))
                    sp.setBookCategory("");
                if (detailLayout != null && detailLayout.getVisibility() == View.VISIBLE)
                    visibleDetailView(false);

                setChannel();
            }
        });


        lv_channel.setAdapter(channelAdapter);

        detailLayout = fv(R.id.layout_detail);

        detailFragment = new DetailViewFragment();
        detailFragment.setListener(this);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.layout_detail, detailFragment);
        transaction.commit();

        typeImage = fv(R.id.image_type);


        filterButton = fv(R.id.button_filter);
        filterButton.setOnClickListener(v -> {
            if (fdf(BookFilterDialog.TAG) == null) {
                final BookFilterDialog typePopup = BookFilterDialog.newInstance(BookFilterDialog.TAG);
                typePopup.setPositiveListener((dialog, tag) -> {
                    setTypeImage();
                    refreshList();
                });
                sdf(typePopup);
            }
        });

        setTypeImage();
        setLoginStatus();
        setChannel();
        visibleDetailView(false);
    }

    private void setLoginStatus() {
        if (!TextUtils.isEmpty(sp.getUserInfoKey())) {
            infoLayout.setVisibility(View.VISIBLE);
            nicknameText.setText(sp.getUserInfoNickname());
            userIdText.setText(new String(Base64.decode(sp.getUserInfoID(), 0)));
            loginButton.setVisibility(View.GONE);
            File file = new File(app.DEFAULT_PROFILE_FOLDER, sp.getSelectedChannelId());
            if (file.exists())
                profileImage.setImageBitmap(ImageUtil.getThumbnailFromFilepath(file.getAbsolutePath()));
        } else {
            infoLayout.setVisibility(View.GONE);
            loginButton.setVisibility(View.VISIBLE);
        }
    }

    private void setTypeImage() {
        if (typeImage != null) {
            if (sp.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB))
                typeImage.setImageResource(R.drawable.ic_open_book_orange_40);
            else if (sp.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF))
                typeImage.setImageResource(R.drawable.ic_open_pdf_navy_40);
            else if (sp.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO))
                typeImage.setImageResource(R.drawable.ic_open_video_blue_40);
            else
                typeImage.setImageResource(R.drawable.ic_all_navy_40);
        }
    }

    private void setChannel() {
        String channel = sp.getSelectedChannelId();
        if (!TextUtils.isEmpty(channel)) {
            ChannelItem cItem = db.getChannelItem(channel);
            if (cItem == null)
                return;
            if (cItem.getChannelType().equalsIgnoreCase("1")) {
                if (cItem.getChannelThumbnail() != null) {
                    homeButton.setImageBitmap(ImageUtil.getImage(cItem.getChannelThumbnail()));
                    channelText.setText("");
                    channelText.setVisibility(View.GONE);
                } else {
                    homeButton.setImageResource(R.drawable.ic_vuuk_logo_color);
                    if (channelText != null && cItem != null) {
                        channelText.setText(cItem.getChannelName());
                        channelText.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                homeButton.setImageResource(R.drawable.ic_vuuk_logo_color);
                if (channelText != null && cItem != null) {
                    channelText.setText(cItem.getChannelName());
                    channelText.setVisibility(View.VISIBLE);
                }
            }

            if (cItem.useChannelCategory() && Util.isNetworkAbailable(this))
                api.getCategoryList(cItem.getChannelId());

            if (TextUtils.isEmpty(sp.getBookCategory())) {
                categoryText.setText("ALL");
            } else {
                CategoryItem categoryItem = db.getCategoryItem(sp.getBookCategory());
                categoryText.setText(categoryItem.getCategoryName());
            }
            categoryText.setVisibility(cItem.useChannelCategory() ? View.VISIBLE : View.GONE);
            categoryButton.setVisibility(cItem.useChannelCategory() ? View.VISIBLE : View.GONE);
        }

        if (Util.isNetworkAbailable(this))
            api.getBookList();
        initBookList();
    }

    private void refreshList() {
        for (BookItem downItem : list) {
            if (!downItem.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB) && downItem.getBookDownloadStatus() != BookItem.DOWNLOAD_STATE_DONE) {
//                downloadButton.setVisibility(View.VISIBLE);
                break;
            }
        }

        String type = sp.getBookType();
        listAdapter.clear();

        if (type.equalsIgnoreCase(BookItem.TYPE_ALL))
            listAdapter.addAll(list);
        else {
            for (BookItem item : list) {
                if (item.getBookType().equalsIgnoreCase(type))
                    listAdapter.add(item);
            }
        }

        ChannelItem cItem = db.getChannelItem(sp.getSelectedChannelId());
        if (cItem.useChannelCategory()) {
            if (!TextUtils.isEmpty(sp.getBookCategory())) {
                CategoryItem categoryItem = db.getCategoryItem(sp.getBookCategory());
                ArrayList<BookItem> temp = new ArrayList<>();
                temp.addAll(listAdapter);
                listAdapter.clear();
                for (BookItem bItem : temp) {
                    if (bItem.getCategoryId().contains(categoryItem.getCategoryId()))
                        listAdapter.add(bItem);
                }
            }
        }

        String sort = sp.getBookOrder();

        if (listAdapter != null && listAdapter.size() > 0) {
            if (sort.equalsIgnoreCase(BookItem.BOOK_ORDER_NEW))
                Collections.sort(listAdapter, BookItem.COMPARATOR_NEW_DESC);
            else if (sort.equalsIgnoreCase(BookItem.BOOK_ORDER_HIT))
                Collections.sort(listAdapter, BookItem.COMPARATOR_HIT_DESC);
            else if (sort.equalsIgnoreCase(BookItem.BOOK_ORDER_BEST))
                Collections.sort(listAdapter, BookItem.COMPARATOR_BEST_DESC);
        }

        if (adapter != null)
            adapter.notifyDataSetChanged();

        emptyLayout.setVisibility(listAdapter.size() > 0 ? View.GONE : View.VISIBLE);
        for (BookItem item : list) {
            if (item.getBookThumbnail() == null)
                new AddBookThumbnail().execute(item);
        }
    }

    private void initBookList() {
        if (refreshLayout != null && refreshLayout.isRefreshing())
            refreshLayout.setRefreshing(false);

        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();

        list.addAll(db.getBookItemByChannel(sp.getSelectedChannelId()));
        refreshList();
    }

    @Override
    public void onVersionUpdate() {
        update();
    }

    @Override
    public void onLoginSucceed() {
        String url = sp.getUserInfoProfileImg();
        if (!TextUtils.isEmpty(url)) {
            File file = new File(app.DEFAULT_PROFILE_FOLDER, sp.getSelectedChannelId());
            ImageUtil.saveThumbnailFromURL(url, file.getAbsolutePath());

            profileImage.setImageBitmap(ImageUtil.getThumbnailFromFilepath(file.getAbsolutePath()));
        }
        setLoginStatus();
    }

    @Override
    public void onDownloadButtonClicked(BookItem item) {
        item.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_READY);
        db.updateBookStates(item);
        downloadManager.addToRequestList(item.getBookId());
    }

    @Override
    public void onBookDownloadStart(BookItem book) {
        if (api != null)
            api.setBookFileVersion(book.getChannelId(), book.getBookId());
    }

    @Override
    public void onBookDownloadError(BookItem book, int errorCode) {
        Toast.makeText(this, "ERROR " + book.getBookTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBookDownloadProgress(BookItem book, long progress, long total) {
        if (this.progress != null && this.progress.getVisibility() == View.GONE)
            this.progress.setVisibility(View.VISIBLE);
//        Toast.makeText(this, book.getBookTitle() + progress, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBookDownloadSuccess(BookItem book) {
        int count = 0;
        for (BookItem downItem : db.getBookItemByChannel(sp.getSelectedChannelId())) {
            if (!downItem.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB) &&
                    downItem.getBookDownloadStatus() != BookItem.DOWNLOAD_STATE_DONE) {
                count++;
            }
        }

//        downloadButton.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
        tagText.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
        if (Util.isTablet(this))
            tagText.setText("[ Downloading : " + count + " ]     Download Succeed " + book.getBookTitle());
        else
            tagText.setText("[ Downloading : " + count + " ]");

        if (downloadManager.isEmpty()) {
            progress.setVisibility(View.GONE);
            tagText.setVisibility(View.GONE);
        }
        if (detailLayout != null && detailLayout.getVisibility() == View.VISIBLE)
            detailFragment.setButtonState();
    }

    @Override
    public void onNetworkChanged(int status) {
        if (downloadManager == null || downloadManager.isEmpty())
            return;

        if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
//            showAlertMessageDialog(r.s(R.string.dlg_msg_check_network));
            if (downloadManager != null)
                downloadManager.cancelAll();
            db.restateBookDownloadState();
//
//            if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1)
//                ingFragment.refreshLIst();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE) {
//            showAlertMessageDialog(r.s(R.string.dlg_msg_check_wifi));
//            if (downloadManager != null)
//                downloadManager.cancelAll();
//            db.restateBookDownloadState();
//
//            if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1)
//                ingFragment.refreshLIst();
        }
    }

    private void startDownload() {
        if (!Util.isNetworkAbailable(MainActivity.this)
                || !Util.getNetworkConnectionType(MainActivity.this).equalsIgnoreCase("WIFI")) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_check_network));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }
        } else {
            int index = 0;
            for (BookItem item : db.getBookItemByChannel(sp.getSelectedChannelId())) {
                if (!item.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB)
                        && item.getBookDownloadStatus() != BookItem.DOWNLOAD_STATE_DONE) {

                    index++;
                    item.setBookDownloadStatus(BookItem.DOWNLOAD_STATE_READY);
                    db.updateBookStates(item);
                    downloadManager.addToRequestList(item.getBookId());
                }
            }
            tagText.setText("Download Start [ " + index + " ]");
            tagText.setVisibility(index > 0 ? View.VISIBLE : View.GONE);
        }
    }

    private void startRepeatPlay() {
        sp.setAutoPlayStartTime(System.currentTimeMillis());
        if (detailFragment != null && detailLayout != null && detailLayout.getVisibility() == View.VISIBLE) {
            BookItem item = detailFragment.getItem();
            if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB)) {
                if (Util.isNetworkAbailable(this)) {
                    Intent intent = new Intent(this, EpubViewActivity.class);
                    intent.putExtra(ARG_EPUB_URL, item.getBookViewUrl());
                    intent.putExtra(ARG_SLEEP_MODE, true);
                    intent.putExtra(ARG_MULTI_MODE, false);
                    startActivityForResult(intent, REQUEST_EPUB);
                } else {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_check_network));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                }
            } else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF)) {

                if (Util.isNetworkAbailable(this)) {
                    onDownloadButtonClicked(db.getBookItem(item.getBookId()));
                    Intent intent = new Intent(this, EpubViewActivity.class);
                    intent.putExtra(ARG_EPUB_URL, item.getBookViewUrl());
                    intent.putExtra(ARG_SLEEP_MODE, true);
                    intent.putExtra(ARG_MULTI_MODE, false);
                    startActivityForResult(intent, REQUEST_EPUB);
                } else {
                    File file = new File(sp.getDownloadDirectory(), item.getChannelId());
                    File p = new File(file.getAbsoluteFile(), item.getBookId());
                    if (p.exists()) {
                        Intent intent = new Intent(this, PDFViewActivity.class);
                        intent.putExtra(ARG_PDF_URL, p.getAbsolutePath());
                        intent.putExtra(ARG_SLEEP_MODE, true);
                        intent.putExtra(ARG_MULTI_MODE, false);
                        startActivityForResult(intent, REQUEST_PDF);
                    } else {
                        if (fdf(MessageDialog.TAG) == null) {
                            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                            dlg.setMessage(r.s(R.string.message_check_network));
                            dlg.setPositiveLabel(r.s(R.string.ok));
                            sdf(dlg);
                        }
                    }

                }

            } else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO)) {
                File file = new File(sp.getDownloadDirectory(), item.getChannelId());
                File p = new File(file.getAbsoluteFile(), item.getBookId());
                if (p.exists()) {
                    Intent intent = new Intent(this, VideoViewActivity.class);
                    intent.putExtra(ARG_VIDEO_URL, p.getAbsolutePath());
                    intent.putExtra(ARG_SLEEP_MODE, true);
                    intent.putExtra(ARG_MULTI_MODE, false);
                    startActivityForResult(intent, REQUEST_VIDEO);
                } else {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_required_download));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                }
            }
        }

    }

    private void startPlayList() {
        sp.setAutoPlayStartTime(System.currentTimeMillis());

        if (playlist == null)
            playlist = new ArrayList<>();
        playlist.clear();
        playIndex = 0;

        playlist.addAll(db.getPlaylistItems());
        if (playlist.size() <= 0) {
            for (BookItem item : listAdapter) {
                playlist.add(new PlayListItem(item));
            }

            if (playlist.size() <= 0) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_empty_content));
                    dlg.setPositiveLabel(r.s(R.string.ok));
                    sdf(dlg);
                }
                return;
            }
        }
        startPlay();
    }

    private void startPlay() {
        if (!Util.isNetworkAbailable(this)) {
            int count = 0;
            for (PlayListItem item : playlist) {
                if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB))
                    count++;
                else {
                    File file = new File(sp.getDownloadDirectory(), item.getChannelId());
                    File p = new File(file.getAbsoluteFile(), item.getBookId());

                    if (!p.exists())
                        count++;
                }
            }

            if (count >= playlist.size()) {
                Util.showToast(MainActivity.this, r.s(R.string.message_check_network));
                return;
            }
        }

        if (playIndex >= playlist.size())
            playIndex = 0;
        PlayListItem item = playlist.get(playIndex);
        if (item == null) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_empty_content));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }
            return;
        }

        if (LogUtil.DEBUG_MODE) {
            Util.showToast(MainActivity.this, item.getBookTitle());
        }

        if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB)) {
            if (Util.isNetworkAbailable(this)) {
                Intent intent = new Intent(this, EpubViewActivity.class);
                intent.putExtra(ARG_EPUB_URL, item.getBookViewUrl());
                intent.putExtra(ARG_SLEEP_MODE, true);
                intent.putExtra(ARG_MULTI_MODE, true);
                startActivityForResult(intent, REQUEST_EPUB);
            } else {
//                if (fdf(MessageDialog.TAG) == null) {
//                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//                    dlg.setMessage(r.s(R.string.message_check_network));
//                    dlg.setPositiveLabel(r.s(R.string.ok));
//                    sdf(dlg);
//                }
                autoplayButton.postDelayed(() -> {
//                    Util.showToast(MainActivity.this, r.s(R.string.message_check_network));
                    playIndex++;
                    startPlay();
                    return;
                }, 100);

            }
        } else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF)) {
            File file = new File(sp.getDownloadDirectory(), item.getChannelId());
            File p = new File(file.getAbsoluteFile(), item.getBookId());
            if (Util.isNetworkAbailable(this)) {
                if (!p.exists())
                    onDownloadButtonClicked(db.getBookItem(item.getBookId()));
                Intent intent = new Intent(this, EpubViewActivity.class);
                intent.putExtra(ARG_EPUB_URL, item.getBookViewUrl());
                intent.putExtra(ARG_SLEEP_MODE, true);
                intent.putExtra(ARG_MULTI_MODE, true);
                startActivityForResult(intent, REQUEST_EPUB);
            } else {
                if (p.exists()) {
                    Intent intent = new Intent(this, PDFViewActivity.class);
                    intent.putExtra(ARG_PDF_URL, p.getAbsolutePath());
                    intent.putExtra(ARG_SLEEP_MODE, true);
                    intent.putExtra(ARG_MULTI_MODE, true);
                    startActivityForResult(intent, REQUEST_PDF);
                } else {
//                    if (fdf(MessageDialog.TAG) == null) {
//                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//                        dlg.setMessage(r.s(R.string.message_check_network));
//                        dlg.setPositiveLabel(r.s(R.string.ok));
//                        sdf(dlg);
//                    }
                    autoplayButton.postDelayed(() -> {
//                        Util.showToast(MainActivity.this, r.s(R.string.message_check_network));
                        playIndex++;
                        startPlay();
                        return;
                    }, 100);
                }

            }

        } else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO)) {
            File file = new File(sp.getDownloadDirectory(), item.getChannelId());
            File p = new File(file.getAbsoluteFile(), item.getBookId());
            if (p.exists()) {
                Intent intent = new Intent(this, VideoViewActivity.class);
                intent.putExtra(ARG_VIDEO_URL, p.getAbsolutePath());
                intent.putExtra(ARG_SLEEP_MODE, true);
                intent.putExtra(ARG_MULTI_MODE, true);
                startActivityForResult(intent, REQUEST_VIDEO);
            } else {
//                if (fdf(MessageDialog.TAG) == null) {
//                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//                    dlg.setMessage(r.s(R.string.message_required_download));
//                    dlg.setPositiveLabel(r.s(R.string.ok));
//                    sdf(dlg);
//                }
                if (Util.isNetworkAbailable(this))
                    onDownloadButtonClicked(db.getBookItem(item.getBookId()));
                autoplayButton.postDelayed(() -> {
//                    Util.showToast(MainActivity.this, r.s(R.string.message_check_network));
                    playIndex++;
                    startPlay();
                    return;
                }, 100);
            }
        }
    }

    private void startAutoPlay(String name) {
        if (!TextUtils.isEmpty(name) && name.equalsIgnoreCase(ACTIVITY_EPUB)) {
            Intent i = new Intent(this, EpubViewActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(ARG_SLEEP_MODE, true);
            i.putExtra(ARG_MULTI_MODE, true);
            startActivityForResult(i, REQUEST_EPUB);
        } else if (!TextUtils.isEmpty(name) && name.equalsIgnoreCase(ACTIVITY_PDF)) {
            Intent i = new Intent(this, PDFViewActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(ARG_SLEEP_MODE, true);
            i.putExtra(ARG_MULTI_MODE, true);
            startActivityForResult(i, REQUEST_PDF);
        } else if (!TextUtils.isEmpty(name) && name.equalsIgnoreCase(ACTIVITY_VIDEO)) {
            Intent i = new Intent(this, VideoViewActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(ARG_SLEEP_MODE, true);
            i.putExtra(ARG_MULTI_MODE, true);
            startActivityForResult(i, REQUEST_VIDEO);
        } else {
            BookItem item = null;

            if (detailFragment != null && detailLayout != null && detailLayout.getVisibility() == View.VISIBLE) {
                item = detailFragment.getItem();
            } else {
                if (TextUtils.isEmpty(sp.getAutoPlayContent())) {
                    if (listAdapter != null && listAdapter.size() > 0)
                        item = listAdapter.get(0);
//                        if (fdf(MessageDialog.TAG) == null) {
//                            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//                            dlg.setMessage("No selected Content");
//                            dlg.setNegativeLabel(r.s(R.string.cancel));
//                            dlg.setPositiveLabel(r.s(R.string.title_setting));
//                            dlg.setPositiveListener((dialog, tag) -> {
//                                Intent intent = new Intent(MainActivity.this, AutoPlaySettingActivity.class);
//                                startActivity(intent);
//                            });
//                            sdf(dlg);
//                        }
                } else {
                    item = db.getBookItem(sp.getAutoPlayContent());
                }
            }

            if (item == null) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_empty_content));
                    dlg.setPositiveLabel(r.s(R.string.ok));
                    sdf(dlg);
                }
                return;
            }
            if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_EPUB)) {
                if (Util.isNetworkAbailable(this)) {
                    Intent intent = new Intent(this, EpubViewActivity.class);
                    intent.putExtra(ARG_EPUB_URL, item.getBookViewUrl());
                    intent.putExtra(ARG_SLEEP_MODE, true);
                    intent.putExtra(ARG_MULTI_MODE, true);
                    startActivityForResult(intent, REQUEST_EPUB);
                } else {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_check_network));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                }
            } else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF)) {
                File file = new File(sp.getDownloadDirectory(), item.getChannelId());
                File p = new File(file.getAbsoluteFile(), item.getBookId());
                if (p.exists()) {
                    Intent intent = new Intent(this, PDFViewActivity.class);
                    intent.putExtra(ARG_PDF_URL, p.getAbsolutePath());
                    intent.putExtra(ARG_SLEEP_MODE, true);
                    intent.putExtra(ARG_MULTI_MODE, true);
                    startActivityForResult(intent, REQUEST_PDF);
                } else {
                    if (Util.isNetworkAbailable(this)) {
                        Intent intent = new Intent(this, EpubViewActivity.class);
                        intent.putExtra(ARG_EPUB_URL, item.getBookViewUrl());
                        intent.putExtra(ARG_SLEEP_MODE, true);
                        intent.putExtra(ARG_MULTI_MODE, true);
                        startActivityForResult(intent, REQUEST_EPUB);
                    } else {
                        if (fdf(MessageDialog.TAG) == null) {
                            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                            dlg.setMessage(r.s(R.string.message_check_network));
                            dlg.setPositiveLabel(r.s(R.string.ok));
                            sdf(dlg);
                        }
                    }
                }
            } else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO)) {
                File file = new File(sp.getDownloadDirectory(), item.getChannelId());
                File p = new File(file.getAbsoluteFile(), item.getBookId());
                if (p.exists()) {
                    Intent intent = new Intent(this, VideoViewActivity.class);
                    intent.putExtra(ARG_VIDEO_URL, p.getAbsolutePath());
                    intent.putExtra(ARG_SLEEP_MODE, true);
                    intent.putExtra(ARG_MULTI_MODE, true);
                    startActivityForResult(intent, REQUEST_VIDEO);
                } else {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setMessage(r.s(R.string.message_required_download));
                        dlg.setPositiveLabel(r.s(R.string.ok));
                        sdf(dlg);
                    }
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        if (Util.isNetworkAbailable(this))
            api.getBookList();
        else {
            refreshLayout.setRefreshing(false);
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_check_network));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }
        }
    }

    private void update() {
        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
        dlg.setMessage(r.s(R.string.message_required_update));
        dlg.setPositiveLabel(r.s(R.string.str_continue));
        dlg.setPositiveListener((dialog, tag) -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + getPackageName()));
            startActivity(intent);
            finish();
        });
        dlg.setNegativeLabel(r.s(R.string.cancel));
        dlg.setNegativeListener((dialog, tag) -> finish());
        sdf(dlg);
    }

    private void visibleDetailView(boolean show) {
        menuButton.setVisibility(show ? View.GONE : View.VISIBLE);
        backButton.setVisibility(show ? View.VISIBLE : View.GONE);
        detailLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        autoplayButton.setVisibility(show ? View.GONE : View.VISIBLE);
        repeatButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    class AddChannelLogo extends AsyncTask<ChannelItem, Void, ChannelItem> {
        @Override
        protected ChannelItem doInBackground(ChannelItem... channelItems) {
            ChannelItem item = channelItems[0];

            Bitmap bmp = ImageUtil.getImageFromURLNonTask(item.getChannelThumbnailUrl());
            if (bmp != null)
                item.setChannelThumbnail(ImageUtil.getImageBytes(bmp));

            return item;
        }

        @Override
        protected void onPostExecute(ChannelItem channelItem) {
            if (channelItem != null)
                db.insertOrUpdateChannelItem(channelItem);
        }
    }

    class AddBookThumbnail extends AsyncTask<BookItem, Void, BookItem> {
        @Override
        protected BookItem doInBackground(BookItem... params) {
            BookItem item = params[0];

            Bitmap bmp = ImageUtil.getImageFromURLNonTask(item.getBookThumbnailUrl());
            if (bmp != null)
                item.setBookThumbnail(ImageUtil.getImageBytes(bmp));
            return item;
        }

        @Override
        protected void onPostExecute(BookItem bookItem) {
            if (bookItem != null)
                db.insertOrUpdateBookItem(bookItem);

            if (adapter != null)
                adapter.updateThumbnail(bookItem);
        }
    }
}

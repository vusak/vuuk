package io.jmobile.vuuk.ui.dialog;


import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.adapter.CommentAdapter;
import io.jmobile.vuuk.adapter.ReAdapter;
import io.jmobile.vuuk.base.BaseDialogFragment;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.CommentItem;
import io.jmobile.vuuk.network.Api;

public class CommentDialog extends BaseDialogFragment {

    public static final String TAG = CommentDialog.class.getSimpleName();

    ImageButton sendButton;
    BookItem item;
    EditText commentEdit;
    TextView titleText;
    Api api;
    String bookId;

    LinearLayout emptyLayout;
    RecyclerView lv;
    LinearLayoutManager manager;
    ArrayList<CommentItem> list;
    CommentAdapter adapter;

    private CommentDialogListener listener;

    public static CommentDialog newInstance(String tag, String bookId) {
        CommentDialog d = new CommentDialog();

        d.bookId = bookId;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void handleApiMessage(Message m) {
        boolean result = m.getData().getBoolean(Common.ARG_RESULT);
        ArrayList<CommentItem> items = new ArrayList<>();
        if (result) {
            if (m.what == Common.API_CODE_POST_ADD_COMMENT) {
                list.clear();
                items = m.getData().getParcelableArrayList(Common.ARG_COMMENT_LIST);
                list.addAll(items);

                if (adapter != null)
                    adapter.notifyDataSetChanged();

                if (emptyLayout != null)
                    emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);

            } else if (m.what == Common.API_CODE_POST_COMMENT_LIST) {
                list.clear();
                items = m.getData().getParcelableArrayList(Common.ARG_COMMENT_LIST);
                list.addAll(items);

                if (adapter != null)
                    adapter.notifyDataSetChanged();

                if (emptyLayout != null)
                    emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);

            }
        } else {
            String message = m.getData().getString(Common.ARG_ERROR_MESSAGE);
            if (!TextUtils.isEmpty(message) && message.equalsIgnoreCase(Common.RESULT_CODE_ERROR_VERSION)) {
                if (listener != null)
                    listener.onVersionUpdate();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        item = db.getBookItem(bookId);
        api.getBookCommentList(bookId);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_comment;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        titleText = fv(R.id.text_title);
        titleText.setText(item.getBookTitle());
        commentEdit = fv(R.id.edit_comment);
        sendButton = fv(R.id.button_send_comment);
        sendButton.setOnClickListener(v -> {
            if (!checkEmpty())
                return;

            api.addBookComment(bookId, commentEdit.getText().toString().trim());
            commentEdit.setText("");
            Util.hideKeyBoard(commentEdit);
        });

        emptyLayout = fv(R.id.layout_empty);

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new CommentAdapter(getActivity(), sp, R.layout.item_comment, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {

            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

//        if (!Util.isTablet(getActivity())) {
        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.8f;
        params.width = (int) (dm.widthPixels * rate);
        getDialog().getWindow().setAttributes(params);
//        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private boolean checkEmpty() {
        if (commentEdit != null && TextUtils.isEmpty(commentEdit.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Empty Comment", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void setListener(CommentDialogListener listener) {
        this.listener = listener;
    }

    public static interface CommentDialogListener {
        public void onVersionUpdate();
    }
}

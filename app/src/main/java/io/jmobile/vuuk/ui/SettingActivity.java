package io.jmobile.vuuk.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.data.TimerItem;
import io.jmobile.vuuk.ui.dialog.IntervalPopup;
import io.jmobile.vuuk.ui.dialog.TimerPopup;

public class SettingActivity extends BaseActivity {

    ImageButton backButton;
    LinearLayout autoDownloadLayout, autoPlayLayout, finishTimerLayout, intervalLayout, autoStartLayout;
    TextView contentsText, timerText, autoPlayText, versionText, finishTimerText, intervalText;
    Switch autoDownloadCBox, autoStartCBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        initView();
        setSettingVal();
    }

    @Override
    protected void onResume() {
        setSettingVal();
        super.onResume();
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> finish());

        /** Auto playlist **/
        autoPlayLayout = fv(R.id.layout_auto_play);
        autoPlayLayout.setOnClickListener(v -> {
            Intent intent = new Intent(SettingActivity.this, AutoPlaySettingActivity.class);
            startActivityForResult(intent, 1);
        });

        autoPlayText = fv(R.id.text_auto_play);
//        contentsText = fv(R.id.text_set_contents);
//        contentsText.setVisibility(sp.isAutoPlaySetting() ? View.VISIBLE : View.GONE);
//        timerText = fv(R.id.text_set_timer);
//        timerText.setVisibility(sp.isAutoPlaySetting() ? View.VISIBLE : View.GONE);
        /** Finish Timer **/
        finishTimerLayout = fv(R.id.layout_finish_timer);
        finishTimerLayout.setOnClickListener(v -> {
            if (fdf(TimerPopup.TAG) == null) {
                TimerPopup popup = TimerPopup.newInstance(TimerPopup.TAG);
                popup.setListener(item -> {
                    if (item != null) {
                        finishTimerText.setText(item.getTitle());
                        finishTimerText.setTextColor(ContextCompat.getColor(SettingActivity.this, item.getTime() == 0 ? R.color.gray7_a100 : R.color.orange_a100));
                        sp.setAutoPlayTimer(item.getTime());
                    }
                });
                sdf(popup);
            }
        });

        finishTimerText = fv(R.id.text_finish_timer);
        /** Turning Interval **/
        intervalLayout = fv(R.id.layout_turning_interval);
        intervalLayout.setOnClickListener(v -> {
            if (fdf(IntervalPopup.TAG) == null) {
                IntervalPopup popup = IntervalPopup.newInstance(IntervalPopup.TAG);
                popup.setPositiveListener((dialog, tag) -> intervalText.setText(String.format(r.s(R.string.timer_format_second), sp.getPlayTurningInterval())));
                sdf(popup);
            }
        });

        intervalText = fv(R.id.text_turning_interval);

        /** Auto start setting **/
        autoStartLayout = fv(R.id.layout_auto_start);
        autoStartLayout.setOnClickListener(v -> {
            autoStartCBox.setChecked(!sp.isAutoStart());
        });

        autoStartCBox = fv(R.id.switch_auto_start);
        autoStartCBox.setOnCheckedChangeListener((buttonView, isChecked) -> sp.setAutoStart(isChecked));

        /** Download start setting **/
        autoDownloadLayout = fv(R.id.layout_auto_download);
        autoDownloadLayout.setOnClickListener(v -> autoDownloadCBox.setChecked(!sp.isAutoDownloadStart()));

        autoDownloadCBox = fv(R.id.switch_auto_download);
        autoDownloadCBox.setOnCheckedChangeListener((buttonView, isChecked) -> sp.setAutoDownloadStart(isChecked));

        /** Version **/
        versionText = fv(R.id.text_app_version);
    }

    private void setSettingVal() {
        autoPlayText.setText(r.s(sp.isAutoPlaySetting() ? R.string.on : R.string.off));
        autoPlayText.setTextColor(ContextCompat.getColor(this, sp.isAutoPlaySetting() ? R.color.orange_a100 : R.color.gray7_a100));
//        contentsText.setText(String.format(r.s(R.string.playlist_format_contents), db.getPlaylistItems().size()));
//        timerText.setText((new TimerItem(this, sp.getAutoPlayTimer())).getTitle());
        TimerItem item = new TimerItem(this, sp.getAutoPlayTimer());
        finishTimerText.setText(item.getTitle());
        finishTimerText.setTextColor(ContextCompat.getColor(SettingActivity.this, item.getTime() == 0 ? R.color.gray7_a100 : R.color.orange_a100));
        autoDownloadCBox.setChecked(sp.isAutoDownloadStart());
        autoStartCBox.setChecked(sp.isAutoStart());
        intervalText.setText(String.format(r.s(R.string.timer_format_second), sp.getPlayTurningInterval()));

        String version = "1.0.0";
        try {
            PackageInfo i = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = i.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            LogUtil.log(e.getMessage());
        }
        versionText.setText(version);
    }


}

package io.jmobile.vuuk.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;

import java.io.File;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.base.BaseActivity;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.LogUtil;

import static io.jmobile.vuuk.common.Common.ARG_MULTI_MODE;
import static io.jmobile.vuuk.common.Common.ARG_SLEEP_MODE;
import static io.jmobile.vuuk.common.Common.ARG_VIDEO_URL;
import static io.jmobile.vuuk.common.Util.offTimer;
import static io.jmobile.vuuk.common.Util.showToast;
import static io.jmobile.vuuk.common.Util.startTimer;

public class VideoViewActivity extends BaseActivity implements ExoPlayer.EventListener {
    SimpleExoPlayer player;
    SimpleExoPlayerView playerView;
    String url;

    Button backButton, closeButton;
    boolean sleepMode = false;
    boolean multiMode = false;

    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogUtil.DEBUG_MODE)
                showToast(VideoViewActivity.this, "Times up");
            finishViewer();
        }
    };

    private void finishViewer() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_video);

        url = getIntent().getStringExtra(ARG_VIDEO_URL);
        if (TextUtils.isEmpty(url)) {
            url = sp.getAutoPlayCurrentUrl();
            if (TextUtils.isEmpty(url)) {
                showToast(this, "ERROR!!!! URL is Empty!!!");
                finish();
            }
        } else
            sp.setAutoPlayCurrentUrl(url);
        sleepMode = getIntent().getBooleanExtra(ARG_SLEEP_MODE, false);
        multiMode = getIntent().getBooleanExtra(ARG_MULTI_MODE, false);
        if (sleepMode) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            if (sp.getAutoPlayTimer() > 0) {
                long delay = sp.getAutoPlayTimer() - (System.currentTimeMillis() - sp.getAutoPlayStartTime());

                if (LogUtil.DEBUG_MODE)
                    showToast(this, "Timer :: " + delay);
                if (delay > 0)
                    startTimer(this, delay);
            }
        }

        if (!url.isEmpty()) {
            AssetManager am = getResources().getAssets();
        }
        initView();


    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());
        backButton.setVisibility(sleepMode ? View.GONE : View.VISIBLE);

        closeButton = fv(R.id.button_close);
        closeButton.setOnClickListener(v -> {
            offTimer(this);
            onBackPressed();
        });
        closeButton.setVisibility(sleepMode ? View.VISIBLE : View.GONE);

//      1. Create a default TrackSelector
        final BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
//                new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();
        // 3. Create the player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);

        playerView = fv(R.id.player);
        playerView.setPlayer(player);
        playerView.setKeepScreenOn(true);
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                com.google.android.exoplayer2.util.Util.getUserAgent(this, "ExoPlayer"));

        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        Uri uri = Uri.fromFile(new File(url));
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource(uri, //Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"),
                dataSourceFactory, extractorsFactory, null, null);
        // Prepare the player with the source.
        player.addListener(this);
        player.prepare(videoSource);
        playerView.requestFocus();
        player.setPlayWhenReady(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(timeReceiver, new IntentFilter(Common.INTENTFILTER_BROADCAST_TIMER));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(timeReceiver);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case SimpleExoPlayer.STATE_ENDED:
                if (sleepMode) {
                    if (multiMode) {
                        setResult(RESULT_OK);
                        finish();
                    } else
                        player.seekTo(0);
                }
                break;
        }
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.release();
    }
}

package io.jmobile.vuuk;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import io.jmobile.vuuk.common.DBController;
import io.jmobile.vuuk.common.FileUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.network.BookDownloadManager;
import io.jmobile.vuuk.network.JNetworkMonitor;

public class BaseApplication extends Application {
    public static String DEFAULT_DOWNLOAD_FOLDER;
    public static String DEFAULT_PROFILE_FOLDER;
    private SPController sp;
    private DBController db;
    private ResourceWrapper r;
    private BookDownloadManager downloadManager;
    private JNetworkMonitor networkMonitor;

    private String version;
    private String os = "Android " + Build.VERSION.RELEASE;
    private String deviceName;
    private String locale;
    private String modelName = Build.MODEL;
    private String deviceIdentifier;
    private String contry;
    private String simContry;
    private TelephonyManager tManager;

    @SuppressLint("NewApi")
    @Override
    public void onCreate() {
        super.onCreate();

        sp = new SPController(getApplicationContext());
        db = new DBController(getApplicationContext());
        r = new ResourceWrapper(getResources());

        downloadManager = new BookDownloadManager(this);
        networkMonitor = new JNetworkMonitor(this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkMonitor, filter);

        DEFAULT_DOWNLOAD_FOLDER = FileUtil.getExternalFilesDir(getApplicationContext(), "Contents").getAbsolutePath();
        sp.setDownloadDirectory(DEFAULT_DOWNLOAD_FOLDER);
        DEFAULT_PROFILE_FOLDER = FileUtil.getExternalFilesDir(getApplicationContext(), "Profile").getAbsolutePath();

        version = Util.getVersion(getApplicationContext());
        deviceName = Build.MODEL;
        if (Build.VERSION.SDK_INT >= 24) {
            locale = getResources().getConfiguration().getLocales().get(0).getLanguage();
            contry = getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = getResources().getConfiguration().locale.getLanguage();
            contry = getResources().getConfiguration().locale.getCountry();
        }
        tManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        simContry = tManager.getSimCountryIso();

        deviceIdentifier = Util.sha256(Util.getMACAddress());
    }

    public void onNetworkChanged(int status) {
        if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE) {
            Toast.makeText(this, "NETWORK_TYPE_MOBILE", Toast.LENGTH_SHORT).show();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_WIFI) {
            Toast.makeText(this, "NETWORK_TYPE_WIFI", Toast.LENGTH_SHORT).show();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
            Toast.makeText(this, "NETWORK_TYPE_NOT_CONNECTED", Toast.LENGTH_SHORT).show();
        }
    }

    public SPController getSPController() {
        return this.sp;
    }

    public DBController getDBController() {
        return this.db;
    }

    public ResourceWrapper getResourceWrapper() {
        return r;
    }

    public BookDownloadManager getDownloadManager() {
        return this.downloadManager;
    }

    public JNetworkMonitor getNetworkMonitor() {
        return networkMonitor;
    }

    public String getOS() {
        return this.os;
    }

    public String getOSName() {
        return "Android";
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getVersion() {
        return this.version;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setConfiguration(String locale, String contry) {
        this.locale = locale;
        this.contry = contry;
    }

    public String getContry() {
        return this.contry;
    }

    public String getSimContry() {
        return this.simContry;
    }

    public void setSimContry(String val) {
        this.simContry = val;
    }

    public String getModelName() {
        return this.modelName;
    }

    public String getDeviceIdentifier() {
        return this.deviceIdentifier;
    }

    public static class ResourceWrapper {
        private Resources r;

        public ResourceWrapper(Resources r) {
            this.r = r;
        }

        public int px(int id) {
            return r.getDimensionPixelSize(id);
        }

        public String s(int id) {
            return r.getString(id);
        }

        public String[] sa(int id) {
            return r.getStringArray(id);
        }

        public int c(Context context, int id) {
            if (context == null)
                return 0;
            else
                return ContextCompat.getColor(context, id);
        }

        public Drawable d(Context context, int id) {
            if (context == null)
                return null;
            else
                return ContextCompat.getDrawable(context, id);
        }

        public boolean b(int id) {
            return r.getBoolean(id);
        }

        public int i(int id) {
            return r.getInteger(id);
        }

        public long l(int id) {
            return Long.parseLong(s(id));
        }
    }
}

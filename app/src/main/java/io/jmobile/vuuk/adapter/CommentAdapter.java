package io.jmobile.vuuk.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.data.CommentItem;

public class CommentAdapter extends ReSelectableAdapter<CommentItem, CommentAdapter.CommentHolder> {

    private Context context;
    private ArrayList<CommentItem> items = new ArrayList<>();
    private SPController sp;


    public CommentAdapter(Context context, SPController sp, int layoutId, ArrayList<CommentItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.sp = sp;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
        CommentItem item = items.get(position);

        if (item == null)
            return;

        holder.dateText.setText(item.getCommentCreateAt());
        holder.commentText.setText(item.getCommentValue());
        holder.nameText.setText(item.getCommentUserName());
        holder.idText.setText(item.getCommentUserId());


//        if (item.getChannelType().equalsIgnoreCase("1")) {
//            if (item.getChannelThumbnail() != null)
//                holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getChannelThumbnail()));
//            else {
//                if (Util.isNetworkAbailable(context))
//                    holder.thumbnail.setImageBitmap(ImageUtil.getImageFromURL(item.getChannelThumbnailUrl()));
//                else
//                    holder.thumbnail.setImageResource(R.drawable.ic_ondana_logo_color);
//            }
//        }
////        if (item.getChannelId().equalsIgnoreCase("jsecurity"))
////            holder.thumbnail.setImageResource(R.drawable.ic_jsecurity);
////        else if (item.getChannelId().equalsIgnoreCase("jiransoft"))
////            holder.thumbnail.setImageResource(R.drawable.ic_jsecurity);
//        else
//            holder.thumbnail.setImageResource(R.drawable.ic_ondana_logo_color);
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final CommentHolder holder = new CommentHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.nameText = holder.fv(R.id.text_user_name);
        holder.idText = holder.fv(R.id.text_user_id);
        holder.dateText = holder.fv(R.id.text_date);
        holder.commentText = holder.fv(R.id.text_comment);
        holder.profile = holder.fv(R.id.image_profile);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

//        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
//            @Override
//            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
//                if (listener != null && items.size() > position) {
//                    ((ChannelAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
//                }
//            }
//
//            @Override
//            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                return false;
//            }
//        });
        return holder;
    }

//    public static interface ChannelAdapterListener extends ReOnItemClickListener<ChannelItem> {
//        public void onDeleteButtonClick(int position, ChannelItem item);
//    }

    public class CommentHolder extends ReAbstractViewHolder {
        //        OnItemViewClickListener deleteButtonClickListener;
        LinearLayout root;
        TextView nameText, idText, dateText, commentText;
        ImageView profile;

        public CommentHolder(View itemView) {
            super(itemView);
        }

//        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
//            deleteButtonClickListener = listener;
//            if (listener != null && delButton != null)
//                delButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        deleteButtonClickListener.onItemViewClick(getAdapterPosition(), ChannelHolder.this);
//                    }
//                });
//        }

    }
}

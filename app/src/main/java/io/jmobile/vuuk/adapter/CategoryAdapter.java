package io.jmobile.vuuk.adapter;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.CategoryItem;

public class CategoryAdapter extends ReSelectableAdapter<CategoryItem, CategoryAdapter.CategoryHolder> {

    private Context context;
    private ArrayList<CategoryItem> items = new ArrayList<>();
    private int index = -1;


    public CategoryAdapter(Context context, int layoutId, ArrayList<CategoryItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        CategoryItem item = items.get(position);
        holder.title.setText(item.getCategoryName());
        holder.root.setBackgroundResource(index == position ? R.color.gray2_a100 : R.drawable.ripple_effect_main_transparent);
        holder.check.setVisibility(index == position ? View.VISIBLE : View.INVISIBLE);
        holder.depth.setVisibility(item.getCategoryPosition() > 1 ? View.VISIBLE : View.GONE);
        holder.title.setPadding((int) Util.convertDpToPixel(item.getCategoryPosition() > 1 ? 4 : 8, context), 0, 0, 0);
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final CategoryHolder holder = new CategoryHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.title = holder.fv(R.id.text_title);
        holder.depth = holder.fv(R.id.image_depth);
        holder.check = holder.fv(R.id.image_check);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void setSelectedItem(String id) {
        if (TextUtils.isEmpty(id))
            this.index = -1;
        else {
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).getCategoryId().equalsIgnoreCase(id)) {
                    index = i;
                    break;
                }
            }
        }
        notifyDataSetChanged();
    }


    public class CategoryHolder extends ReAbstractViewHolder {
        LinearLayout root;
        ImageView check, depth;
        TextView title;

        public CategoryHolder(View itemView) {
            super(itemView);
        }

    }
}

package io.jmobile.vuuk.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.ChannelItem;

public class ContentAdapter extends BaseExpandableListAdapter {
    private Context context;
    private Map<String, List<BookItem>> map;
    private List<ChannelItem> channelList;
    private LayoutInflater li;

    private List<BookItem> checkList = new ArrayList<>();

    public ContentAdapter(Activity context, Map<String, List<BookItem>> map, List<ChannelItem> channelList) {
        this.context = context;
        this.map = map;
        this.channelList = channelList;
        li = context.getLayoutInflater();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        String channel = channelList.get(groupPosition).getChannelId();
        return map.get(channel).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.item_content, parent, false);
        BookItem item = (BookItem) getChild(groupPosition, childPosition);

        int typeIdx = R.drawable.ic_open_book_orange_18;
        if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_PDF))
            typeIdx = R.drawable.ic_open_pdf_navy_18;
        else if (item.getBookType().equalsIgnoreCase(BookItem.TYPE_VIDEO))
            typeIdx = R.drawable.ic_open_video_blue_18;
        ImageView type = (ImageView) convertView.findViewById(R.id.image_type);
        type.setImageResource(typeIdx);

        CheckBox check = (CheckBox) convertView.findViewById(R.id.check);
        check.setChecked(checkList.contains(item));

        TextView title = (TextView) convertView.findViewById(R.id.text_title);
        title.setText(item.getBookTitle());
        title.setTextColor(ContextCompat.getColor(context, check.isChecked() ? R.color.orange_a100 : R.color.black_a100));

        View line = (View) convertView.findViewById(R.id.view_line);
        line.setVisibility(isLastChild ? View.GONE : View.VISIBLE);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        String name = channelList.get(groupPosition).getChannelId();
        return map.get(name).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return channelList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return channelList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.item_content_group, parent, false);

        TextView channelName = (TextView) convertView.findViewById(R.id.text_title);
        channelName.setText(((ChannelItem) getGroup(groupPosition)).getChannelName());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public List<BookItem> getCheckedList() {
        return this.checkList;
    }

    public void changedCheckList(BookItem item) {
        if (checkList.contains(item))
            checkList.remove(item);
        else
            checkList.add(item);

        notifyDataSetChanged();
    }

//    @Override
//    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//        if (convertView == null)
//            convertView = li.inflate(R.layout.item_history, parent, false);
//
////        holder.root.setBackgroundResource(item.isSelected() ? R.color.black_30 : R.color.transparent);
//        holder.title.setText(item.getBookTitle());
//        holder.title.setTextColor(ContextCompat.getColor(context, item.isSelected() ? R.color.orange_a100 : R.color.black_a100));
//
//        if (holder.add != null)
//            holder.add.setChecked(item.isSelected());
//
//        if (item.getBookType().equals(BookItem.TYPE_EPUB))
//            holder.type.setImageResource(R.drawable.ic_open_book_orange_18);
//        else if (item.getBookType().equals(BookItem.TYPE_PDF))
//            holder.type.setImageResource(R.drawable.ic_open_pdf_navy_18);
//        else if (item.getBookType().equals(BookItem.TYPE_VIDEO))
//            holder.type.setImageResource(R.drawable.ic_open_video_blue_18);
//    }
//
//    @Override
//    public BookHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        final BookHolder holder = new BookHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));
//
//        holder.root = holder.fv(R.id.layout_root);
//        holder.type = holder.fv(R.id.image_type);
//        holder.title = holder.fv(R.id.text_title);
//
//        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
//            @Override
//            public void onItemViewClick(int position, ReAbstractViewHolder h) {
//                if (listener != null && items.size() > position) {
//                    listener.OnItemClick(position, items.get(position));
//                }
//            }
//
//            @Override
//            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                return false;
//            }
//        });
//
//        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
//            @Override
//            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
//                if (listener != null && items.size() > position) {
//                    ((PlaylistAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
//                }
//            }
//
//            @Override
//            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                return false;
//            }
//        });
//        return holder;
//    }
//
//    public void setSelectedItem(String id) {
//        index = -1;
//        for (int i = 0; i < items.size(); i++) {
//            if (items.get(i).getBookId().equalsIgnoreCase(id)) {
//                index = i;
//                break;
//            }
//        }
//
//        notifyDataSetChanged();
//    }
//
//    public boolean isEditMode() {
//        return this.editMode;
//    }
//
//    public void setEditMode(boolean mode) {
//        this.editMode = mode;
//    }
//
//    public static interface PlaylistAdapterListener extends ReOnItemClickListener<BookItem> {
//        public void onDeleteButtonClick(int position , BookItem item);
//    }
//
//    public class BookHolder extends ReAbstractViewHolder {
//        OnItemViewClickListener deleteButtonClickListener;
//
//        LinearLayout root;
//        ImageView type, order;
//        TextView number, title, channel;
//        CheckBox add;
//        ImageButton deleteButton;
//
//        public BookHolder(View itemView) {
//            super(itemView);
//        }
//
//        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
//            deleteButtonClickListener = listener;
//            if (listener != null && deleteButton != null) {
//                deleteButton.setOnClickListener(v -> deleteButtonClickListener.onItemViewClick(getAdapterPosition(), BookHolder.this));
//            }
//        }
//    }
}

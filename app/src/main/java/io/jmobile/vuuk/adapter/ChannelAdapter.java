package io.jmobile.vuuk.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.common.ImageUtil;
import io.jmobile.vuuk.common.SPController;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.ChannelItem;

public class ChannelAdapter extends ReSelectableAdapter<ChannelItem, ChannelAdapter.ChannelHolder> {

    private Context context;
    private ArrayList<ChannelItem> items = new ArrayList<>();
    private int selectedPosition = -1;
    private SPController sp;


    public ChannelAdapter(Context context, SPController sp, int layoutId, ArrayList<ChannelItem> items, ChannelAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.sp = sp;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ChannelHolder holder, int position) {
        ChannelItem item = items.get(position);

        if (item == null)
            return;

        boolean isSelected = sp.getSelectedChannelId().equalsIgnoreCase(item.getChannelId());

        holder.title.setText(item.getChannelName());
        holder.root.setBackgroundResource(isSelected ? R.color.orange_a100_a : R.drawable.ripple_effect_main_transparent);
//        holder.title.setTextColor(ContextCompat.getColor(context, isSelected ? R.color.orange_a100 : R.color.gray5_a100));
        holder.delButton.setVisibility(isSelected ? View.GONE : View.VISIBLE);
        if (item.getChannelType().equalsIgnoreCase("1")) {
            if (item.getChannelThumbnail() != null)
                holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getChannelThumbnail()));
            else {
                if (Util.isNetworkAbailable(context)) {
                    Glide.with(context).load(item.getChannelThumbnailUrl()).asBitmap().placeholder(R.drawable.ic_vuuk_logo_color).into(new BitmapImageViewTarget(holder.thumbnail));
//                    Bitmap bitmap = ImageUtil.getImageFromURL(item.getChannelThumbnailUrl());
//                    if (bitmap != null) {
//                        holder.thumbnail.setImageBitmap(bitmap);
//                    } else
//                        holder.thumbnail.setImageResource(R.drawable.ic_vuuk_logo_color);
                } else
                    holder.thumbnail.setImageResource(R.drawable.ic_vuuk_logo_color);
            }
        }
//        if (item.getChannelId().equalsIgnoreCase("jsecurity"))
//            holder.thumbnail.setImageResource(R.drawable.ic_jsecurity);
//        else if (item.getChannelId().equalsIgnoreCase("jiransoft"))
//            holder.thumbnail.setImageResource(R.drawable.ic_jsecurity);
        else
            holder.thumbnail.setImageResource(R.drawable.ic_vuuk_logo_color);

    }

    @Override
    public ChannelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ChannelHolder holder = new ChannelHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.title = holder.fv(R.id.text_title);
        holder.delButton = holder.fv(R.id.button_delete);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    ((ChannelAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
        this.notifyDataSetChanged();
    }

    public static interface ChannelAdapterListener extends ReOnItemClickListener<ChannelItem> {
        public void onDeleteButtonClick(int position, ChannelItem item);
    }

    public class ChannelHolder extends ReAbstractViewHolder {
        OnItemViewClickListener deleteButtonClickListener;
        LinearLayout root;
        ImageView thumbnail;
        TextView title;
        ImageButton delButton;

        public ChannelHolder(View itemView) {
            super(itemView);
        }

        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
            deleteButtonClickListener = listener;
            if (listener != null && delButton != null)
                delButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteButtonClickListener.onItemViewClick(getAdapterPosition(), ChannelHolder.this);
                    }
                });
        }

    }
}

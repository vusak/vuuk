package io.jmobile.vuuk.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.common.ImageUtil;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.BookItem;

public class BookAdapter extends ReSelectableAdapter<BookItem, BookAdapter.BookHolder> {

    private Context context;
    private ArrayList<BookItem> items = new ArrayList<>();


    public BookAdapter(Context context, int layoutId, ArrayList<BookItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(BookHolder holder, int position) {
        BookItem item = items.get(position);

        if (item == null)
            return;

        holder.title.setText(item.getBookTitle());
        holder.author.setText("by " + item.getBookPublishedBy());
        if (item.getBookThumbnail() == null) {
            if (Util.isNetworkAbailable(context))
                Glide.with(context).load(item.getBookThumbnailUrl()).asBitmap().placeholder(R.drawable.img_logo_vuuk_100).into(new BitmapImageViewTarget(holder.thumbnail));
            else
                holder.thumbnail.setImageResource(R.drawable.img_logo_vuuk_100);
        } else
            holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getBookThumbnail()));

        if (item.getBookType().equals(BookItem.TYPE_EPUB))
            holder.type.setImageResource(R.drawable.ic_open_book_orange_18);
        else if (item.getBookType().equals(BookItem.TYPE_PDF))
            holder.type.setImageResource(R.drawable.ic_open_pdf_navy_18);
        else if (item.getBookType().equals(BookItem.TYPE_VIDEO))
            holder.type.setImageResource(R.drawable.ic_open_video_blue_18);
    }

    @Override
    public BookHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final BookHolder holder = new BookHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.type = holder.fv(R.id.image_type);
        holder.title = holder.fv(R.id.text_title);
        holder.author = holder.fv(R.id.text_by);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void updateThumbnail(BookItem _item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getBookId().equalsIgnoreCase(_item.getBookId())) {
                notifyItemChanged(i);
                break;
            }
        }
    }

    public class BookHolder extends ReAbstractViewHolder {
        LinearLayout root;
        ImageView thumbnail, type;
        TextView title, author;

        public BookHolder(View itemView) {
            super(itemView);
        }

    }
}

package io.jmobile.vuuk.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.common.ImageUtil;
import io.jmobile.vuuk.common.Util;
import io.jmobile.vuuk.data.ChannelItem;

public class ChannelListAdapter extends ReSelectableAdapter<ChannelItem, ChannelListAdapter.ChannelHolder> {

    private Context context;
    private ArrayList<ChannelItem> items = new ArrayList<>();
    private int selectedPosition = -1;
    private String keyword = "";


    public ChannelListAdapter(Context context, int layoutId, ArrayList<ChannelItem> items, ChannelListAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ChannelHolder holder, int position) {
        ChannelItem item = items.get(position);

        if (item == null)
            return;

        holder.title.setText(item.getChannelName());
        holder.site.setText(item.getChannelDomain());
        holder.root.setBackgroundResource(position == selectedPosition ? R.color.orange_a100_a : R.drawable.ripple_effect_main_transparent);
//        holder.title.setTextColor(ContextCompat.getColor(context, position == selectedPosition ? R.color.white_a100 : R.color.black_a100));
//        holder.site.setTextColor(ContextCompat.getColor(context, position == selectedPosition ? R.color.white_a100 : R.color.black_a100));

        if (item.getChannelType().equalsIgnoreCase("1")) {
            if (item.getChannelThumbnail() != null)
                holder.logo.setImageBitmap(ImageUtil.getImage(item.getChannelThumbnail()));
            else {
                if (Util.isNetworkAbailable(context)) {
                    Glide.with(context).load(item.getChannelThumbnailUrl()).asBitmap().placeholder(R.drawable.ic_vuuk_logo_color).into(new BitmapImageViewTarget(holder.logo));
//                    Bitmap bitmap = ImageUtil.getImageFromURL(item.getChannelThumbnailUrl());
//                    if (bitmap != null) {
//                        holder.logo.setImageBitmap(bitmap);
//                    } else
//                        holder.logo.setImageResource(R.drawable.ic_vuuk_logo_color);
                } else
                    holder.logo.setImageResource(R.drawable.ic_vuuk_logo_color);
            }
        } else
            holder.logo.setImageResource(R.drawable.ic_vuuk_logo_color);
    }

    @Override
    public ChannelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ChannelHolder holder = new ChannelHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.logo = holder.fv(R.id.image_log);
        holder.title = holder.fv(R.id.text_title);
        holder.site = holder.fv(R.id.text_site);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

//        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
//            @Override
//            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
//                if (listener != null && items.size() > position) {
//                    ((ChannelAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
//                }
//            }
//
//            @Override
//            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                return false;
//            }
//        });
        return holder;
    }

    public ChannelItem getSelectedChannelItem() {
        if (selectedPosition < 0)
            return null;

        return items.get(selectedPosition);
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
        this.notifyDataSetChanged();
    }

    public void setSelectedItem(String id) {
        selectedPosition = -1;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getChannelId().equalsIgnoreCase(id)) {
                this.selectedPosition = i;
                break;
            }
        }
        this.notifyDataSetChanged();
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;

        notifyDataSetChanged();
    }

    public static interface ChannelListAdapterListener extends ReOnItemClickListener<ChannelItem> {
//        public void onDeleteButtonClick(int position, ChannelItem item);
    }

    public class ChannelHolder extends ReAbstractViewHolder {
        //        OnItemViewClickListener deleteButtonClickListener;
        LinearLayout root;
        ImageView logo;
        TextView title, site;

        public ChannelHolder(View itemView) {
            super(itemView);
        }

//        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
//            deleteButtonClickListener = listener;
//            if (listener != null && delButton != null)
//                delButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        deleteButtonClickListener.onItemViewClick(getAdapterPosition(), ChannelHolder.this);
//                    }
//                });
//        }

    }
}

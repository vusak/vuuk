package io.jmobile.vuuk.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.data.BookItem;
import io.jmobile.vuuk.data.PlayListItem;

public class PlaylistAdapter extends ReSelectableAdapter<PlayListItem, PlaylistAdapter.PlaylistHolder> {
    private Context context;
    private ArrayList<PlayListItem> items = new ArrayList<>();
    private Map<String, String> channelMap = new HashMap<>();
    private int index = -1;
    private boolean editMode = false;

    public PlaylistAdapter(Context context, int layoutId, ArrayList<PlayListItem> items, PlaylistAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(PlaylistHolder holder, int position) {
        PlayListItem item = items.get(position);

        if (item == null)
            return;

        holder.title.setText(item.getBookTitle());
        holder.title.setTextColor(ContextCompat.getColor(context, item.isSelected() ? R.color.orange_a100 : R.color.black_a100));

        if (item.getBookType().equals(BookItem.TYPE_EPUB))
            holder.type.setImageResource(R.drawable.ic_open_book_orange_18);
        else if (item.getBookType().equals(BookItem.TYPE_PDF))
            holder.type.setImageResource(R.drawable.ic_open_pdf_navy_18);
        else if (item.getBookType().equals(BookItem.TYPE_VIDEO))
            holder.type.setImageResource(R.drawable.ic_open_video_blue_18);

        holder.deleteButton.setVisibility(isEditMode() ? View.VISIBLE : View.GONE);
        holder.number.setVisibility(isEditMode() ? View.GONE : View.VISIBLE);
        holder.number.setText(String.valueOf(position + 1));
        holder.order.setVisibility(isEditMode() ? View.VISIBLE : View.GONE);

        String ch = channelMap.get(item.getChannelId());
        if (!TextUtils.isEmpty(ch))
            holder.channel.setText(ch);
    }

    @Override
    public PlaylistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final PlaylistHolder holder = new PlaylistHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.type = holder.fv(R.id.image_type);
        holder.channel = holder.fv(R.id.text_channel);
        holder.title = holder.fv(R.id.text_title);
        holder.deleteButton = holder.fv(R.id.button_delete);
        holder.number = holder.fv(R.id.text_order);
        holder.order = holder.fv(R.id.image_order);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    ((PlaylistAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.order.setOnTouchListener((v, event) -> {
            if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                if (listener != null)
                    ((PlaylistAdapterListener) listener).onStartDrag(holder);
            }
            return false;
        });
        return holder;
    }

    public void setSelectedItem(String id) {
        index = -1;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getBookId().equalsIgnoreCase(id)) {
                index = i;
                break;
            }
        }

        notifyDataSetChanged();
    }

    public boolean isEditMode() {
        return this.editMode;
    }

    public void setEditMode(boolean mode) {
        this.editMode = mode;
        notifyDataSetChanged();
    }

    public void setChannelMap(Map<String, String> map) {
        this.channelMap = map;
        notifyDataSetChanged();
    }

    public static interface PlaylistAdapterListener extends ReOnItemClickListener<PlayListItem> {
        public void onDeleteButtonClick(int position, PlayListItem item);

        public void onStartDrag(ReAbstractViewHolder holder);
    }

    public class PlaylistHolder extends ReAbstractViewHolder {
        OnItemViewClickListener deleteButtonClickListener;

        LinearLayout root;
        ImageView type, order;
        TextView number, title, channel;
        ImageButton deleteButton;

        public PlaylistHolder(View itemView) {
            super(itemView);
        }

        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
            deleteButtonClickListener = listener;
            if (listener != null && deleteButton != null) {
                deleteButton.setOnClickListener(v -> deleteButtonClickListener.onItemViewClick(getAdapterPosition(), PlaylistHolder.this));
            }
        }
    }
}

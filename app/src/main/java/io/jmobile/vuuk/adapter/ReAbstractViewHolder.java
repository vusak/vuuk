package io.jmobile.vuuk.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class ReAbstractViewHolder extends RecyclerView.ViewHolder {
    public OnItemViewClickListener listener;
    private List<ImageView> recyclableImageViews = new ArrayList<ImageView>();

    public ReAbstractViewHolder(View itemView) {
        super(itemView);
    }

    public void setOnCellClickListener(OnItemViewClickListener listener) {
        this.listener = listener;
        if (listener != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReAbstractViewHolder.this.listener.onItemViewClick(getPosition(), ReAbstractViewHolder.this);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return ReAbstractViewHolder.this.listener.onItemViewLongClick(getPosition(), ReAbstractViewHolder.this);
                }
            });
        }
    }

    public <T extends View> T fv(int id) {
        try {
            return (T) itemView.findViewById(id);
        } catch (ClassCastException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addRecyclableImageView(ImageView iv) {
        recyclableImageViews.add(iv);
    }

    public List<ImageView> getRecyclableImageViews() {
        return recyclableImageViews;
    }

    public interface OnItemViewClickListener {
        public void onItemViewClick(int position, ReAbstractViewHolder holder);

        public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder);
    }
}

package io.jmobile.vuuk.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.vuuk.R;
import io.jmobile.vuuk.data.TimerItem;

public class TimerAdapter extends ReSelectableAdapter<TimerItem, TimerAdapter.TimerHolder> {

    private Context context;
    private ArrayList<TimerItem> items = new ArrayList<>();
    private int index = -1;


    public TimerAdapter(Context context, int layoutId, ArrayList<TimerItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(TimerHolder holder, int position) {
        TimerItem item = items.get(position);
        holder.title.setText(item.getTitle());
        holder.root.setBackgroundResource(index == position ? R.color.gray2_a100 : R.color.white);
        holder.check.setVisibility(index == position ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public TimerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final TimerHolder holder = new TimerHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.title = holder.fv(R.id.text_title);
        holder.check = holder.fv(R.id.image_check);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void setSelectedItem(long time) {
        if (time < 0)
            this.index = -1;
        else {
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).getTime() == time) {
                    index = i;
                    break;
                }
            }
        }
        notifyDataSetChanged();
    }


    public class TimerHolder extends ReAbstractViewHolder {
        LinearLayout root;
        ImageView check;
        TextView title;

        public TimerHolder(View itemView) {
            super(itemView);
        }

    }
}

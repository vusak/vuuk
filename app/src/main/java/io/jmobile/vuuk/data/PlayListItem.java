package io.jmobile.vuuk.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.vuuk.common.LogUtil;

public class PlayListItem extends TableObject {
    public static final String TABLE_NAME = "playlist";

    public static final String TYPE_EPUB = "EPUB";
    public static final String TYPE_PDF = "PDF";
    public static final String TYPE_VIDEO = "VIDEO";

    public static final String ROW_ID = "_id";
    public static final String PLAYLIST_ID = "playlist_id";
    public static final String PLAYLIST_BOOK_ID = "playlist_book_id";
    public static final String PLAYLIST_BOOK_TYPE = "playlist_book_type";
    public static final String PLAYLIST_BOOK_PATH = "playlist_book_path";
    public static final String PLAYLIST_BOOK_DOWNLOAD_URL = "playlist_book_download_url";
    public static final String PLAYLIST_BOOK_VIEW_URL = "playlist_book_view_url";
    public static final String PLAYLIST_BOOK_TITLE = "playlist_book_title";
    public static final String PLAYLIST_IDX = "playlist_idx";
    public static final String PLAYLIST_BOOK_CHANNEL_ID = "playlist_book_channel_id";
    public static final Creator<PlayListItem> CREATOR = new Creator<PlayListItem>() {
        @Override
        public PlayListItem createFromParcel(Parcel source) {
            return new PlayListItem(source);
        }

        @Override
        public PlayListItem[] newArray(int size) {
            return new PlayListItem[size];
        }
    };

    private long id;
    private String playlistId;
    private String bookId;
    private String type = TYPE_EPUB;
    private String bookPath;
    private String bookDownloadUrl;
    private String bookViewUrl;
    private String title;
    private int index;
    private String channelId = "demo";

    public PlayListItem() {
        super();
    }

    public PlayListItem(BookItem item) {
        this.bookId = item.getBookId();
        this.type = item.getBookType();
        this.bookPath = item.getBookPath();
        this.bookDownloadUrl = item.getBookDownloadUrl();
        this.bookViewUrl = item.getBookViewUrl();
        this.title = item.getBookTitle();
        this.channelId = item.getChannelId();
    }

    public PlayListItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.playlistId = in.readString();
        this.bookId = in.readString();
        this.type = in.readString();
        this.bookPath = in.readString();
        this.bookDownloadUrl = in.readString();
        this.bookViewUrl = in.readString();
        this.title = in.readString();
        this.index = in.readInt();
        this.channelId = in.readString();
    }

    public static void createTablePlaylist(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(PLAYLIST_ID + " text not null unique, ")
                .append(PLAYLIST_BOOK_ID + " text, ")
                .append(PLAYLIST_BOOK_TYPE + " text, ")
                .append(PLAYLIST_BOOK_PATH + " text, ")
                .append(PLAYLIST_BOOK_DOWNLOAD_URL + " text, ")
                .append(PLAYLIST_BOOK_VIEW_URL + " text, ")
                .append(PLAYLIST_BOOK_TITLE + " text, ")
                .append(PLAYLIST_IDX + " integer, ")
                .append(PLAYLIST_BOOK_CHANNEL_ID + " text) ")
                .toString());
    }

    public static PlayListItem getPlaylistItem(SQLiteDatabase db, String id) {
        List<PlayListItem> list = getPlaylistItems(db, null, PLAYLIST_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<PlayListItem> getPlaylistItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<PlayListItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                PlayListItem item = new PlayListItem()
                        .setRowId(c)
                        .setPlaylistId(c)
                        .setBookId(c)
                        .setBookType(c)
                        .setBookPath(c)
                        .setBookDownloadUrl(c)
                        .setBookViewUrl(c)
                        .setBookTitle(c)
                        .setIndex(c)
                        .setChannelId(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean updatePlaylistIndex(SQLiteDatabase db, PlayListItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putIndex(v)
                    .putBookPath(v);

            db.update(TABLE_NAME, v, PLAYLIST_ID + " = ?", new String[]{item.getPlaylistId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deletePlaylistItem(SQLiteDatabase db, PlayListItem item) {
        try {
            db.delete(TABLE_NAME, PLAYLIST_ID + " = ?", new String[]{item.getPlaylistId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deletePlaylistItemByBookId(SQLiteDatabase db, String bookId) {
        try {
            db.delete(TABLE_NAME, PLAYLIST_BOOK_ID + " = ?", new String[]{bookId});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean insertOrUpdatePlaylistItem(SQLiteDatabase db, PlayListItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putPlaylistId(v)
                    .putBookId(v)
                    .putBookType(v)
                    .putBookPath(v)
                    .putBookDownloadUrl(v)
                    .putBookViewUrl(v)
                    .putBookTitle(v)
                    .putIndex(v)
                    .putChannelId(v);

            int rowAffected = db.update(TABLE_NAME, v, PLAYLIST_ID + " =? ", new String[]{item.getPlaylistId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getBookTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAllPlaylistItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(playlistId);
        out.writeString(bookId);
        out.writeString(type);
        out.writeString(bookPath);
        out.writeString(bookDownloadUrl);
        out.writeString(bookViewUrl);
        out.writeString(title);
        out.writeInt(index);
        out.writeString(channelId);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PlayListItem && bookId == ((PlayListItem) obj).bookId;
    }

    public long getRowId() {
        return this.id;
    }

    public PlayListItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public PlayListItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public PlayListItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getPlaylistId() {
        return this.playlistId;
    }

    public PlayListItem setPlaylistId(String playlistId) {
        this.playlistId = playlistId;

        return this;
    }

    public PlayListItem setPlaylistId(Cursor c) {
        this.playlistId = s(c, PLAYLIST_ID);

        return this;
    }

    public PlayListItem putPlaylistId(ContentValues v) {
        v.put(PLAYLIST_ID, this.playlistId);

        return this;
    }

    public String getBookId() {
        return this.bookId;
    }

    public PlayListItem setBookId(String bookId) {
        this.bookId = bookId;

        return this;
    }

    public PlayListItem setBookId(Cursor c) {
        this.bookId = s(c, PLAYLIST_BOOK_ID);

        return this;
    }

    public PlayListItem putBookId(ContentValues v) {
        v.put(PLAYLIST_BOOK_ID, this.bookId);

        return this;
    }

    public String getBookType() {
        return this.type;
    }

    public PlayListItem setBookType(String type) {
        this.type = type;

        return this;
    }

    public PlayListItem setBookType(Cursor c) {
        this.type = s(c, PLAYLIST_BOOK_TYPE);

        return this;
    }

    public PlayListItem putBookType(ContentValues v) {
        v.put(PLAYLIST_BOOK_TYPE, this.type);

        return this;
    }

    public String getBookPath() {
        return this.bookPath;
    }

    public PlayListItem setBookPath(String path) {
        this.bookPath = path;

        return this;
    }

    public PlayListItem setBookPath(Cursor c) {
        this.bookPath = s(c, PLAYLIST_BOOK_PATH);

        return this;
    }

    public PlayListItem putBookPath(ContentValues v) {
        v.put(PLAYLIST_BOOK_PATH, this.bookPath);

        return this;
    }

    public String getBookDownloadUrl() {
        return this.bookDownloadUrl;
    }

    public PlayListItem setBookDownloadUrl(Cursor c) {
        this.bookDownloadUrl = s(c, PLAYLIST_BOOK_DOWNLOAD_URL);

        return this;
    }

    public PlayListItem setBookDownloadUrl(String downloadUrl) {
        this.bookDownloadUrl = downloadUrl;

        return this;
    }

    public PlayListItem putBookDownloadUrl(ContentValues v) {
        v.put(PLAYLIST_BOOK_DOWNLOAD_URL, this.bookDownloadUrl);

        return this;
    }

    public String getBookViewUrl() {
        return this.bookViewUrl;
    }

    public PlayListItem setBookViewUrl(Cursor c) {
        this.bookViewUrl = s(c, PLAYLIST_BOOK_VIEW_URL);

        return this;
    }

    public PlayListItem setBookViewUrl(String viewUrl) {
        this.bookViewUrl = viewUrl;

        return this;
    }

    public PlayListItem putBookViewUrl(ContentValues v) {
        v.put(PLAYLIST_BOOK_VIEW_URL, this.bookViewUrl);

        return this;
    }


    public String getBookTitle() {
        return this.title;
    }

    public PlayListItem setBookTitle(String title) {
        this.title = title;

        return this;
    }

    public PlayListItem setBookTitle(Cursor c) {
        this.title = s(c, PLAYLIST_BOOK_TITLE);

        return this;
    }

    public PlayListItem putBookTitle(ContentValues v) {
        v.put(PLAYLIST_BOOK_TITLE, this.title);

        return this;
    }

    public int getIndex() {
        return this.index;
    }

    public PlayListItem setIndex(int index) {
        this.index = index;

        return this;

    }

    public PlayListItem setIndex(Cursor c) {
        this.index = i(c, PLAYLIST_IDX);

        return this;
    }

    public PlayListItem putIndex(ContentValues v) {
        v.put(PLAYLIST_IDX, this.index);

        return this;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public PlayListItem setChannelId(String id) {
        this.channelId = id;

        return this;
    }

    public PlayListItem setChannelId(Cursor c) {
        this.channelId = s(c, PLAYLIST_BOOK_CHANNEL_ID);

        return this;
    }

    public PlayListItem putChannelId(ContentValues v) {
        v.put(PLAYLIST_BOOK_CHANNEL_ID, this.channelId);

        return this;
    }

}

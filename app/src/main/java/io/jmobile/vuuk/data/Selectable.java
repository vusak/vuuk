package io.jmobile.vuuk.data;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreatro")
public class Selectable implements Parcelable {
    public final Creator<Selectable> CREATOR = new Creator<Selectable>() {
        @Override
        public Selectable createFromParcel(Parcel source) {
            return new Selectable(source);
        }

        @Override
        public Selectable[] newArray(int size) {
            return new Selectable[size];
        }
    };
    protected boolean selected = false;
    protected boolean enabled = true;

    public Selectable() {

    }

    public Selectable(Parcel in) {
        setSelected(in.readInt() != 0);
        setEnabled(in.readInt() != 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(selected ? 1 : 0);
        out.writeInt(enabled ? 1 : 0);
    }

    public boolean isSelected() {
        return selected;
    }

    public Selectable setSelected(boolean selected) {
        this.selected = selected;

        return this;
    }

    public Selectable toggle() {
        selected = !selected;

        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Selectable setEnabled(boolean enabled) {
        this.enabled = enabled;

        return this;
    }
}

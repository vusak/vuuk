package io.jmobile.vuuk.data;

import android.os.Parcel;

import org.json.JSONObject;

import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.Util;

public class CommentItem extends TableObject {
    public static final String TABLE_NAME = "comment";

    public static final String ROW_ID = "_id";
    public static final String COMMENT_ID = "comment_id";
    public static final String COMMENT_VALUE = "comment_value";
    public static final String COMMENT_USER_NAME = "comment_user_name";
    public static final String COMMENT_USER_ID = "comment_user_id";
    public static final String COMMENT_CREATE_DATE = "comment_create_date";
    public static final String COMMENT_USER_PROFILE_URL = "comment_user_profile_url";
    public static final String COMMENT_USER_PROFILE = "comment_user_profile";

    public static final Creator<CommentItem> CREATOR = new Creator<CommentItem>() {
        @Override
        public CommentItem createFromParcel(Parcel source) {
            return new CommentItem(source);
        }

        @Override
        public CommentItem[] newArray(int size) {
            return new CommentItem[size];
        }
    };

    private long id;
    private String commentId;
    private String comment;
    private String userName;
    private String userId;
    private String createAt;
    private String userProfileUrl;
    private byte[] userProfile;
    private String likeCount;
    private String dislikeCount;

    public CommentItem() {
        super();
    }

    public CommentItem(JSONObject o) {
        commentId = Util.optString(o, Common.TAG_COMMENT_ID);
        comment = Util.optString(o, Common.TAG_COMMENT_VALUE);
        userName = Util.optString(o, Common.TAG_COMMENT_USER_NAME);
        userId = Util.optString(o, Common.TAG_COMMENT_USER_ID);
        createAt = Util.optString(o, Common.TAG_COMMENT_DATE);
        userProfileUrl = Util.optString(o, Common.TAG_COMMENT_USER_PROFILE_URL);
        likeCount = Util.optString(o, Common.TAG_COMMENT_LIKE_COUNT);
        dislikeCount = Util.optString(o, Common.TAG_COMMENT_DISLIKE_COUNT);
    }


    public CommentItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.commentId = in.readString();
        this.comment = in.readString();
        this.userName = in.readString();
        this.userId = in.readString();
        this.createAt = in.readString();
        this.likeCount = in.readString();
        this.dislikeCount = in.readString();
        this.userProfileUrl = in.readString();
        in.readByteArray(this.userProfile);
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(commentId);
        out.writeString(comment);
        out.writeString(userName);
        out.writeString(userId);
        out.writeString(likeCount);
        out.writeString(dislikeCount);
        out.writeString(userProfileUrl);
        out.writeByteArray(userProfile);
        out.writeString(createAt);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CommentItem && commentId == ((CommentItem) obj).commentId;
    }

    public long getRowId() {
        return this.id;
    }

    public CommentItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public String getCommentId() {
        return this.commentId;
    }

    public CommentItem setCommentId(String id) {
        this.commentId = id;

        return this;
    }

    public String getCommentValue() {
        return this.comment;
    }

    public CommentItem setCommentValue(String comment) {
        this.comment = comment;

        return this;
    }

    public String getCommentUserId() {
        return this.userId;
    }

    public CommentItem setCommentUserId(String userId) {
        this.userId = userId;

        return this;
    }

    public String getCommentUserName() {
        return this.userName;
    }

    public CommentItem setCommentUserName(String userName) {
        this.userName = userName;

        return this;
    }

    public String getCommentListCount() {
        return this.likeCount;
    }

    public CommentItem setCommentLikeCount(String count) {
        this.likeCount = count;

        return this;
    }

    public String getCommentDislikeCount() {
        return this.dislikeCount;
    }

    public CommentItem setCommentDislikeCount(String count) {
        this.dislikeCount = count;

        return this;
    }

    public String getCommentUserProfileUrl() {
        return this.userProfileUrl;
    }

    public CommentItem setCommentUserProfileUrl(String url) {
        this.userProfileUrl = url;

        return this;
    }

    public byte[] getCommentUserProfile() {
        return this.userProfile;
    }

    public CommentItem setCommentUserProfile(byte[] profile) {
        this.userProfile = profile;

        return this;
    }

    public String getCommentCreateAt() {
        return this.createAt;
    }

    public CommentItem setCommentCreateAt(String createAt) {
        this.createAt = createAt;

        return this;
    }
}

package io.jmobile.vuuk.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import io.jmobile.vuuk.base.BaseComparator;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.Util;

public class CategoryItem extends TableObject {
    public static final String TABLE_NAME = "category";

    public static final String ROW_ID = "_id";
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_THUMBNAIL_URL = "category_thumbnail_url";
    public static final String CATEGORY_THUMBNAIL = "category_thumbnail";
    public static final String CATEGORY_POSITION = "category_position";
    public static final String CATEGORY_CHANNEL_ID = "category_channel_id";

    public static final Creator<CategoryItem> CREATOR = new Creator<CategoryItem>() {
        @Override
        public CategoryItem createFromParcel(Parcel source) {
            return new CategoryItem(source);
        }

        @Override
        public CategoryItem[] newArray(int size) {
            return new CategoryItem[size];
        }
    };

    public static CategoryComparator CATEGORY_DEPTH_ASC = new CategoryDepthComparator(true);
    public static CategoryComparator CATEGORY_DEPTH_DESC = new CategoryDepthComparator(false);

    private long id;
    private int position;
    private String categoryId;
    private String name;
    private String thumbnailUrl;
    private byte[] thumbnail;
    private String channelId;

    public CategoryItem() {
        super();
    }

    public CategoryItem(JSONObject o) {
        categoryId = Util.optString(o, Common.TAG_CATEGORY_CODE);
        if (!TextUtils.isEmpty(categoryId)) {
            String[] depth = categoryId.split("\\.");
            position = depth.length - 1;
            if (position == 0)
                thumbnailUrl = categoryId;
            else if (position == 1)
                thumbnailUrl = String.format("%s.%03d", depth[0], Integer.parseInt(depth[1]));
            else if (position == 2)
                thumbnailUrl = String.format("%s.%03d.%03d", depth[0], Integer.parseInt(depth[1]), Integer.parseInt(depth[2]));
        }

        name = Util.optString(o, Common.TAG_CATEGORY_NAME);
    }

//    public CategoryItem(JSONObject o) {
//        this.id = Util.optString(o, Common.TAG_ID);
//        this.etag = Util.optString(o, Common.TAG_ETAG);
//        this.kind = Util.optString(o, Common.TAG_KIND);
//        try {
//            JSONObject ob = new JSONObject(Util.optString(o, Common.TAG_SNIPPET));
//            this.channelId = Util.optString(ob, Common.TAG_CHANNEL_ID);
//            this.title = Util.optString(ob, Common.TAG_TITLE);
//        } catch (Exception e) {
//            LogUtil.log(e.getMessage());
//        }
//    }

    public CategoryItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.categoryId = in.readString();
        this.position = in.readInt();
        this.name = in.readString();
        this.thumbnailUrl = in.readString();
        in.readByteArray(this.thumbnail);
        this.channelId = in.readString();
    }

    public static void createTableCategory(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(CATEGORY_ID + " text not null unique, ")
                .append(CATEGORY_POSITION + " integer, ")
                .append(CATEGORY_NAME + " text, ")
                .append(CATEGORY_CHANNEL_ID + " text, ")
                .append(CATEGORY_THUMBNAIL_URL + " text, ")
                .append(CATEGORY_THUMBNAIL + " BLOB) ")
                .toString());
    }

    public static CategoryItem getCategoryItem(SQLiteDatabase db, String id) {
        List<CategoryItem> list = getCategoryItems(db, null, CATEGORY_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<CategoryItem> getCategoryItemsByChannel(SQLiteDatabase db, String channelId) {
        List<CategoryItem> list = getCategoryItems(db, null, CATEGORY_CHANNEL_ID + " = ? ", new String[]{channelId}, null, null, null, null);

        return list;
    }

    public static List<CategoryItem> getCategoryItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<CategoryItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                CategoryItem item = new CategoryItem()
                        .setRowId(c)
                        .setCategoryId(c)
                        .setCategoryPosition(c)
                        .setCategoryName(c)
                        .setCategoryChannelId(c)
                        .setCategoryThumbnailUrl(c)
                        .setCategoryThumbnail(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateCategoryItem(SQLiteDatabase db, CategoryItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putCategoryId(v)
                    .putCategoryPosition(v)
                    .putCategoryName(v)
                    .putCategoryChannelId(v)
                    .putCategoryThumbnailUrl(v)
                    .putCategoryThumbnail(v);

            int rowAffected = db.update(TABLE_NAME, v, CATEGORY_ID + " =? ", new String[]{item.getCategoryId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getCategoryName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteCategoryItem(SQLiteDatabase db, CategoryItem item) {
        try {
            db.delete(TABLE_NAME, CATEGORY_ID + " = ? ", new String[]{item.getCategoryId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAppCategoryItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeInt(position);
        out.writeString(categoryId);
        out.writeString(name);
        out.writeString(thumbnailUrl);
        out.writeByteArray(thumbnail);
        out.writeString(channelId);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CategoryItem && categoryId == ((CategoryItem) obj).categoryId;
    }

    public long getRowId() {
        return this.id;
    }

    public CategoryItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public CategoryItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public CategoryItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getCategoryId() {
        return this.categoryId;
    }

    public CategoryItem setCategoryId(Cursor c) {
        this.categoryId = s(c, CATEGORY_ID);

        return this;
    }

    public CategoryItem setCategoryId(String id) {
        this.categoryId = id;

        return this;
    }

    public CategoryItem putCategoryId(ContentValues v) {
        v.put(CATEGORY_ID, this.categoryId);

        return this;
    }

    public int getCategoryPosition() {
        return this.position;
    }

    public CategoryItem setCategoryPosition(Cursor c) {
        this.position = i(c, CATEGORY_POSITION);

        return this;
    }

    public CategoryItem setCategoryPosition(int position) {
        this.position = position;

        return this;
    }

    public CategoryItem putCategoryPosition(ContentValues v) {
        v.put(CATEGORY_POSITION, this.position);

        return this;
    }

    public String getCategoryName() {
        return this.name;
    }

    public CategoryItem setCategoryName(Cursor c) {
        this.name = s(c, CATEGORY_NAME);

        return this;
    }

    public CategoryItem setCategoryName(String name) {
        this.name = name;

        return this;
    }

    public CategoryItem putCategoryName(ContentValues v) {
        v.put(CATEGORY_NAME, this.name);

        return this;
    }

    public String getCategoryThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public CategoryItem setCategoryThumbnailUrl(Cursor c) {
        this.thumbnailUrl = s(c, CATEGORY_THUMBNAIL_URL);

        return this;
    }

    public CategoryItem setCategoryThumbnailUrl(String url) {
        this.thumbnailUrl = url;

        return this;
    }

    public CategoryItem putCategoryThumbnailUrl(ContentValues v) {
        v.put(CATEGORY_THUMBNAIL_URL, this.thumbnailUrl);

        return this;
    }

    public byte[] getCategoryThumbnail() {
        return this.thumbnail;
    }

    public CategoryItem setCategoryThumbnail(Cursor c) {
        this.thumbnail = blob(c, CATEGORY_THUMBNAIL);

        return this;
    }

    public CategoryItem setCategoryThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public CategoryItem putCategoryThumbnail(ContentValues v) {
        v.put(CATEGORY_THUMBNAIL, this.thumbnail);

        return this;
    }

    public String getCategoryChannelId() {
        return this.channelId;
    }

    public CategoryItem setCategoryChannelId(Cursor c) {
        this.channelId = s(c, CATEGORY_CHANNEL_ID);

        return this;
    }

    public CategoryItem setCategoryChannelId(String channelId) {
        this.channelId = channelId;

        return this;
    }

    public CategoryItem putCategoryChannelId(ContentValues v) {
        v.put(CATEGORY_CHANNEL_ID, channelId);

        return this;
    }

    private static class CategoryComparator extends BaseComparator implements Comparator<CategoryItem> {
        public CategoryComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(CategoryItem lhs, CategoryItem rhs) {
            if (lhs == null && rhs != null)
                return 1;
            else if (rhs == null && lhs != null)
                return -1;
            else
                return 0;
        }
    }

    private static class CategoryDepthComparator extends CategoryComparator {
        public CategoryDepthComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(CategoryItem lhs, CategoryItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;
            else
                return compareString(lhs.getCategoryThumbnailUrl(), rhs.getCategoryThumbnailUrl(), false);

        }
    }
}

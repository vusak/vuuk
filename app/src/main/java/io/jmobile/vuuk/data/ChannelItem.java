package io.jmobile.vuuk.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.Util;

public class ChannelItem extends TableObject {
    public static final String TABLE_NAME = "channel";

    public static final String ROW_ID = "_id";
    public static final String CHANNEL_ID = "channel_id";
    public static final String CHANNEL_NAME = "channel_name";
    public static final String CHANNEL_DOMAIN = "channel_domain";
    public static final String CHANNEL_TYPE = "channel_type";
    public static final String CHANNEL_THUMBNAIL_URL = "channel_thumbnail_url";
    public static final String CHANNEL_THUMBNAIL = "channel_thumbnail";
    public static final String CHANNEL_POSITION = "channel_position";
    public static final String CHANNEL_JOINED = "channel_joined";
    public static final String CHANNEL_AT = "channel_at";
    public static final String CHANNEL_USER_NICKNAME = "channel_user_nickname";
    public static final String CHANNEL_USER_PROFILE_URL = "channel_user_profile_url";
    public static final String CHANNEL_USER_PROFILE = "channel_user_profile";
    public static final String CHANNEL_USER_AT = "channel_user_at";
    public static final String CHANNEL_USE_CATEGORY = "channel_use_category";

    public static final Creator<ChannelItem> CREATOR = new Creator<ChannelItem>() {
        @Override
        public ChannelItem createFromParcel(Parcel source) {
            return new ChannelItem(source);
        }

        @Override
        public ChannelItem[] newArray(int size) {
            return new ChannelItem[size];
        }
    };

    private long id;
    private int position;
    private String channelId;
    private String name;
    private String domain;
    private String type;
    private String thumbnailUrl;
    private byte[] thumbnail;
    private long channelAt;
    private boolean joined = false;
    private String userNickname;
    private String userProfileUrl;
    private byte[] userProfile;
    private long channelUserAt;
    private boolean useCategory = false;

    public ChannelItem() {
        super();
    }

    public ChannelItem(JSONObject o) {
        this.channelId = Util.optString(o, Common.TAG_CHANNEL_ID);
        this.name = Util.optString(o, Common.TAG_CHANNEL_NAME);
        this.domain = Util.optString(o, Common.TAG_DOMAIN);
        this.type = Util.optString(o, Common.TAG_LOGO_TYPE);
        this.thumbnailUrl = Util.optString(o, Common.TAG_LOGO_URL);
        String use = Util.optString(o, Common.TAG_USE_CATEGORY);
        if (!TextUtils.isEmpty(use) && use.equalsIgnoreCase("Y"))
            this.useCategory = true;

    }

    public ChannelItem(String id, String name) {
        this.channelId = id;
        this.name = name;
    }

    public ChannelItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.channelId = in.readString();
        this.position = in.readInt();
        this.name = in.readString();
        this.domain = in.readString();
        this.type = in.readString();
        this.thumbnailUrl = in.readString();
        in.readByteArray(this.thumbnail);
        this.channelAt = in.readLong();
        this.joined = in.readInt() == 1;
        this.userNickname = in.readString();
        this.userProfileUrl = in.readString();
        in.readByteArray(this.userProfile);
        this.channelUserAt = in.readLong();
        this.useCategory = in.readInt() == 1;
    }

    public static void createTableChannel(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(CHANNEL_ID + " text not null unique, ")
                .append(CHANNEL_POSITION + " integer, ")
                .append(CHANNEL_AT + " integer, ")
                .append(CHANNEL_USER_AT + " integer, ")
                .append(CHANNEL_JOINED + " integer, ")
                .append(CHANNEL_NAME + " text, ")
                .append(CHANNEL_USER_NICKNAME + " text, ")
                .append(CHANNEL_DOMAIN + " text, ")
                .append(CHANNEL_TYPE + " text, ")
                .append(CHANNEL_THUMBNAIL_URL + " text, ")
                .append(CHANNEL_USER_PROFILE_URL + " text, ")
                .append(CHANNEL_USE_CATEGORY + " integer, ")
                .append(CHANNEL_USER_PROFILE + " BLOB, ")
                .append(CHANNEL_THUMBNAIL + " BLOB) ")
                .toString());
    }

    public static ChannelItem getChannelItem(SQLiteDatabase db, String id) {
        List<ChannelItem> list = getChannelItems(db, null, CHANNEL_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<ChannelItem> getJoinedChannelItems(SQLiteDatabase db) {
        List<ChannelItem> list = new ArrayList<>();
        for (ChannelItem item : getChannelItems(db, null, null, null, null, null, CHANNEL_AT + " ASC", null))
            if (item.isChannelJoined())
                list.add(item);

        return list;
    }

    public static List<ChannelItem> getChannelItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<ChannelItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                ChannelItem item = new ChannelItem()
                        .setRowId(c)
                        .setChannelId(c)
                        .setChannelPosition(c)
                        .setChannelName(c)
                        .setChannelDomain(c)
                        .setChannelType(c)
                        .setChannelThumbnailUrl(c)
                        .setChannelThumbnail(c)
                        .setChannelJoined(c)
                        .setChannelAt(c)
                        .setChannelUserNickname(c)
                        .setChannelUseCategory(c)
                        .setChannelUserProfileUrl(c)
                        .setChannelUserProfile(c)
                        .setChannelUserAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateChannelItem(SQLiteDatabase db, ChannelItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putChannelId(v)
                    .putChannelPosition(v)
                    .putChannelName(v)
                    .putChannelDomain(v)
                    .putChannelType(v)
                    .putChannelThumbnailUrl(v)
                    .putChannelThumbnail(v)
                    .putChannelJoined(v)
                    .putChannelAt(v)
                    .putChannelUserNickname(v)
                    .putChannelUseCategory(v)
                    .putChannelUserProfileUrl(v)
                    .putChannelUserProfile(v)
                    .putChannelUserAt(v);

            int rowAffected = db.update(TABLE_NAME, v, CHANNEL_ID + " =? ", new String[]{item.getChannelId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getChannelName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean updateChannelJoined(SQLiteDatabase db, ChannelItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putChannelAt(v)
                    .putChannelJoined(v);

            db.update(TABLE_NAME, v, CHANNEL_ID + " = ?", new String[]{item.getChannelId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteChannelItem(SQLiteDatabase db, ChannelItem item) {
        try {
            db.delete(TABLE_NAME, CHANNEL_ID + " = ? ", new String[]{item.getChannelId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllChannelItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeInt(position);
        out.writeString(channelId);
        out.writeString(name);
        out.writeString(domain);
        out.writeString(type);
        out.writeString(thumbnailUrl);
        out.writeByteArray(thumbnail);
        out.writeLong(channelAt);
        out.writeInt(joined ? 1 : 0);
        out.writeString(userNickname);
        out.writeString(userProfileUrl);
        out.writeByteArray(userProfile);
        out.writeLong(channelUserAt);
        out.writeInt(useCategory ? 1 : 0);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ChannelItem && channelId == ((ChannelItem) obj).channelId;
    }

    public long getRowId() {
        return this.id;
    }

    public ChannelItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public ChannelItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public ChannelItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public ChannelItem setChannelId(Cursor c) {
        this.channelId = s(c, CHANNEL_ID);

        return this;
    }

    public ChannelItem setChannelId(String id) {
        this.channelId = id;

        return this;
    }

    public ChannelItem putChannelId(ContentValues v) {
        v.put(CHANNEL_ID, this.channelId);

        return this;
    }

    public int getChannelPosition() {
        return this.position;
    }

    public ChannelItem setChannelPosition(Cursor c) {
        this.position = i(c, CHANNEL_POSITION);

        return this;
    }

    public ChannelItem setChannelPosition(int position) {
        this.position = position;

        return this;
    }

    public ChannelItem putChannelPosition(ContentValues v) {
        v.put(CHANNEL_POSITION, this.position);

        return this;
    }

    public String getChannelName() {
        return this.name;
    }

    public ChannelItem setChannelName(Cursor c) {
        this.name = s(c, CHANNEL_NAME);

        return this;
    }

    public ChannelItem setChannelName(String name) {
        this.name = name;

        return this;
    }

    public ChannelItem putChannelName(ContentValues v) {
        v.put(CHANNEL_NAME, this.name);

        return this;
    }

    public String getChannelDomain() {
        return this.domain;
    }

    public ChannelItem setChannelDomain(Cursor c) {
        this.domain = s(c, CHANNEL_DOMAIN);

        return this;
    }

    public ChannelItem setChannelDomain(String domain) {
        this.domain = domain;

        return this;
    }

    public ChannelItem putChannelDomain(ContentValues v) {
        v.put(CHANNEL_DOMAIN, domain);

        return this;
    }

    public String getChannelType() {
        return this.type;
    }

    public ChannelItem setChannelType(Cursor c) {
        this.type = s(c, CHANNEL_TYPE);

        return this;
    }

    public ChannelItem setChannelType(String type) {
        this.type = type;

        return this;
    }

    public ChannelItem putChannelType(ContentValues v) {
        v.put(CHANNEL_TYPE, type);

        return this;
    }

    public String getChannelThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public ChannelItem setChannelThumbnailUrl(Cursor c) {
        this.thumbnailUrl = s(c, CHANNEL_THUMBNAIL_URL);

        return this;
    }

    public ChannelItem setChannelThumbnailUrl(String url) {
        this.thumbnailUrl = url;

        return this;
    }

    public ChannelItem putChannelThumbnailUrl(ContentValues v) {
        v.put(CHANNEL_THUMBNAIL_URL, this.thumbnailUrl);

        return this;
    }

    public byte[] getChannelThumbnail() {
        return this.thumbnail;
    }

    public ChannelItem setChannelThumbnail(Cursor c) {
        this.thumbnail = blob(c, CHANNEL_THUMBNAIL);

        return this;
    }

    public ChannelItem setChannelThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public ChannelItem putChannelThumbnail(ContentValues v) {
        v.put(CHANNEL_THUMBNAIL, this.thumbnail);

        return this;
    }

    public long getChannelAt() {
        return this.channelAt;
    }

    public ChannelItem setChannelAt(Cursor c) {
        this.channelAt = l(c, CHANNEL_AT);

        return this;
    }

    public ChannelItem setChannelAt(long at) {
        this.channelAt = at;

        return this;
    }

    public ChannelItem putChannelAt(ContentValues v) {
        v.put(CHANNEL_AT, channelAt);

        return this;
    }

    public boolean isChannelJoined() {
        return this.joined;
    }

    public ChannelItem setChannelJoined(Cursor c) {
        this.joined = i(c, CHANNEL_JOINED) == 1;

        return this;
    }

    public ChannelItem setChannelJoined(boolean join) {
        this.joined = join;

        return this;
    }

    public ChannelItem putChannelJoined(ContentValues v) {
        v.put(CHANNEL_JOINED, joined ? 1 : 0);

        return this;
    }

    public String getChannelUserNickname() {
        return this.userNickname;
    }

    public ChannelItem setChannelUserNickname(Cursor c) {
        this.userNickname = s(c, CHANNEL_USER_NICKNAME);

        return this;
    }

    public ChannelItem setChannelUserNickname(String nickname) {
        this.userNickname = nickname;

        return this;
    }

    public ChannelItem putChannelUserNickname(ContentValues v) {
        v.put(CHANNEL_USER_NICKNAME, this.userNickname);

        return this;
    }

    public String getChannelUserProfileUrl() {
        return this.userProfileUrl;
    }

    public ChannelItem setChannelUserProfileUrl(Cursor c) {
        this.userProfileUrl = s(c, CHANNEL_USER_PROFILE_URL);

        return this;
    }

    public ChannelItem setChannelUserProfileUrl(String url) {
        this.userProfileUrl = url;

        return this;
    }

    public ChannelItem putChannelUserProfileUrl(ContentValues v) {
        v.put(CHANNEL_USER_PROFILE_URL, this.userProfileUrl);

        return this;
    }

    public byte[] getChannelUserProfile() {
        return this.userProfile;
    }

    public ChannelItem setChannelUserProfile(Cursor c) {
        this.userProfile = blob(c, CHANNEL_USER_PROFILE);

        return this;
    }

    public ChannelItem setChannelUserProfile(byte[] thumbnail) {
        this.userProfile = thumbnail;

        return this;
    }

    public ChannelItem putChannelUserProfile(ContentValues v) {
        v.put(CHANNEL_USER_PROFILE, this.userProfile);

        return this;
    }

    public long getChannelUserAt() {
        return this.channelUserAt;
    }

    public ChannelItem setChannelUserAt(Cursor c) {
        this.channelUserAt = l(c, CHANNEL_USER_AT);

        return this;
    }

    public ChannelItem setChannelUserAt(long at) {
        this.channelUserAt = at;

        return this;
    }

    public ChannelItem putChannelUserAt(ContentValues v) {
        v.put(CHANNEL_USER_AT, channelUserAt);

        return this;
    }

    public boolean useChannelCategory() {
        return useCategory;
    }

    public ChannelItem setChannelUseCategory(boolean use) {
        this.useCategory = use;

        return this;
    }

    public ChannelItem setChannelUseCategory(Cursor c) {
        this.useCategory = i(c, CHANNEL_USE_CATEGORY) == 1;

        return this;
    }

    public ChannelItem putChannelUseCategory(ContentValues v) {
        v.put(CHANNEL_USE_CATEGORY, useCategory ? 1 : 0);

        return this;
    }
}

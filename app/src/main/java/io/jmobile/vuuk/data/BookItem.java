package io.jmobile.vuuk.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import io.jmobile.vuuk.base.BaseComparator;
import io.jmobile.vuuk.common.Common;
import io.jmobile.vuuk.common.FileUtil;
import io.jmobile.vuuk.common.LogUtil;
import io.jmobile.vuuk.common.Util;

public class BookItem extends TableObject {
    public static final String TABLE_NAME = "book";

    public static final String TYPE_ALL = "ALL";
    public static final String TYPE_EPUB = "EPUB";
    public static final String TYPE_PDF = "PDF";
    public static final String TYPE_VIDEO = "VIDEO";

    public static final String SHARE_ALL = "A";
    public static final String SHARE_LINK = "B";
    public static final String SHARE_FRANDS = "C";
    public static final String SHARE_NONE = "D";

    public static final String BOOK_ORDER_NEW = "N";
    public static final String BOOK_ORDER_HIT = "H";
    public static final String BOOK_ORDER_BEST = "B";

    public static final int DOWNLOAD_STATE_NONE = 0;
    public static final int DOWNLOAD_STATE_READY = 1;
    public static final int DOWNLOAD_STATE_DOWNLOADING = 2;
    public static final int DOWNLOAD_STATE_PAUSE = 3;
    public static final int DOWNLOAD_STATE_DONE = 4;

    public static final String ROW_ID = "_id";
    public static final String BOOK_ID = "book_id";
    public static final String BOOK_TYPE = "book_type";
    public static final String BOOK_VALUE = "book_value";
    public static final String BOOK_PATH = "book_path";
    public static final String BOOK_DOWNLOAD_URL = "book_download_url";
    public static final String BOOK_VIEW_URL = "book_view_url";
    public static final String BOOK_FILE_SIZE = "book_file_size";
    public static final String BOOK_SHORT_URL = "book_short_url";
    public static final String BOOK_THUMBNAIL_URL = "book_thumbnail_url";
    public static final String BOOK_THUMBNAIL = "book_thumbnail";
    public static final String BOOK_THUMBNAIL_PATH = "book_thumbnail_path";
    public static final String BOOK_TITLE = "book_title";
    public static final String BOOK_DETAIL = "book_detail";
    public static final String BOOK_PUBLISHER = "book_publisher";
    public static final String BOOK_PUBLICATION_DATE = "book_publication_date";
    public static final String BOOK_PUBLISHED_BY = "book_published_by";
    public static final String BOOK_LANGUAGE = "book_language";
    public static final String BOOK_OVERVIEW = "book_over_view";
    public static final String BOOK_VIEW_COUNT = "book_view_count";
    public static final String BOOK_LIKE_COUNT = "book_like_count";
    public static final String BOOK_COMMENT_COUNT = "book_comment_count";
    public static final String BOOK_LIKE = "book_like";
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String BOOK_CREATE_USER_KEY = "book_create_user_key";
    public static final String BOOK_CREATE_USER_NAME = "book_create_user_name";
    public static final String BOOK_CREATE_NICK_NAME = "book_create_nick_name";
    public static final String BOOK_CREATE_USER_COMMENT = "book_create_user_comment";
    public static final String BOOK_CREATE_USER_PROFILE_URL = "book_create_user_profile_url";
    public static final String BOOK_CREATE_USER = "book_create_user";
    public static final String BOOK_CREATE_DATE = "book_create_date";
    public static final String BOOK_MODIFY_DATE = "book_modify_date";
    public static final String BOOK_SHARE_STATE = "booK_share_state";
    public static final String BOOK_DOWNLOAD_STATUS = "book_download_status";
    public static final String BOOK_DOWNLOAD_PROGRESS = "book_download_progress";
    public static final String BOOK_DOWNLOAD_AT = "book_download_at";
    public static final String BOOK_DOWNLOAD_STARTED_AT = "book_download_started_at";
    public static final String BOOK_TAG = "book_tag";
    public static final String CHANNEL_ID = "channel_id";
    public static final Creator<BookItem> CREATOR = new Creator<BookItem>() {
        @Override
        public BookItem createFromParcel(Parcel source) {
            return new BookItem(source);
        }

        @Override
        public BookItem[] newArray(int size) {
            return new BookItem[size];
        }
    };
    public static BookComparator COMPARATOR_NEW_ASC = new NewBookComparator(true);
    public static BookComparator COMPARATOR_NEW_DESC = new NewBookComparator(false);
    public static BookComparator COMPARATOR_HIT_ASC = new HitBookComparator(true);
    public static BookComparator COMPARATOR_HIT_DESC = new HitBookComparator(false);
    public static BookComparator COMPARATOR_BEST_ASC = new BestBookComparator(true);
    public static BookComparator COMPARATOR_BEST_DESC = new BestBookComparator(false);
    private long id;
    private String bookId;
    private String type = TYPE_EPUB;
    private String value;
    private String bookPath;
    private String bookDownloadUrl;
    private String bookViewUrl;
    private long bookFileSize;
    private String shortUrl;
    private String thumbnailUrl;
    private byte[] thumbnail;
    private String thumbnailPath;
    private String title;
    private String detail;
    private String publisher;
    private String publicationDate;
    private String publishedBy;
    private String overview;
    private String categoryId;
    private String categoryName;
    private String language;
    private String likeCount;
    private String viewCount;
    private String commentCount;
    private boolean like;
    private String userKey;
    private String userName;
    private String userNickname;
    private String userComment;
    private String userProfileUrl;
    private String user;
    private String createDate;
    private String modifyDate;
    private String shareState = SHARE_ALL;
    private int downlaodStatus = DOWNLOAD_STATE_NONE;
    private float downloadProgress = 0f;
    private long downloadAt;
    private long downloadStartedAt;
    private String channelId = "vuuk";
    private String tag;

    public BookItem() {
        super();
    }

    public BookItem(JSONObject ob) {
        this.bookId = Util.optString(ob, Common.TAG_BOOK_OBJ_ID);
        this.type = Util.optString(ob, Common.TAG_BOOK_TAGS);
        this.detail = Util.optString(ob, Common.TAG_BOOK_SUMMARY);
        this.viewCount = Util.optString(ob, Common.TAG_BOOK_VIEW_COUNT);
        this.likeCount = Util.optString(ob, Common.TAG_BOOK_LIKE_COUNT);
        this.commentCount = Util.optString(ob, Common.TAG_BOOK_COMMENT_COUNT);
        this.title = Util.optString(ob, Common.TAG_BOOK_TITLE);
        this.publishedBy = Util.optString(ob, Common.TAG_BOOK_AUTHOR);
        this.publisher = Util.optString(ob, Common.TAG_BOOK_PUBLISHER);
        this.publicationDate = Util.optString(ob, Common.TAG_BOOK_PUBLISHED_DATE);
        this.language = Util.optString(ob, Common.TAG_BOOK_LANGUAGE);
        this.type = Util.optString(ob, Common.TAG_BOOK_DOCUMENT_FORMAT);
        this.shortUrl = Util.optString(ob, Common.TAG_BOOK_SHORT_URL);
        this.user = Util.optString(ob, Common.TAG_BOOK_CREATE_USER);
        this.createDate = Util.optString(ob, Common.TAG_BOOK_CREATE_DATE);
        this.modifyDate = Util.optString(ob, Common.TAG_BOOK_MODIFY_DATE);
        this.shareState = Util.optString(ob, Common.TAG_BOOK_SHARE);
        this.categoryId = Util.optString(ob, Common.TAG_BOOK_CATEGORY_CODE);
        this.categoryName = Util.optString(ob, Common.TAG_BOOK_CATEGORY_VALUE_KO);
        this.userKey = Util.optString(ob, Common.TAG_BOOK_PERSON_OBJID);
        this.userName = Util.optString(ob, Common.TAG_USER_NAME);
        this.userComment = Util.optString(ob, Common.TAG_BOOK_ABOUT_CONTENT);
        this.bookFileSize = Long.parseLong(Util.optString(ob, Common.TAG_BOOK_FILE_SIZE));
        this.thumbnailUrl = Util.optString(ob, Common.TAG_BOOK_THUMBNAIL_URL);
        this.bookDownloadUrl = Util.optString(ob, Common.TAG_BOOK_DOWNLOAD_URL);
        this.bookViewUrl = Util.optString(ob, Common.TAG_BOOK_VIEWER_URL);
        this.userProfileUrl = Util.optString(ob, Common.TAG_BOOK_PROFILE_URL);
        String val = Util.optString(ob, Common.TAG_BOOK_IS_LIKE);
        if (!TextUtils.isEmpty(val))
            this.like = val.equals("1");
    }

    public BookItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.bookId = in.readString();
        this.type = in.readString();
        this.value = in.readString();
        this.bookPath = in.readString();
        this.bookDownloadUrl = in.readString();
        this.bookViewUrl = in.readString();
        this.bookFileSize = in.readLong();
        this.shortUrl = in.readString();
        this.thumbnailUrl = in.readString();
        this.thumbnailPath = in.readString();
        in.readByteArray(this.thumbnail);
        this.title = in.readString();
        this.detail = in.readString();
        this.publisher = in.readString();
        this.publicationDate = in.readString();
        this.publishedBy = in.readString();
        this.overview = in.readString();
        this.categoryId = in.readString();
        this.categoryName = in.readString();
        this.language = in.readString();
        this.likeCount = in.readString();
        this.viewCount = in.readString();
        this.commentCount = in.readString();
        this.like = in.readInt() == 1;
        this.userKey = in.readString();
        this.userName = in.readString();
        this.userNickname = in.readString();
        this.userComment = in.readString();
        this.userProfileUrl = in.readString();
        this.user = in.readString();
        this.createDate = in.readString();
        this.modifyDate = in.readString();
        this.shareState = in.readString();
        this.downlaodStatus = in.readInt();
        this.downloadProgress = in.readFloat();
        this.downloadAt = in.readLong();
        this.downloadStartedAt = in.readLong();
        this.channelId = in.readString();
        this.tag = in.readString();
    }

    public static void createTableBook(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(BOOK_ID + " text not null unique, ")
                .append(BOOK_TYPE + " text, ")
                .append(BOOK_VALUE + " text, ")
                .append(BOOK_PATH + " text, ")
                .append(BOOK_DOWNLOAD_URL + " text, ")
                .append(BOOK_VIEW_URL + " text, ")
                .append(BOOK_FILE_SIZE + " integer, ")
                .append(BOOK_SHORT_URL + " text, ")
                .append(BOOK_TITLE + " text, ")
                .append(BOOK_DETAIL + " text, ")
                .append(BOOK_PUBLISHER + " text, ")
                .append(BOOK_PUBLICATION_DATE + " text, ")
                .append(BOOK_PUBLISHED_BY + " text, ")
                .append(BOOK_OVERVIEW + " text, ")
                .append(BOOK_LANGUAGE + " text, ")
                .append(BOOK_VIEW_COUNT + " text, ")
                .append(BOOK_LIKE_COUNT + " text, ")
                .append(BOOK_COMMENT_COUNT + " text, ")
                .append(BOOK_LIKE + " integer, ")
                .append(BOOK_CREATE_USER_KEY + " text, ")
                .append(BOOK_CREATE_USER_NAME + " text, ")
                .append(BOOK_CREATE_NICK_NAME + " text, ")
                .append(BOOK_CREATE_USER_COMMENT + " text, ")
                .append(BOOK_CREATE_USER_PROFILE_URL + " text, ")
                .append(BOOK_CREATE_USER + " text, ")
                .append(BOOK_CREATE_DATE + " text, ")
                .append(BOOK_MODIFY_DATE + " text, ")
                .append(BOOK_SHARE_STATE + " text, ")
                .append(CATEGORY_ID + " text, ")
                .append(CATEGORY_NAME + " text, ")
                .append(CHANNEL_ID + " text, ")
                .append(BOOK_TAG + " text, ")
                .append(BOOK_DOWNLOAD_STATUS + " integer, ")
                .append(BOOK_DOWNLOAD_PROGRESS + " float,  ")
                .append(BOOK_DOWNLOAD_AT + " integer, ")
                .append(BOOK_DOWNLOAD_STARTED_AT + " integer, ")
                .append(BOOK_THUMBNAIL_URL + " text, ")
                .append(BOOK_THUMBNAIL_PATH + " text, ")
                .append(BOOK_THUMBNAIL + " BLOB) ")
                .toString());
    }

    public static BookItem getBookItem(SQLiteDatabase db, String id) {
        List<BookItem> list = getBookItems(db, null, BOOK_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<BookItem> getBookItemsByCategory(SQLiteDatabase db, String categoryId) {
        List<BookItem> list = getBookItems(db, null, CATEGORY_ID + " = ?", new String[]{categoryId}, null, null, null, null);

        return list;
    }

    public static List<BookItem> getBookItemsByChannel(SQLiteDatabase db, String channelId) {
        List<BookItem> list = getBookItems(db, null, CHANNEL_ID + " = ?", new String[]{channelId}, null, null, null, null);

        return list;
    }

    public static List<BookItem> getBookItemByCreateUser(SQLiteDatabase db, String createUser) {
        List<BookItem> list = getBookItems(db, null, BOOK_CREATE_USER_KEY + " = ?", new String[]{createUser}, null, null, null, null);

        return list;
    }

    public static List<BookItem> getBookItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<BookItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                BookItem item = new BookItem()
                        .setRowId(c)
                        .setBookId(c)
                        .setBookType(c)
                        .setBookFileVersion(c)
                        .setBookPath(c)
                        .setBookDownloadUrl(c)
                        .setBookViewUrl(c)
                        .setBookFileSize(c)
                        .setBookShortUrl(c)
                        .setBookThumbnailUrl(c)
                        .setBookThumbnailPath(c)
                        .setBookThumbnail(c)
                        .setBookLikeCount(c)
                        .setBookViewCount(c)
                        .setBookCommentCount(c)
                        .setBookLike(c)
                        .setBookCreateDate(c)
                        .setBookModifyDate(c)
                        .setBookShareState(c)
                        .setBookDownloadStatus(c)
                        .setBookDownloadProgress(c)
                        .setBookDownloadAt(c)
                        .setBookDownloadStartedAt(c)
                        .setBookTitle(c)
                        .setBookDetail(c)
                        .setBookPublisher(c)
                        .setBookPublicationDate(c)
                        .setBookPublishedBy(c)
                        .setBookOverview(c)
                        .setBookLanguage(c)
                        .setBookTags(c)
                        .setCategoryId(c)
                        .setCategoryName(c)
                        .setUser(c)
                        .setUserKey(c)
                        .setUserName(c)
                        .setUserNickname(c)
                        .setUserComment(c)
                        .setUserProfileUrl(c)
                        .setChannelId(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean restateBookDownloadState(SQLiteDatabase db) {
        try {
            db.execSQL("update " + TABLE_NAME + " set " + BOOK_DOWNLOAD_STATUS + " = '" + DOWNLOAD_STATE_PAUSE + "' where " + BOOK_DOWNLOAD_STATUS + " = '" +
                    DOWNLOAD_STATE_DOWNLOADING + "'");

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateBookStatus(SQLiteDatabase db, BookItem book) {
        try {
            ContentValues v = new ContentValues();
            book.putBookDownloadStatus(v)
                    .putBookDownloadProgress(v)
                    .putBookDownloadStartedAt(v)
                    .putBookThumbnailPath(v)
                    .putBookDownloadAt(v)
                    .putBookPath(v);

            db.update(TABLE_NAME, v, BOOK_ID + " = ?", new String[]{book.getBookId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateBookCountState(SQLiteDatabase db, BookItem book) {
        try {
            ContentValues v = new ContentValues();
            book.putBookLikeCount(v)
                    .putBookCommentCount(v)
                    .putBookViewCount(v)
                    .putBookLikeCount(v)
                    .putBookLike(v);

            db.update(TABLE_NAME, v, BOOK_ID + " = ?", new String[]{book.getBookId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteBook(Context context, SQLiteDatabase db, BookItem item) {
        try {
            db.delete(TABLE_NAME, BOOK_ID + " = ?", new String[]{item.getBookId()});
            if (!TextUtils.isEmpty(item.getBookPath())) {
                File bookFile = new File(item.getBookPath());
                FileUtil.delete(bookFile);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean insertOrUpdateBookItem(SQLiteDatabase db, BookItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBookId(v)
                    .putBookType(v)
                    .putBookFileVersion(v)
                    .putBookPath(v)
                    .putBookDownloadUrl(v)
                    .putBookViewUrl(v)
                    .putBookFileSize(v)
                    .putBookShortUrl(v)
                    .putBookThumbnailPath(v)
                    .putBookThumbnailUrl(v)
                    .putBookThumbnail(v)
                    .putBookLikeCount(v)
                    .putBookViewCount(v)
                    .putBookCommentCount(v)
                    .putBookLike(v)
                    .putBookCreateDate(v)
                    .putBookModifyDate(v)
                    .putBookShareState(v)
                    .putBookDownloadStatus(v)
                    .putBookDownloadProgress(v)
                    .putBookDownloadAt(v)
                    .putBookDownloadStartedAt(v)
                    .putBookTitle(v)
                    .putBookDetail(v)
                    .putBookPublisher(v)
                    .putBookPublicationDate(v)
                    .putBookPublishedBy(v)
                    .putBookOverview(v)
                    .putBookLanguage(v)
                    .putBookTags(v)
                    .putCategoryId(v)
                    .putCategoryName(v)
                    .putUser(v)
                    .putUserKey(v)
                    .putUserName(v)
                    .putUserNickname(v)
                    .putUserComment(v)
                    .putUserProfileUrl(v)
                    .putChannelId(v);

            int rowAffected = db.update(TABLE_NAME, v, BOOK_ID + " =? ", new String[]{item.getBookId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getBookTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAllBookItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(bookId);
        out.writeString(type);
        out.writeString(value);
        out.writeString(bookPath);
        out.writeString(bookDownloadUrl);
        out.writeString(bookViewUrl);
        out.writeLong(bookFileSize);
        out.writeString(shortUrl);
        out.writeString(thumbnailPath);
        out.writeString(thumbnailUrl);
        out.writeByteArray(thumbnail);
        out.writeString(likeCount);
        out.writeString(viewCount);
        out.writeString(commentCount);
        out.writeInt(like ? 1 : 0);
        out.writeString(createDate);
        out.writeString(modifyDate);
        out.writeString(shareState);
        out.writeInt(downlaodStatus);
        out.writeFloat(downloadProgress);
        out.writeLong(downloadAt);
        out.writeLong(downloadStartedAt);
        out.writeString(title);
        out.writeString(detail);
        out.writeString(publisher);
        out.writeString(publicationDate);
        out.writeString(publishedBy);
        out.writeString(overview);
        out.writeString(language);
        out.writeString(categoryId);
        out.writeString(categoryName);
        out.writeString(user);
        out.writeString(userKey);
        out.writeString(userName);
        out.writeString(userNickname);
        out.writeString(userComment);
        out.writeString(userProfileUrl);
        out.writeString(channelId);
        out.writeString(tag);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BookItem && bookId == ((BookItem) obj).bookId;
    }

    public long getRowId() {
        return this.id;
    }

    public BookItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public BookItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public BookItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getBookId() {
        return this.bookId;
    }

    public BookItem setBookId(Cursor c) {
        this.bookId = s(c, BOOK_ID);

        return this;
    }

    public BookItem setBookId(String bookId) {
        this.bookId = bookId;

        return this;
    }

    public BookItem putBookId(ContentValues v) {
        v.put(BOOK_ID, this.bookId);

        return this;
    }

    public String getBookType() {
        return this.type;
    }

    public BookItem setBookType(Cursor c) {
        this.type = s(c, BOOK_TYPE);

        return this;
    }

    public BookItem setBookType(String type) {
        this.type = type;

        return this;
    }

    public BookItem putBookType(ContentValues v) {
        v.put(BOOK_TYPE, this.type);

        return this;
    }

    public String getBookFileVersion() {
        return this.value;
    }

    public BookItem setBookFileVersion(String val) {
        this.value = val;

        return this;
    }

    public BookItem setBookFileVersion(Cursor c) {
        this.value = s(c, BOOK_VALUE);

        return this;
    }

    public BookItem putBookFileVersion(ContentValues v) {
        v.put(BOOK_VALUE, this.value);

        return this;
    }

    public String getBookPath() {
        return this.bookPath;
    }

    public BookItem setBookPath(Cursor c) {
        this.bookPath = s(c, BOOK_PATH);

        return this;
    }

    public BookItem setBookPath(String path) {
        this.bookPath = path;

        return this;
    }

    public BookItem putBookPath(ContentValues v) {
        v.put(BOOK_PATH, this.bookPath);

        return this;
    }

    public String getBookDownloadUrl() {
        return this.bookDownloadUrl;
    }

    public BookItem setBookDownloadUrl(String downloadUrl) {
        this.bookDownloadUrl = downloadUrl;

        return this;
    }

    public BookItem setBookDownloadUrl(Cursor c) {
        this.bookDownloadUrl = s(c, BOOK_DOWNLOAD_URL);

        return this;
    }

    public BookItem putBookDownloadUrl(ContentValues v) {
        v.put(BOOK_DOWNLOAD_URL, this.bookDownloadUrl);

        return this;
    }

    public String getBookViewUrl() {
        return this.bookViewUrl;
    }

    public BookItem setBookViewUrl(String viewUrl) {
        this.bookViewUrl = viewUrl;

        return this;
    }

    public BookItem setBookViewUrl(Cursor c) {
        this.bookViewUrl = s(c, BOOK_VIEW_URL);

        return this;
    }

    public BookItem putBookViewUrl(ContentValues v) {
        v.put(BOOK_VIEW_URL, this.bookViewUrl);

        return this;
    }

    public long getBookFileSize() {
        return this.bookFileSize;
    }

    public BookItem setBookFileSize(long fileSize) {
        this.bookFileSize = fileSize;

        return this;
    }

    public BookItem setBookFileSize(Cursor c) {
        this.bookFileSize = l(c, BOOK_FILE_SIZE);

        return this;
    }

    public BookItem putBookFileSize(ContentValues v) {
        v.put(BOOK_FILE_SIZE, bookFileSize);

        return this;
    }

    public String getBookShortUrl() {
        return this.shortUrl;
    }

    public BookItem setBookShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;

        return this;
    }

    public BookItem setBookShortUrl(Cursor c) {
        this.shortUrl = s(c, BOOK_SHORT_URL);

        return this;
    }

    public BookItem putBookShortUrl(ContentValues v) {
        v.put(BOOK_SHORT_URL, this.shortUrl);

        return this;
    }

    public String getBookThumbnailPath() {
        return this.thumbnailPath;
    }

    public BookItem setBookThumbnailPath(Cursor c) {
        this.thumbnailPath = s(c, BOOK_THUMBNAIL_PATH);

        return this;
    }

    public BookItem setBookThumbnailPath(String path) {
        this.thumbnailPath = path;

        return this;
    }

    public BookItem putBookThumbnailPath(ContentValues v) {
        v.put(BOOK_THUMBNAIL_PATH, this.thumbnailPath);

        return this;
    }

    public String getBookThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public BookItem setBookThumbnailUrl(Cursor c) {
        this.thumbnailUrl = s(c, BOOK_THUMBNAIL_URL);

        return this;
    }

    public BookItem setBookThumbnailUrl(String url) {
        this.thumbnailUrl = url;

        return this;
    }

    public BookItem putBookThumbnailUrl(ContentValues v) {
        v.put(BOOK_THUMBNAIL_URL, this.thumbnailUrl);

        return this;
    }

    public byte[] getBookThumbnail() {
        return this.thumbnail;
    }

    public BookItem setBookThumbnail(Cursor c) {
        this.thumbnail = blob(c, BOOK_THUMBNAIL);

        return this;
    }

    public BookItem setBookThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public BookItem putBookThumbnail(ContentValues v) {
        v.put(BOOK_THUMBNAIL, this.thumbnail);

        return this;
    }

    public String getBookLikeCount() {
        return this.likeCount;
    }

    public BookItem setBookLikeCount(String likeCount) {
        this.likeCount = likeCount;

        return this;
    }

    public BookItem setBookLikeCount(Cursor c) {
        this.likeCount = s(c, BOOK_LIKE_COUNT);

        return this;
    }

    public BookItem putBookLikeCount(ContentValues v) {
        v.put(BOOK_LIKE_COUNT, this.likeCount);

        return this;
    }

    public String getBookViewCount() {
        return this.viewCount;
    }

    public BookItem setBookViewCount(String viewCount) {
        this.viewCount = viewCount;

        return this;
    }

    public BookItem setBookViewCount(Cursor c) {
        this.viewCount = s(c, BOOK_VIEW_COUNT);

        return this;
    }

    public BookItem putBookViewCount(ContentValues v) {
        v.put(BOOK_VIEW_COUNT, viewCount);

        return this;
    }

    public String getBookCommentCount() {
        return this.commentCount;
    }

    public BookItem setBookCommentCount(String commentCount) {
        this.commentCount = commentCount;

        return this;
    }

    public BookItem setBookCommentCount(Cursor c) {
        this.commentCount = s(c, BOOK_COMMENT_COUNT);

        return this;
    }

    public BookItem putBookCommentCount(ContentValues v) {
        v.put(BOOK_COMMENT_COUNT, this.commentCount);

        return this;
    }

    public boolean isBookLike() {
        return this.like;
    }

    public BookItem setBookLike(boolean like) {
        this.like = like;

        return this;
    }

    public BookItem setBookLike(Cursor c) {
        this.like = i(c, BOOK_LIKE) == 1;

        return this;
    }

    public BookItem putBookLike(ContentValues v) {
        v.put(BOOK_LIKE, like ? 1 : 0);

        return this;
    }

    public String getBookCreateDate() {
        return this.createDate;
    }

    public BookItem setBookCreateDate(String createDate) {
        this.createDate = createDate;

        return this;
    }

    public BookItem setBookCreateDate(Cursor c) {
        this.createDate = s(c, BOOK_CREATE_DATE);

        return this;
    }

    public BookItem putBookCreateDate(ContentValues v) {
        v.put(BOOK_CREATE_DATE, createDate);

        return this;
    }

    public String getBookModifyDate() {
        return this.modifyDate;
    }

    public BookItem setBookModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;

        return this;
    }

    public BookItem setBookModifyDate(Cursor c) {
        this.modifyDate = s(c, BOOK_MODIFY_DATE);

        return this;
    }

    public BookItem putBookModifyDate(ContentValues v) {
        v.put(BOOK_MODIFY_DATE, modifyDate);

        return this;
    }

    public String getBookShareState() {
        return this.shareState;
    }

    public BookItem setBookShareState(String shareState) {
        this.shareState = shareState;

        return this;
    }

    public BookItem setBookShareState(Cursor c) {
        this.shareState = s(c, BOOK_SHARE_STATE);

        return this;
    }

    public BookItem putBookShareState(ContentValues v) {
        v.put(BOOK_SHARE_STATE, this.shareState);

        return this;
    }

    public String getBookTitle() {
        return this.title;
    }

    public BookItem setBookTitle(Cursor c) {
        this.title = s(c, BOOK_TITLE);

        return this;
    }

    public BookItem setBookTitle(String title) {
        this.title = title;

        return this;
    }

    public BookItem putBookTitle(ContentValues v) {
        v.put(BOOK_TITLE, this.title);

        return this;
    }

    public String getBookDetail() {
        return this.detail;
    }

    public BookItem setBookDetail(Cursor c) {
        this.detail = s(c, BOOK_DETAIL);

        return this;
    }

    public BookItem setBookDetail(String detail) {
        this.detail = detail;

        return this;
    }

    public BookItem putBookDetail(ContentValues v) {
        v.put(BOOK_DETAIL, this.detail);

        return this;
    }

    public String getBookPublisher() {
        return this.publisher;
    }

    public BookItem setBookPublisher(String publisher) {
        this.publisher = publisher;

        return this;
    }

    public BookItem setBookPublisher(Cursor c) {
        this.publisher = s(c, BOOK_PUBLISHER);

        return this;
    }

    public BookItem putBookPublisher(ContentValues v) {
        v.put(BOOK_PUBLISHER, this.publisher);

        return this;
    }

    public String getBookPublicationDate() {
        return this.publicationDate;
    }

    public BookItem setBookPublicationDate(Cursor c) {
        this.publicationDate = s(c, BOOK_PUBLICATION_DATE);

        return this;
    }

    public BookItem setBookPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;

        return this;
    }

    public BookItem putBookPublicationDate(ContentValues v) {
        v.put(BOOK_PUBLICATION_DATE, this.publicationDate);

        return this;
    }

    public String getBookPublishedBy() {
        return this.publishedBy;
    }

    public BookItem setBookPublishedBy(Cursor c) {
        this.publishedBy = s(c, BOOK_PUBLISHED_BY);

        return this;
    }

    public BookItem setBookPublishedBy(String publishedBy) {
        this.publishedBy = publishedBy;

        return this;
    }

    public BookItem putBookPublishedBy(ContentValues v) {
        v.put(BOOK_PUBLISHED_BY, this.publishedBy);

        return this;
    }

    public String getBookOverview() {
        return this.overview;
    }

    public BookItem setBookOverview(Cursor c) {
        this.overview = s(c, BOOK_OVERVIEW);

        return this;
    }

    public BookItem setBookOverview(String overview) {
        this.overview = overview;

        return this;
    }

    public BookItem putBookOverview(ContentValues v) {
        v.put(BOOK_OVERVIEW, this.overview);

        return this;
    }

    public String getBookLanguage() {
        return this.language;
    }

    public BookItem setBookLanguage(String language) {
        this.language = language;

        return this;
    }

    public BookItem setBookLanguage(Cursor c) {
        this.language = s(c, BOOK_LANGUAGE);

        return this;
    }

    public BookItem putBookLanguage(ContentValues v) {
        v.put(BOOK_LANGUAGE, this.language);

        return this;
    }

    public String getCategoryId() {
        return this.categoryId;
    }

    public BookItem setCategoryId(Cursor c) {
        this.categoryId = s(c, CATEGORY_ID);

        return this;
    }

    public BookItem setCategoryId(String id) {
        this.categoryId = id;

        return this;
    }

    public BookItem putCategoryId(ContentValues v) {
        v.put(CATEGORY_ID, this.categoryId);

        return this;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public BookItem setCategoryName(String categoryName) {
        this.categoryName = categoryName;

        return this;
    }

    public BookItem setCategoryName(Cursor c) {
        this.categoryName = s(c, CATEGORY_NAME);

        return this;
    }

    public BookItem putCategoryName(ContentValues v) {
        v.put(CATEGORY_NAME, this.categoryName);

        return this;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public BookItem setChannelId(Cursor c) {
        this.channelId = s(c, CHANNEL_ID);

        return this;
    }

    public BookItem setChannelId(String id) {
        this.channelId = id;

        return this;
    }

    public BookItem putChannelId(ContentValues v) {
        v.put(CHANNEL_ID, this.channelId);

        return this;
    }

    public int getBookDownloadStatus() {
        return this.downlaodStatus;
    }

    public BookItem setBookDownloadStatus(Cursor c) {
        this.downlaodStatus = i(c, BOOK_DOWNLOAD_STATUS);

        return this;
    }

    public BookItem setBookDownloadStatus(int status) {
        this.downlaodStatus = status;

        return this;
    }

    public BookItem putBookDownloadStatus(ContentValues v) {
        v.put(BOOK_DOWNLOAD_STATUS, this.downlaodStatus);

        return this;
    }

    public float getBookDownloadProgress() {
        return this.downloadProgress;
    }

    public BookItem setBookDownloadProgress(Cursor c) {
        this.downloadProgress = f(c, BOOK_DOWNLOAD_PROGRESS);

        return this;
    }

    public BookItem setBookDownloadProgress(float progress) {
        this.downloadProgress = progress;

        return this;
    }

    public BookItem putBookDownloadProgress(ContentValues v) {
        v.put(BOOK_DOWNLOAD_PROGRESS, this.downloadProgress);

        return this;
    }

    public long getBookDownloadAt() {
        return this.downloadAt;
    }

    public BookItem setBookDownloadAt(Cursor c) {
        this.downloadAt = l(c, BOOK_DOWNLOAD_AT);

        return this;
    }

    public BookItem setBookDownloadAt(long at) {
        this.downloadAt = at;

        return this;
    }

    public BookItem putBookDownloadAt(ContentValues v) {
        v.put(BOOK_DOWNLOAD_AT, this.downloadAt);

        return this;
    }

    public long getBookDownloadStartedAt() {
        return this.downloadStartedAt;
    }

    public BookItem setBookDownloadStartedAt(Cursor c) {
        this.downloadStartedAt = l(c, BOOK_DOWNLOAD_STARTED_AT);

        return this;
    }

    public BookItem setBookDownloadStartedAt(long at) {
        this.downloadStartedAt = at;

        return this;
    }

    public BookItem putBookDownloadStartedAt(ContentValues v) {
        v.put(BOOK_DOWNLOAD_STARTED_AT, this.downloadStartedAt);

        return this;
    }

    public String getBookTags() {
        return this.tag;
    }

    public BookItem setBookTags(Cursor c) {
        this.tag = s(c, BOOK_TAG);

        return this;
    }

    public BookItem setBookTags(String tags) {
        this.tag = tags;

        return this;
    }

    public BookItem putBookTags(ContentValues v) {
        v.put(BOOK_TAG, this.tag);

        return this;
    }

    public String getUser() {
        return this.user;
    }

    public BookItem setUser(String user) {
        this.user = user;

        return this;
    }

    public BookItem setUser(Cursor c) {
        this.user = s(c, BOOK_CREATE_USER);

        return this;
    }

    public BookItem putUser(ContentValues v) {
        v.put(BOOK_CREATE_USER, this.user);

        return this;
    }

    public String getUserKey() {
        return this.userKey;
    }

    public BookItem setUserKey(String userKey) {
        this.userKey = userKey;

        return this;
    }

    public BookItem setUserKey(Cursor c) {
        this.userKey = s(c, BOOK_CREATE_USER_KEY);

        return this;
    }

    public BookItem putUserKey(ContentValues v) {
        v.put(BOOK_CREATE_USER_KEY, this.userKey);

        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public BookItem setUserName(String userName) {
        this.userName = userName;

        return this;
    }

    public BookItem setUserName(Cursor c) {
        this.userName = s(c, BOOK_CREATE_USER_NAME);

        return this;
    }

    public BookItem putUserName(ContentValues v) {
        v.put(BOOK_CREATE_USER_NAME, this.userName);

        return this;
    }

    public String getUserNickname() {
        return this.userNickname;
    }

    public BookItem setUserNickname(String nickname) {
        this.userNickname = nickname;

        return this;
    }

    public BookItem setUserNickname(Cursor c) {
        this.userNickname = s(c, BOOK_CREATE_NICK_NAME);

        return this;
    }

    public BookItem putUserNickname(ContentValues v) {
        v.put(BOOK_CREATE_NICK_NAME, this.userNickname);

        return this;
    }

    public String getUserComment() {
        return this.userComment;
    }

    public BookItem setUserComment(String userComment) {
        this.userComment = userComment;

        return this;
    }

    public BookItem setUserComment(Cursor c) {
        this.userComment = s(c, BOOK_CREATE_USER_COMMENT);

        return this;
    }

    public BookItem putUserComment(ContentValues v) {
        v.put(BOOK_CREATE_USER_COMMENT, this.userComment);

        return this;
    }

    public String getUserProfileUrl() {
        return this.userProfileUrl;
    }

    public BookItem setUserProfileUrl(String userProfileUrl) {
        this.userProfileUrl = userProfileUrl;

        return this;
    }

    public BookItem setUserProfileUrl(Cursor c) {
        this.userProfileUrl = s(c, BOOK_CREATE_USER_PROFILE_URL);

        return this;
    }

    public BookItem putUserProfileUrl(ContentValues v) {
        v.put(BOOK_CREATE_USER_PROFILE_URL, this.userProfileUrl);

        return this;
    }

    public boolean isDownloaded() {
        return this.downloadAt > 0;
    }

    public File getBookDir() {
        return new File(bookPath, channelId);
    }

    public File getBookFile() {
        return new File(getBookDir(), bookId);
    }

    public File getTempBookFile() {
        return new File(getBookDir(), bookId + FileUtil.TEMP_FILE_EXTENSION);
    }

    private static class BookComparator extends BaseComparator implements Comparator<BookItem> {
        public BookComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(BookItem lhs, BookItem rhs) {
            if (lhs == null && rhs != null)
                return 1;
            else if (rhs == null && lhs != null)
                return -1;
            else
                return 0;
        }
    }

    private static class NewBookComparator extends BookComparator {
        public NewBookComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(BookItem lhs, BookItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareString(lhs.getBookCreateDate(), rhs.getBookCreateDate(), false);
            if (result != 0)
                return result;
            else {
                result = compareString(lhs.getBookTitle(), rhs.getBookTitle(), false, true);
                if (result != 0)
                    return result;
                else
                    return compareString(lhs.getBookModifyDate(), rhs.getBookModifyDate(), false, false);
            }
        }

        ;
    }

    private static class HitBookComparator extends BookComparator {
        public HitBookComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(BookItem lhs, BookItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareInt(Integer.parseInt(lhs.getBookViewCount()), Integer.parseInt(rhs.getBookViewCount()), false);
            if (result != 0)
                return result;
            else {
                result = compareString(lhs.getBookCreateDate(), rhs.getBookCreateDate(), false, true);
                if (result != 0)
                    return result;
                else
                    return compareString(lhs.getBookTitle(), rhs.getBookTitle(), false);
            }
        }

        ;
    }

    private static class BestBookComparator extends BookComparator {
        public BestBookComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(BookItem lhs, BookItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareInt(Integer.parseInt(lhs.getBookLikeCount()), Integer.parseInt(rhs.getBookLikeCount()), false);
            if (result != 0)
                return result;
            else {
                result = compareString(lhs.getBookCreateDate(), rhs.getBookCreateDate(), false, true);
                if (result != 0)
                    return result;
                else
                    return compareString(lhs.getBookTitle(), rhs.getBookTitle(), false);
            }
        }

        ;
    }
}

package io.jmobile.vuuk.data;

import android.content.Context;
import android.os.Parcel;

import io.jmobile.vuuk.R;

public class TimerItem extends TableObject {
    public static final long TIMER_NONE = 0;
    public static final long TIMER_30_MINUTES = 1000 * 60 * 30;
    public static final long TIMER_1_HOUR = 1000 * 60 * 60;
    public static final long TIMER_3_HOURS = 1000 * 60 * 60 * 3;
    public static final long TIMER_6_HOURS = 1000 * 60 * 60 * 6;
    public static final long TIMER_9_HOURS = 1000 * 60 * 60 * 9;

    public static final Creator<TimerItem> CREATOR = new Creator<TimerItem>() {
        @Override
        public TimerItem createFromParcel(Parcel source) {
            return new TimerItem(source);
        }

        @Override
        public TimerItem[] newArray(int size) {
            return new TimerItem[size];
        }
    };

    private String title;
    private long time;

    public TimerItem() {
        super();
    }

    public TimerItem(Context context, long time) {
        this.time = time;
        if (time == 0)
            this.title = context.getString(R.string.timer_none);
        else if (time == TIMER_30_MINUTES)
            this.title = String.format(context.getString(R.string.timer_format_min), 30);
        else if (time == TIMER_1_HOUR)
            this.title = String.format(context.getString(R.string.timer_format_hour), 1);
        else if (time == TIMER_3_HOURS)
            this.title = String.format(context.getString(R.string.timer_format_hour), 3);
        else if (time == TIMER_6_HOURS)
            this.title = String.format(context.getString(R.string.timer_format_hour), 6);
        else if (time == TIMER_9_HOURS)
            this.title = String.format(context.getString(R.string.timer_format_hour), 9);
    }

    public TimerItem(long time, String title) {
        this.time = time;
        this.title = title;
    }

    public TimerItem(Parcel in) {
        super(in);

        this.time = in.readLong();
        this.title = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(time);
        out.writeString(title);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof TimerItem && time == ((TimerItem) obj).time;
    }

    public String getTitle() {
        return this.title;
    }

    public TimerItem setTitle(String title) {
        this.title = title;

        return this;
    }

    public long getTime() {
        return this.time;
    }

    public TimerItem setTime(long time) {
        this.time = time;

        return this;
    }
}
